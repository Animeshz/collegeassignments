#include <bits/stdc++.h>

using namespace std;

int lru(int ref[], int n, int frames)
{
    set<int> s;
    map<int, int> m;
    int page_faults = 0;

    for (int i = 0; i < n; i++)
    {
        m[ref[i]] = i;
        if (s.find(ref[i]) != s.end())
        {
            continue;
        }
        if (s.size() < frames)
        {
            s.insert(ref[i]);
            page_faults++;
            continue;
        }
        int val = -1;
        int d = INT_MAX;
        for (auto x : s)
        {
            if (m[x] < d)
            {
                d = m[x];
                val = x;
            }
        }
        s.erase(val);
        s.insert(ref[i]);
        page_faults++;
    }

    return page_faults;
}

int main()
{
    int ref[] = {3, 8, 2, 3, 9, 1, 6, 3, 8, 9, 3, 6, 2, 1, 3};
    int n = sizeof(ref) / sizeof(ref[0]);
    int frames = 5;
    int page_faults_lru = lru(ref, n, frames);
    cout << "Page Faults (LRU): " << page_faults_lru << endl;

    std::deque<int> last_five_unique;
    std::set<int> unique_set;

    cout << "The LRU ending queue will be: ";
    for (int i = n - 1; i >= 0 && last_five_unique.size() < 5; --i) {
        if (unique_set.insert(ref[i]).second) {
            last_five_unique.push_front(ref[i]);
        }
    }

    for (auto i : last_five_unique) {
        std::cout << i << " ";
    }
    std::cout << std::endl;

    return 0;
}


