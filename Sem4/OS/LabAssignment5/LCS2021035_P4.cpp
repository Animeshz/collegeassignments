#include <bits/stdc++.h>

using namespace std;

// int fifo(int ref[], int n, int frames)
// {
//     queue<int> q;
//     set<int> s;
//     int page_faults = 0;
//
//     for (int i = 0; i < n; i++)
//     {
//         if (s.find(ref[i]) != s.end())
//         {
//             continue;
//         }
//
//         if (q.size() == frames)
//         {
//             int oldest_page = q.front();
//             q.pop();
//             s.erase(oldest_page);
//         }
//         q.push(ref[i]);
//         s.insert(ref[i]);
//         page_faults++;
//     }
//
//     return page_faults;
// }
int lru(int ref[], int n, int frames)
{
    set<int> s;
    map<int, int> m;
    int page_faults = 0;

    for (int i = 0; i < n; i++)
    {
        m[ref[i]] = i;
        if (s.find(ref[i]) != s.end())
        {
            continue;
        }
        if (s.size() < frames)
        {
            s.insert(ref[i]);
            page_faults++;
            continue;
        }
        int val = -1;
        int d = INT_MAX;
        for (auto x : s)
        {
            if (m[x] < d)
            {
                d = m[x];
                val = x;
            }
        }
        s.erase(val);
        s.insert(ref[i]);
        page_faults++;
    }

    return page_faults;
}
// int optimal(int ref[], int n, int frames)
// {
//     set<int> s;
//     map<int, int> m;
//     int page_faults = 0;
//
//     for (int i = 0; i < n; i++)
//     {
//         if (s.find(ref[i]) != s.end())
//         {
//             continue;
//         }
//         if (s.size() < frames)
//         {
//             s.insert(ref[i]);
//             page_faults++;
//             continue;
//         }
//         vector<int> temp(1e5, INT_MAX);
//
//         for (auto x : s)
//         {
//             for (int j = i; j < n; j++)
//             {
//                 if (x == ref[j])
//                 {
//                     if (temp[x] == INT_MAX)
//                     {
//                         temp[x] = j;
//                     }
//                 }
//             }
//         }
//         int val = -1;
//         int d = -1;
//         for (auto x : s)
//         {
//             if (temp[x] > d)
//             {
//                 d = temp[x];
//                 val = x;
//             }
//         }
//         s.erase(val);
//         s.insert(ref[i]);
//         page_faults++;
//     }
//
//     return page_faults;
// }

int main()
{
    int ref[] = {3, 8, 2, 3, 9, 1, 6, 3, 8, 9, 3, 6, 2, 1, 3};
    int n = sizeof(ref) / sizeof(ref[0]);
    int frames = 5;
    // int page_faults_fifo = fifo(ref, n, frames);
    // cout << "Page Faults (FIFO): " << page_faults_fifo << endl;
    int page_faults_lru = lru(ref, n, frames);
    cout << "Page Faults (LRU): " << page_faults_lru << endl;
    // int page_faults_optimal = optimal(ref, n, frames);
    // cout << "Page Faults (OPTIMAL): " << page_faults_optimal << endl;
    return 0;
}


