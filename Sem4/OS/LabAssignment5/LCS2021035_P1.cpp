#include <bits/stdc++.h>
using namespace std;

int main() {
    int n, m;

    cout << "Enter the number of blocks: ";
    cin >> n;
    int blocks[n];
    cout << "Enter the block sizes: ";
    for (int i = 0; i < n; i++) {
        cin >> blocks[i];
    }

    cout << "Enter the number files: ";
    cin >> m;
    int files[m];
    cout << "Enter the sizes of files: ";
    for (int i = 0; i < m; i++) {
        cin >> files[i];
    }

    int answer[m];
    set<int> s;
    for (int i = 0; i < m; i++) {
        int flag = 0;
        for (int j = 0; j < n; j++) {
            if (blocks[j] >= files[i] && s.find(j) == s.end()) {
                answer[i] = j;
                s.insert(j);
                flag = 1;
                break;
            }
        }
        if (flag == 0) {
            answer[i] = -1;
        }
    }
    for (int i = 0; i < m; i++) {
        if (answer[i] == -1) {
            cout << "File " << i + 1 << " cannot be placed in any block"
                 << endl;
            continue;
        }
        cout << "File " << i + 1 << " "
             << "is placed in Block " << answer[i] + 1 << " of size "
             << blocks[answer[i]] << endl;
    }
    return 0;
}

