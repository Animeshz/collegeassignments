#include <bits/stdc++.h>
using namespace std;

int main()
{
    int n, s, f;

    cout << "Enter the number of pages: ";
    cin >> n;
    cout << "Enter the page size: ";
    cin >> s;
    cout << "Enter the number of frames: ";
    cin >> f;

    int pt[n];
    cout << "Enter the page table: ";
    for (int i = 0; i < n; i++)
    {
        cin >> pt[i];
    }

    int l, p, pg, off;
    cout << "Enter the logical address: ";
    cin >> l;
    p = l / s;
    off = l % s;
    pg = pt[p];

    if (pg != -1 && p < n)
    {
        cout << "Page " << p << " is present in frame " << pg << endl;
    }
    else
    {
        cout << "Page " << p << " is not present in any frame" << endl;
        for (int i = 0; i < f; i++)
        {
            if (pt[i] == -1)
            {
                pt[i] = p;
                pg = i;
                break;
            }
            else if (i == f - 1)
            {
                int min = f;
                for (int j = 0; j < f; j++)
                {
                    int k = p + 1;
                    while (k < n)
                    {
                        if (pt[j] == pt[k])
                        {
                            if (k < min)
                            {
                                min = k;
                                pg = j;
                            }
                            break;
                        }
                        k++;
                    }
                    if (k == n)
                    {
                        pg = j;
                        break;
                    }
                }
                pt[pg] = p;
            }
        }
        cout << "Page " << p << " is allocated in frame " << pg << endl;
    }
    int pa = pg * s + off;
    cout << "Physical address is: " << pa << endl;
    return 0;
}
