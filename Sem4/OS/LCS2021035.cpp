#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef vector<char> vc;
typedef vector<ll> vl;
typedef vector<bool> vb;
typedef vector<vector<ll>> vvl;
typedef pair<ll, ll> pi;
typedef vector<pair<ll, ll>> vp;
const int M = 1e9 + 7;
const ll INF = 1e11;

int main() {
    vector<int> page_references = {3, 8, 2, 3, 9, 1, 6, 3, 8, 9, 3, 6, 2, 1, 3};
    int num_frames = 5;

    list<int> frames;
    unordered_map<int, list<int>::iterator> frame_map;

    int page_faults = 0;

    for (const int& page : page_references) {
        if (frame_map.find(page) == frame_map.end()) {
            if (frames.size() == num_frames) {
                int least_recently_used = frames.front();
                frames.pop_front();
                frame_map.erase(least_recently_used);
            }
            frames.push_back(page);
            frame_map[page] = prev(frames.end());
            page_faults++;
        } else {
            frames.erase(frame_map[page]);
            frames.push_back(page);
            frame_map[page] = prev(frames.end());
        }
    }

    cout << "\nRoll Number: LCS2021035";

    cout << "\nQueue: ";
    for (const int& frame : frames) {
        cout << frame << " ";
    }
    cout << "\nFaults: " << page_faults << endl;

    return 0;
}
