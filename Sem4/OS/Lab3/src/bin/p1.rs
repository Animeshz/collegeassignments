use std::io::{self, Write, BufRead};

macro_rules! input {
    () => (std::io::stdin().lock().lines().next().map_or(Err(io::Error::new(io::ErrorKind::UnexpectedEof, "Input BufRead reached EOF before".to_string())), |line| line).unwrap());
    ($($arg:tt)*) => ({ std::io::stdout().write_fmt(format_args!($($arg)*)).unwrap(); std::io::stdout().flush().unwrap(); input!() });
}

#[derive(Clone, Debug, Default)]
struct Process {
    pid: usize,
    arrival_time: u32,
    burst_time: u32,
    start_time: u32,
    completion_time: u32,
    turnaround_time: u32,
    waiting_time: u32,
    response_time: u32,
}

fn main() {
    let n: usize = input!("Enter the number of processes: ").parse().expect("Expected a positive integer value");
    let mut process = vec![Process::default(); n];
    println!();

    for i in 0..n {
        process[i].pid = i + 1;
        process[i].arrival_time = input!("Enter arrival time of process {}: ", process[i].pid).parse().expect("Expected a positive integer value");
        process[i].burst_time = input!("Enter burst time of process {}: ", process[i].pid).parse().expect("Expected a positive integer value");
        println!();
    }

    fcfs(&mut process);
    print_procs(&mut process);
}

fn fcfs(process: &mut [Process]) {
    process.sort_by(|a, b| a.arrival_time.cmp(&b.arrival_time));

    let mut last: Option<&Process> = None;

    process.iter_mut().for_each(|p| {
        let prev_completion = last.map(|prev| prev.completion_time).unwrap_or(0);

        p.start_time = std::cmp::max(prev_completion, p.arrival_time);
        p.completion_time = p.start_time + p.burst_time;
        p.turnaround_time = p.completion_time - p.arrival_time;
        p.waiting_time = p.turnaround_time - p.burst_time;
        p.response_time = p.start_time - p.arrival_time;

        last = Some(p);
    });
}

fn print_procs(process: &mut [Process]) {
    let n = process.len();

    let total_idle_time: u32 = process[0].arrival_time + process.windows(2).map(|w| w[1].start_time - w[0].completion_time).sum::<u32>();
    let avg_turnaround_time: f32 = process.iter().map(|p| p.turnaround_time).sum::<u32>() as f32 / n as f32;
    let avg_waiting_time: f32 = process.iter().map(|p| p.waiting_time).sum::<u32>() as f32 / n as f32;
    let avg_response_time: f32 = process.iter().map(|p| p.response_time).sum::<u32>() as f32 / n as f32;

    let throughput = (n as f32) / ((process[n - 1].completion_time - process[0].arrival_time) as f32);
    let cpu_utilisation = ((process[n - 1].completion_time - total_idle_time) as f32 / process[n - 1].completion_time as f32) * 100.0;

    // Generate Gantt chart
    let gantt_chart = process
        .iter()
        .map(|p| format!("P{} ({})", &p.pid.to_string(), p.burst_time))
        .collect::<Vec<String>>()
        .join(" | ");
    let gantt_chart = format!("| {} |", gantt_chart);
    let gantt_length = gantt_chart.len();
    let padding = "-".repeat(gantt_length);
    let mut gantt_chart = format!("{}\n{}\n{}\n", padding, gantt_chart, padding);
    let completion = process.iter().map(|p| p.completion_time).max().unwrap();
    gantt_chart.push_str(&format!("0{}{}\n", &" ".repeat(gantt_length-2), completion));

    println!("Scheduling Algorithm: FCFS");
    println!("\nGantt Chart:\n{}", gantt_chart);

    // Sort the processes by ID and print out their details
    process.sort_by_key(|process| process.pid);

    println!();
    println!("#P\tAT\tBT\tST\tCT\tTAT\tWT\tRT\n");

    for i in 0..n {
        let process = &process[i];
        println!(
            "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n",
            process.pid,
            process.arrival_time,
            process.burst_time,
            process.start_time,
            process.completion_time,
            process.turnaround_time,
            process.waiting_time,
            process.response_time
        );
    }

    println!("Average Turnaround Time = {:.2}", avg_turnaround_time);
    println!("Average Waiting Time = {:.2}", avg_waiting_time);
    println!("Average Response Time = {:.2}", avg_response_time);
    println!("CPU Idle Time = {}", total_idle_time);
    println!("CPU Utilization = {:.2}%", cpu_utilisation);
    println!("Throughput = {:.2} process/unit time", throughput);
}
