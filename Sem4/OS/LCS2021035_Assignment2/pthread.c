#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void *thread_task(void *arg) {
    int *tid = (int *)arg;
    printf("Thread %d starting...\n", *tid);

    sleep(1);
    printf("Thread %d exiting...\n", *tid);
    pthread_exit(0);  // Terminate this thread and return value to thread's parent
}

int main() {
    int n = 5;
    pthread_t threads[n];
    int tids[n];

    for (int i = 0; i < n; i++) {
        tids[i] = i;
        printf("Creating thread %d\n", i);

        // Create a new thread
        int status = pthread_create(&threads[i], NULL, thread_task, &tids[i]);
        if (status != 0) {
            printf("Error creating thread %d\n", i);
            exit(1);
        }
    }

    for (int i = 0; i < n; i++) {
        // Join the thread to wait for its completion
        int status = pthread_join(threads[i], NULL);
        if (status != 0) {
            printf("Error joining thread %d\n", i);
            exit(1);
        }
        printf("Thread %d completed\n", i);
    }

    return 0;
}

