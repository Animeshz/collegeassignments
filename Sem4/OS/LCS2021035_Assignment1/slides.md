---
theme: seriph
defaults:
  layout: 'default'
themeConfig:
  code-font-size: '10px'
# background: https://source.unsplash.com/collection/94734566/1920x1080
class: 'text-left'  # windi-css
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# persist drawings in exports and build
drawings:
  persist: false
# use UnoCSS
css: unocss
---

# OS Assignment 1

List of 50 linux terminal commands related to process and file management.

* Name => Animesh Sahu
* EnrollmentNo => LCS2021035
* Program => BTech (CS) 4th Sem
* Subject => Operating Systems
* Instructor => Dr. Mainak Adhikari
* Institute => Indian Institute of Information Technology, Lucknow


---

# 1) ECHO command

The echo command is used to display a message or the value of a variable

SYNTAX - echo message

EXAMPLE - echo "Hello World!"


---

# 2) PWD command

The pwd command is used to display the current working directory

SYNTAX - pwd

EXAMPLE - pwd


---

# 3) CD command

The cd command is used to change the current working directory

SYNTAX - cd destination-path

EXAMPLE - cd /usr/local/bin


---

# 4) LS command

The ls command is used to list the files and directories in a directory

SYNTAX - ls [optional-path]

EXAMPLE - ls ~/Downloads


---

# 5) CP command

The cp command is used to copy files and directories

SYNTAX - cp file(s)... destination

EXAMPLE - cp file1.txt file2.txt ~/Documents


---

# 6) MV command

The mv command is used to move a file from one directory to another

SYNTAX - mv file(s)... destination-path

EXAMPLE - mv file1.txt file2.txt ~/Documents


---

# 7) TOUCH command

The touch command is used to create new files or update the timestamp of existing files

SYNTAX - touch file

EXAMPLE - touch ~/Documents/newfile.txt


---

# 8) LN command

The ln command is used to create links between files

SYNTAX - ln source destination

EXAMPLE - ln -s ~/file.txt linkfile.txt


---

# 9) RM command

The rm command is used to delete files and directories

SYNTAX - rm file/directory

EXAMPLE - rm -rf ~/Documents/temp


---

# 10) MKDIR command

The mkdir command is used to create new directories

SYNTAX - mkdir directory

EXAMPLE - mkdir -p ~/Documents/newdir


---

# 11) RMDIR command

The rmdir command is used to delete empty directories

SYNTAX - rmdir directory

EXAMPLE - rmdir ~/Documents/newdir


---

# 12) FILE command

The file command is used to determine the type of a file

SYNTAX - file file

EXAMPLE - file ~/image.jpg


---

# 13) CHMOD command

The chmod command is used to change the permissions of files and directories

SYNTAX - chmod mode file/directory

EXAMPLE - chmod +x ~/script.sh


---

# 14) CHOWN command

The chown command is used to change the ownership of files and directories

SYNTAX - chown user:group file/directory

EXAMPLE - chown \$(whoami):\$(whoami) ~/Documents/file.txt


---

# 15) CHGRP command

The chgrp command is used to change the group ownership of a file or directory

SYNTAX - chgrp group file/directory

EXAMPLE - chgrp users ~/file.txt


---

# 16) CHATTR command

The chattr command is used to change the file attributes on a Linux file system

SYNTAX - chattr attribute file

EXAMPLE - chattr +i ~/file.txt


---

# 17) CAT command

The cat command is used to concatenate and display the contents of files

SYNTAX - cat file

EXAMPLE - cat ~/file.txt


---

# 18) TEE command

The tee command is used to redirect output to both a file and the terminal

SYNTAX - command | tee file

EXAMPLE - ls -l ~ | tee filelist.txt


---

# 19) HEAD command

The head command is used to display the first lines of a file

SYNTAX - head file

EXAMPLE - head -n5 ~/file.txt


---

# 20) TAIL command

The tail command is used to display the last lines of a file

SYNTAX - tail file

EXAMPLE - tail -n5 ~/file.txt


---

# 21) CUT command

The cut command is used to remove sections from each line of files

SYNTAX - cut file

EXAMPLE - cut -f1 -d, file.txt


---

# 22) GREP command

The grep command is used to search for patterns in text files

SYNTAX - grep pattern file

EXAMPLE - grep "error" /var/log/syslog


---

# 23) SED command

The sed command is used to perform basic text transformations on an input stream (a file or input from a pipeline)

SYNTAX - sed 'command' file

EXAMPLE - sed 's/hello/replaced-msg/g' file.txt


---

# 24) AWK command

The awk command is used for text processing and data extraction

SYNTAX - awk 'pattern { action }' file

EXAMPLE - awk '{print $1}' file.txt


---

# 25) LESS command

The less command is used to display the contents of a file one screen at a time

SYNTAX - less file

EXAMPLE - less hello.txt


---

# 26) WHO command

The who command is used to display information about logged-in users

SYNTAX - who

EXAMPLE - who


---

# 27) WHOAMI command

The whoami command is used to display the current user's username

SYNTAX - whoami

EXAMPLE - whoami


---

# 28) W command

The w command is used to display information about logged-in users and their processes

SYNTAX - w

EXAMPLE - w


---

# 29) LAST command

The last command is used to display information about previous logins

SYNTAX - last [username]

EXAMPLE - last


---

# 30) DF command

The df command is used to display the available and used disk space

SYNTAX - df [filesystem]

EXAMPLE - df -h


---

# 31) DU command

The du command is used to display the disk usage of files and directories

SYNTAX - du [file/directory]

EXAMPLE - du -sh $HOME


---

# 32) FIND command

The find command is used to search for files and directories

SYNTAX - find [path] [expression]

EXAMPLE - find $HOME -name "*.txt"


---

# 33) RSYNC command

The rsync command is used to efficiently transfer and synchronize files between machines

SYNTAX - rsync source destination

EXAMPLE - rsync -avP \~/file.txt user@192.168.1.100:\~


---

# 34) HEXDUMP command

The hexdump command is used to display the contents of a file in hexadecimal

SYNTAX - hexdump file

EXAMPLE - hexdump -C file.txt


---

# 35) TAR command

The tar command is used to create and extract archive files

SYNTAX - tar archive file/directory

EXAMPLE - tar -cvf archive.tar ~/Documents


---

# 36) GZIP command

The gzip command is used to compress and decompress files

SYNTAX - gzip file

EXAMPLE - gzip file.txt


---

# 37) GUNZIP command

The gunzip command is used to decompress files compressed with gzip

SYNTAX - gunzip file.gz

EXAMPLE - gunzip file.txt.gz


---

# 38) ZIP command

The zip command is used to create and manage zip archives

SYNTAX - zip archive.zip file/directory

EXAMPLE - zip -r archive.zip ~/Documents


---

# 39) UNZIP command

The unzip command is used to extract files from a zip archive

SYNTAX - unzip archive.zip

EXAMPLE - unzip archive.zip


---

# 40) PS command

The ps command is used to display information about running processes

SYNTAX - ps

EXAMPLE - ps aux


---

# 41) KILL command

The kill command is used to send signals to processes to terminate them

SYNTAX - kill pid

EXAMPLE - kill -9 1234


---

# 42) JOBS command

The jobs command is used to display the status of jobs in the current shell

SYNTAX - jobs

EXAMPLE - jobs


---

# 43) TOP command

The top command is used to display real-time information about running processes

SYNTAX - top

EXAMPLE - top


---

# 44) NICE command

The nice command is used to set the priority of a process

SYNTAX - nice command

EXAMPLE - nice -n 10 firefox


---

# 45) RENICE command

The renice command is used to change the priority of an already running process

SYNTAX - renice pid

EXAMPLE - renice -n 10 1234


---

# 46) FREE command

The free command is used to display the amount of free and used memory in the system

SYNTAX - free

EXAMPLE - free -h


---

# 47) UPTIME command

The uptime command is used to display the system uptime and load averages

SYNTAX - uptime

EXAMPLE - uptime


---

# 48) TIME command

The time command is used to measure the elapsed time of a command

SYNTAX - time [command]

EXAMPLE - time ls -l /


---

# 49) CMP command

The cmp command is used to compare two files byte-by-byte

SYNTAX - cmp file1 file2

EXAMPLE - cmp file1.txt file2.txt


---

# 50) DIFF command

The diff command is used to compare the contents of two files

SYNTAX - diff file1 file2

EXAMPLE - diff -u file1.txt file2.txt


---

# 51) HISTORY command

The history command is used to display the command history

SYNTAX - history

EXAMPLE - history


---

# 52) DMESG command

The dmesg command is used to display the kernel logs from the moment OS has started

SYNTAX - dmesg

EXAMPLE - sudo dmesg


---
