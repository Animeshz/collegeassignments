#include <iostream>
#include <thread>

using namespace std;

int shared_resource = 200000;

void work_1() {
    for (int i = 0; i < 100000; i++) {
        shared_resource--;
    }
}

void work_2() {
    for (int i = 0; i < 100000; i++) {
        shared_resource--;
    }   
}

int main() {
    thread t1(work_1);
    thread t2(work_2);

    t1.join();
    t2.join();

    cout << "The value of the shared resource after 100000 subtractions is: " << shared_resource << endl;

    return 0;
}
