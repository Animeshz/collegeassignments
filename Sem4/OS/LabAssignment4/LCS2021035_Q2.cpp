#include <iostream>
#include <thread>
#include <mutex>
#include <semaphore.h>

using namespace std;

int shared_resource = 200000;
mutex m;
sem_t sem;

void work_1() {
    for (int i = 0; i < 100000; i++) {
        sem_wait(&sem);
        m.lock();
        shared_resource--;
        m.unlock();
        sem_post(&sem);
    }
}

void work_2() {
    for (int i = 0; i < 100000; i++) {
        sem_wait(&sem);
        m.lock();
        shared_resource--;
        m.unlock();
        sem_post(&sem);
    }
}

int main() {
    sem_init(&sem, 0, 1);
    thread t1(work_1);
    thread t2(work_2);

    t1.join();
    t2.join();

    sem_destroy(&sem);
    cout << "The value of the shared resource after 100000 subtractions is: " << shared_resource << endl;

    return 0;
}
