#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <semaphore.h>

const int BUFFER_SIZE = 5;
std::vector<int> buffer(BUFFER_SIZE);

sem_t empty;
sem_t full;
sem_t mutex;

void producer() {
    for (int i = 1; i <= 25; i++) {
        sem_wait(&empty);
        sem_wait(&mutex);
        buffer[i % BUFFER_SIZE] = i;
        std::cout << "Produced item: " << i << std::endl;
        sem_post(&mutex);
        sem_post(&full);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void consumer() {
    for (int i = 1; i <= 25; i++) {
        sem_wait(&full);
        sem_wait(&mutex);
        int item = buffer[i % BUFFER_SIZE];
        std::cout << "Consumed item: " << item << std::endl;
        sem_post(&mutex);
        sem_post(&empty);
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
    }
}

int main() {
    sem_init(&empty, 0, BUFFER_SIZE);
    sem_init(&full, 0, 0);
    sem_init(&mutex, 0, 1);

    std::thread t1(producer);
    std::thread t2(consumer);

    t1.join();
    t2.join();

    sem_destroy(&empty);
    sem_destroy(&full);
    sem_destroy(&mutex);

    return 0;
}
