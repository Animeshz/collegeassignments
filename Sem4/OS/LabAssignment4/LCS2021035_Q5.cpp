#include <stdio.h>
#include <stdbool.h>

const int NUM_PROCESSES = 4;
const int NUM_RESOURCES = 3;

int available[NUM_RESOURCES] = {3, 3, 2};
int allocation[NUM_PROCESSES][NUM_RESOURCES] = {
    {0, 1, 0},
    {2, 0, 0},
    {3, 0, 2},
    {2, 1, 1},
};

int max[NUM_PROCESSES][NUM_RESOURCES] = {
    {7, 5, 3},
    {3, 2, 2},
    {9, 0, 2},
    {2, 2, 2},
};

int need[NUM_PROCESSES][NUM_RESOURCES];
bool finish[NUM_PROCESSES] = {false};
int safe_sequence[NUM_PROCESSES];

bool is_safe() {
    int work[NUM_RESOURCES];
    for (int i = 0; i < NUM_RESOURCES; i++) {
        work[i] = available[i];
    }

    int count = 0;
    while (count < NUM_PROCESSES) {
        bool found = false;
        for (int i = 0; i < NUM_PROCESSES; i++) {
            if (!finish[i]) {
                bool can_finish = true;
                for (int j = 0; j < NUM_RESOURCES; j++) {
                    if (need[i][j] > work[j]) {
                        can_finish = false;
                        break;
                    }
                }
                if (can_finish) {
                    for (int j = 0; j < NUM_RESOURCES; j++) {
                        work[j] += allocation[i][j];
                    }
                    finish[i] = true;
                    safe_sequence[count] = i;
                    count++;
                    found = true;
                }
            }
        }
        if (!found) {
            return false;
        }
    }
    return true;
}

int main() {
    for (int i = 0; i < NUM_PROCESSES; i++) {
        for (int j = 0; j < NUM_RESOURCES; j++) {
            need[i][j] = max[i][j] - allocation[i][j];
        }
    }

    if (is_safe()) {
        printf("Safe sequence: ");
        for (int i = 0; i < NUM_PROCESSES; i++) {
            printf("P%d ", safe_sequence[i]);
        }
        printf("\n");
    }
    else {
        printf("Unsafe state\n");
    }

    return 0;
}
