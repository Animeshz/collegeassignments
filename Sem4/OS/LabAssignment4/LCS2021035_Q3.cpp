#include <iostream>
#include <thread>
#include <atomic>

using namespace std;

int shared_resource = 200000;
atomic<int> turn;
atomic<bool> flag[2];

void work_1() {
    flag[0] = true;
    turn = 1;
    while (flag[1] && turn == 1);
    for (int i = 0; i < 100000; i++) {
        shared_resource--;
    }
    flag[0] = false;
}

void work_2() {
    flag[1] = true;
    turn = 0;
    while (flag[0] && turn == 0);
    for (int i = 0; i < 100000; i++) {
        shared_resource--;
    }
    flag[1] = false;
}

int main() {
    thread t1(work_1);
    thread t2(work_2);

    t1.join();
    t2.join();

    cout << "The value of the shared resource is: " << shared_resource << endl;

    return 0;
}
