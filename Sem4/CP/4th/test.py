from typing import List
from collections import deque

class Solution:
    def minReverseOperations(self, n: int, p: int, banned: List[int], k: int) -> List[int]:
        banned_set = set(banned)
        arr = [1 if i == p else 0 for i in range(n)]
        queue = deque([(arr, p, 0)])
        ans = [-1 for _ in range(n)]

        while queue:
            arr, i, d = queue.popleft()
            if ans[i] != -1:
                continue

            ans[i] = d
            for j in range(max(0, i-k+1), i+1):
                if arr[j:j+k].count(1) != 1:
                    continue

                sr = arr[j:j+k]
                sr.reverse()
                new = arr[:j] + sr + arr[j+k:]
                if all(new[j] != 1 or j in banned_set for j in range(n)):
                    queue.append((new, j, d+1))

        return ans 