pub fn check_valid_grid(grid: Vec<Vec<i32>>) -> bool {
    let mut indexes: Vec<(usize, usize)> = vec![(0, 0); grid.len() * grid.len()];
    for (col, ele) in grid.iter().enumerate() {
        for (row, ele) in ele.iter().enumerate() {
            indexes[*ele as usize] = (row, col);
        }
    }
    if indexes[0] != (0, 0) {
        return false;
    }
    let mut answer = true;
    for index in 0..(indexes.len() - 1) {
        if 5 != (indexes[index].0 - indexes[index + 1].0)
            * (indexes[index].0 - indexes[index + 1].0)
            + (indexes[index].1 - indexes[index + 1].1) * (indexes[index].1 - indexes[index + 1].1)
        {
            answer = false;
        }
    }
    answer
}

fn main() {}
