#include <bits/stdc++.h>
using namespace std;

int main() {
  int n = 886996;
bool positive = true;
        int answer = 0;
        int i = 0;
        while (n) {
            auto msb = n % 10;
            answer += msb * (positive ? 1 : -1);
            positive = !positive;
            n /= 10;
            i++;
        }

        if (i % 2 == 0) answer = -answer;
        cout << answer << endl;
  return 0;
}

// class Solution {
// public:
//     int alternateDigitSum(int n) {
//         bool positive = true;
//         int answer = 0;
//         int i = 0;
//         while (n) {
//             auto msb = n % 10;
//             answer += msb * (positive ? 1 : -1);
//             positive = !positive;
//             n /= 10;
//             i++;
//         }
//
//         if (i % 2 != 0) answer = -answer;
//         return answer;
//     }
// };
