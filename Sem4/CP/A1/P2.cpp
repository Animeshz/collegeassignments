#include <bits/stdc++.h>
using namespace std;


#if !defined(ONLINE_JUDGE) && 1
#define dbg(x) cerr << #x <<" "; _print(x); cerr << endl;
#else
#define dbg(x)
#endif

template <class T> void _print(T t) {cerr << t;}
void _print(string t) {cerr << '"' << t << '"';}
template <class T, class V> void _print(pair <T, V> p) {cerr << "{"; _print(p.first); cerr << ","; _print(p.second); cerr << "}";}
template <class T> void _print(vector <T> v) {cerr << "[ "; for (T i : v) {_print(i); cerr << " ";} cerr << "]"<<endl;}
template <class T> void _print(set <T> v) {cerr << "[ "; for (T i : v) {_print(i); cerr << " ";} cerr << "]";}
template <class T> void _print(multiset <T> v) {cerr << "[ "; for (T i : v) {_print(i); cerr << " ";} cerr << "]";}
template <class T, class V> void _print(map <T, V> v) {cerr << "[ "; for (auto i : v) {_print(i); cerr << " ";} cerr << "]";}

int main() {
  vector<vector<int>> score = {{10,6,9,1},{7,5,11,2},{4,8,3,15}};
  int k = 2;
int n = score.size();
        int m = score[0].size();

        for (int i = n-1; i > 0; i--) {
            for (int j = n-1; j > 0; j--)
            if (score[j-1][k] < score[j][k])
                score[j-1].swap(score[j]);
        }

        dbg(score);
  return 0;
}
