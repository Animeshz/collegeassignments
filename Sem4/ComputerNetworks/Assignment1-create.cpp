#include <iostream>
using namespace std;

int main() {
  string s;
  cout << "input a binary sequence: ";
  cin >> s;

  int ones = 0;
  for (int i = 0; i < s.size(); i++) {
    if (s[i] == '1') ones++;
    else if (s[i] == '0') continue;
    else {
      cout << "Invalid sequence" << endl;
      return 1;
    }
  }

  cout << "Create Even parity [enter 0] or Odd parity [enter 1]: ";
  bool odd_parity;
  cin >> odd_parity;

  cout << endl;
  cout << s;
  if (odd_parity ^ (ones % 2 == 0)) {
    cout << 0 << endl;
  } else {
    cout << 1 << endl;
  }
}
