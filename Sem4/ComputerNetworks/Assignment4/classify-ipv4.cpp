#include <bits/stdc++.h>
using namespace std;

int main() {
  int a, b, c, d;
  printf("Input an IPv4 addr: ");
  scanf("%d.%d.%d.%d", &a, &b, &c, &d);

  if (!(0 <= a && a <= 255) || !(0 <= b && b <= 255) || !(0 <= b && b <= 255) || !(0 <= d && d <= 255)) {
    cout << "Invalid IP Address" << endl;
    exit(1);
  }

  if (0 <= a && a <= 127) {
    cout << "Given IPv4 Address is Class A IP Address" << endl;
  } else if (128 <= a && a <= 191) {
    cout << "Given IPv4 Address is Class B IP Address" << endl;
  } else if (192 <= a && a <= 223) {
    cout << "Given IPv4 Address is Class C IP Address" << endl;
  } else if (224 <= a && a <= 239) {
    cout << "Given IPv4 Address is Class D IP Address" << endl;
  } else if (240 <= a && a <= 255) {
    cout << "Given IPv4 Address is Class E IP Address" << endl;
  }
  return 0;
}
