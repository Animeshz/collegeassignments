#include <bits/stdc++.h>
using namespace std;

string bin(int in) {
  if (in > 255 || in < 0) {
    cout << "invalid ipv4 address, each bit must be in range of [0, 255]" << endl;
    exit(1);
  }

  string ans;
  for (int i = 0; i < 8; i++) {
    ans.append(in%2 ? "1" : "0");
    in /= 2;
  }

  int n = ans.size();
  for (int i = 0; i < n/2; i++) {
    char tmp = ans[i];
    ans[i] = ans[n-1-i];
    ans[n-1-i] = tmp;
  }
  return ans;
}

int main() {
  int a, b, c, d;
  printf("Input an Ipv4 addr: ");
  scanf("%d.%d.%d.%d", &a, &b, &c, &d);

  cout << bin(a) << '.' << bin(b) << '.' << bin(c) << '.' << bin(d) << endl;
  return 0;
}
