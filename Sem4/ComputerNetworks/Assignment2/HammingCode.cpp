#include <bits/stdc++.h>
using namespace std;

// 2^r-r >= m+1
int required_bits(int input_size) {
  int r = 0, pwr = 1;
  while (pwr < r+input_size+1) {
    r++, pwr *= 2;
  }
  return r;
}

string encode(string input) {
  input = string(input.rbegin(), input.rend());
  int m = input.size();
  int no_of_bits = required_bits(m);

  for (int i = 0; i < no_of_bits; i++) {
    input.insert((1<<i)-1, "0");
  }
  m = input.size();

  for (int i = 0; i < no_of_bits; i++) {
    int cnt_of_ones = 0;
    for (int j = 0; j < m; j++) {
      if ((j+1) & (1<<i)) {
        if (input[j] == '1') cnt_of_ones++;
      }
    }

    if (cnt_of_ones % 2 == 1) {
      input[(1<<i)-1] = '1';
    }
  }

  input = string(input.rbegin(), input.rend());
  return input;
}

int main() {
  string input = "1010";
  cout << encode(input) << endl;

  return 0;
}
