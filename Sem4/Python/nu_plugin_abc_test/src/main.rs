use std::vec;

use chrono::{DateTime, NaiveDateTime};
use nu_plugin::{serve_plugin, EvaluatedCall, JsonSerializer, LabeledError, Plugin};
use nu_protocol::{Category, PluginSignature, Spanned, SyntaxShape, Value};
struct Query;
impl Plugin for Query {
    fn signature(&self) -> Vec<PluginSignature> {
        vec![
            // PluginSignature::build("gmail")
            //     .usage("Perform operations on gmail")
            //     .category(Category::Filters),
            // PluginSignature::build("gmail list")
            //     .usage("List all emails")
            //     .optional(
            //         "user_id",
            //         SyntaxShape::Int,
            //         "User identifier (n'th account logged into browser)",
            //     )
            //     .switch("force-reload", "Force invalidate cache", Some('f'))
            //     .category(Category::Network),
            // PluginSignature::build("gmail open")
            //     .usage("Open set of email(s)")
            //     .category(Category::Network),
            // PluginSignature::build("gmail archive")
            //     .usage("Archive set of email(s)")
            //     .category(Category::Network),
            // PluginSignature::build("gmail delete")
            //     .usage("Delete set of email(s)")
            //     .category(Category::Network),
            
            PluginSignature::build("discord")
                .usage("Perform operations on discord")
                .category(Category::Filters),
            PluginSignature::build("discord list")
                .usage("List all user list")
                .category(Category::Network),
            PluginSignature::build("discord send")
                .usage("Send messsage to one or more discord ids")
                .required("msg", SyntaxShape::String, "The message to be sent".to_string())
                .category(Category::Network),

        ]
    }

    fn run(
        &mut self,
        name: &str,
        call: &EvaluatedCall,
        _input: &Value,
    ) -> Result<Value, LabeledError> {
        // You can use the name to identify what plugin signature was called
        let _path: Option<Spanned<String>> = call.opt(0)?;

        match name {
            "gmail" => {
                let val = Value::Record {
                    cols: vec!["name".to_string(), "from".to_string(), "subject".to_string(), "date".to_string(), "permalink".to_string()],
                    vals: vec![Value::String { val: "ABC".to_string(), span: call.head }, Value::String { val: "ABC".to_string(), span: call.head }, Value::String { val: "ABC".to_string(), span: call.head }, Value::String { val: "ABC".to_string(), span: call.head }, Value::String { val: "ABC".to_string(), span: call.head }],
                    span: call.head
                };
                
                let val = Value::Date { val: chrono::offset::Local::now().into(), span: call.head };
                Ok(val)
                //     Value::Nothing { span: () }
                // Ok()
            },
            // "query" => {
            //     self.query(name, call, input, path)
            // }
            // "query json" => self.query_json( name, call, input, path),
            // "query web" => self.query_web(name, call, input, path),
            // "query xml" => self.query_xml(name, call, input, path),
            _ => Err(LabeledError {
                label: "Plugin call with wrong name signature".into(),
                msg: "the signature used to call the plugin does not match any name in the plugin signature vector".into(),
                span: Some(call.head),
            }),
        }
    }
}
fn main() {
    serve_plugin(&mut Query {}, JsonSerializer {})
}
