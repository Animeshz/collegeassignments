# Define the employee data dictionary
employees = {
    "1": {
        "first_name": "Animesh",
        "last_name": "Sahu",
        "address": "Bilaspur, CG",
        "past_jobs": ["Software Eng"]
    },
    "2": {
        "first_name": "Brahad",
        "last_name": "Kokad",
        "address": "Pune, MH",
        "past_jobs": ["Photographer", "Music Producer"]
    }
}

# Function to add a new employee record
def add_employee():
    emp_id = input("Enter employee ID: ")
    first_name = input("Enter first name: ")
    last_name = input("Enter last name: ")
    address = input("Enter address: ")
    past_jobs = input("Enter past jobs (comma separated): ").split(",")
    employees[emp_id] = {
        "first_name": first_name,
        "last_name": last_name,
        "address": address,
        "past_jobs": past_jobs
    }
    print("Employee added successfully!")

# Function to update an existing employee record
def update_employee():
    emp_id = input("Enter employee ID to update: ")
    if emp_id in employees:
        field = input("Enter field to update (first_name, last_name, address, past_jobs): ")
        value = input("Enter new value: ")
        employees[emp_id][field] = value
        print("Employee record updated successfully!")
    else:
        print("Employee not found.")

# Function to delete an existing employee record
def delete_employee():
    emp_id = input("Enter employee ID to delete: ")
    if emp_id in employees:
        del employees[emp_id]
        print("Employee record deleted successfully!")
    else:
        print("Employee not found.")

# Function to print all employee records
def print_employees():
    for emp_id, emp_data in employees.items():
        print("Employee ID:", emp_id)
        print("First Name:", emp_data["first_name"])
        print("Last Name:", emp_data["last_name"])
        print("Address:", emp_data["address"])
        print("Past Jobs:", ", ".join(emp_data["past_jobs"]))
        print("--------------------")

# Main program loop
while True:
    print("\nEmployee Record Management System")
    print("1. Add New Employee")
    print("2. Update Employee")
    print("3. Delete Employee")
    print("4. Print All Employees")
    print("5. Quit")

    choice = input("Enter your choice (1-5): ")
    if choice == "1":
        add_employee()
    elif choice == "2":
        update_employee()
    elif choice == "3":
        delete_employee()
    elif choice == "4":
        print_employees()
    elif choice == "5":
        print("Thank you for using the Employee Record Management System!")
        break
    else:
        print("Invalid choice. Please try again.")
