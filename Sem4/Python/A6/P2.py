# Define the owner data list
owner_data = []

# Function to add a new owner record
def add_owner():
    try:
        house_num = int(input("Enter house number: "))
        owner_name = input("Enter owner name: ")
        phone_num = input("Enter phone number: ")
        email_id = input("Enter email id: ")
        owner_data.append((house_num, owner_name, phone_num, email_id))
        print("Owner record added successfully!")
    except ValueError:
        print("Invalid house number. Please enter a valid integer.")

# Function to display all owner records sorted by house number
def display_owners():
    if not owner_data:
        print("No owner records found.")
    else:
        owner_data.sort(key=lambda x: x[0])
        print("House Number\tOwner Name\tPhone Number\tEmail Id")
        for owner in owner_data:
            print(owner[0], "\t\t", owner[1], "\t\t", owner[2], "\t\t", owner[3])

# Main program loop
while True:
    print("\nSociety Owner Record Management System")
    print("1. Add New Owner Record")
    print("2. Display All Owner Records (sorted by house number)")
    print("3. Quit")

    try:
        choice = int(input("Enter your choice (1-3): "))
        if choice == 1:
            add_owner()
        elif choice == 2:
            display_owners()
        elif choice == 3:
            print("Thank you for using the Society Owner Record Management System!")
            break
        else:
            print("Invalid choice. Please try again.")
    except ValueError:
        print("Invalid choice. Please enter a valid integer.")

