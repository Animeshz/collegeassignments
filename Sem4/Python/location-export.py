#!/usr/bin/env python3

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
import argparse
import re
import shutil
import time
import os
import csv
import sys


def search_place(rangen, wait):
    min, max = rangen
    upper_lim = max - min

    min -= 1
    while min > 0:
        next_pg = browser.find_element(By.XPATH, '//button[@aria-label=" Next page "]')
        try:
            # overlay click
            next_pg.click()
        except:
            pass
        min -= 1
        time.sleep(0.05)

    links = []
    while upper_lim > 0:
        WebDriverWait(browser, wait).until(EC.presence_of_element_located((By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[1]')))
        search_results = browser.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]/div/div/div[2]/div[1]')
        for i in range(20):
            browser.execute_script('arguments[0].scrollTo(0, arguments[0].scrollHeight);', search_results);
            time.sleep(0.05)

        link_elements = search_results.find_elements(By.CSS_SELECTOR, "a")
        links += [link.get_attribute('href') for link in link_elements]

        next_pg = browser.find_element(By.XPATH, '//button[@aria-label=" Next page "]')
        try:
            next_pg.click()
        except:
            pass
        upper_lim -= 1
        time.sleep(0.05)

    return [re.sub("(\"|')", "\\\\thevalue", link) for link in links]

def extract_place_info(links, wait):
    result = []
    for link in links:
        browser.execute_script(f"window.open('{link}');")
        browser.switch_to.window(browser.window_handles[-1])

        WebDriverWait(browser, wait).until(EC.presence_of_element_located((By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]')))
        panel = browser.find_element(By.XPATH, '//*[@id="QA0Szd"]/div/div/div[1]/div[2]/div/div[1]')

        for i in range(4):
            browser.execute_script('arguments[0].scrollTo(0, arguments[0].scrollHeight);', panel);
            time.sleep(0.05)


        name = panel.find_element(By.XPATH, '//h1[contains(@class, "fontHeadlineLarge")]/span').get_attribute('innerHTML')
        address = panel.find_element(By.XPATH, '//button[@data-item-id="address"]/div[1]/div[2]/div[1]').get_attribute('innerHTML')

        website = ""
        contact = ""
        review = ""
        review_count = ""

        try:
            contact = panel.find_element(By.XPATH, '//button[starts-with(@data-item-id, "phone:tel:")]/div[1]/div[2]/div[1]').get_attribute('innerHTML').replace(' ', '')
        except:
            pass

        try:
            website = panel.find_element(By.XPATH, '//button[@data-item-id="authority"]/div[1]/div[2]/div[1]').get_attribute('innerHTML')
        except:
            pass

        try:
            review_element = panel.find_element(By.CSS_SELECTOR, '.jANrlb')
            review = review_element.find_element(By.TAG_NAME, 'div').get_attribute('innerHTML')
            review_count = review_element.find_element(By.CSS_SELECTOR, 'button').get_attribute('innerHTML').replace(',', '')
        except:
            pass

        result.append((name, address, link, website, contact, review, review_count))

        browser.close()
        browser.switch_to.window(browser.window_handles[-1])
    return result


def require(name):
    abs_path = shutil.which(name)
    if not abs_path:
        print(f'Required binary {name} does not exist in the system.')
        exit(1)
    return abs_path

def parse_range(string):
    m = re.match(r'^(\d+)(?:-(\d+))?$', string)
    if not m:
        raise argparse.ArgumentTypeError("'" + string + "' is not a range of number. Expected forms like '0-5' or '2'.")
    start = m.group(1)
    end = m.group(2) or start
    return (int(start,10), int(end,10)+1)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Maps Result Export', usage='location-export [-h] [-r RANGE] -- <url> <file>')
    parser.add_argument('-r', '--range', type=parse_range, help='range of result pages to export, indexed from 1, e.g. 1-5')
    parser.add_argument('url', help='link from where to export from')
    parser.add_argument('file', help='csv-file to export to')
    args = parser.parse_args()

    brave = require('brave-browser-stable')
    # brave = require('chromium')

    option = webdriver.ChromeOptions()
    # option.binary_location = brave
    # option.add_argument('--headless')
    # option.add_argument("--user-data-dir=/home/animesh/.config/chromium")
    option.add_experimental_option("debuggerAddress", "127.0.0.1:9222")
    # option.add_argument("--user-data-dir=/home/animesh/.config/BraveSoftware/Brave-Browser")
    print(option)
    print(dir(option))

    browser = webdriver.Chrome(options=option)
    main_window = browser.window_handles[0]

    browser.get(args.url)


    WebDriverWait(browser, 5).until(EC.presence_of_element_located((By.XPATH, '//input[@name="email"]')))
    search_results = browser.find_element(By.XPATH, '//input[@name="email"]')
    print(search_results)


    time.sleep(100)
    sys.exit(0)

    links = search_place(args.range or (1, 2), wait=5)
    extracts = extract_place_info(links, wait=5)

    with open(args.file, 'w') as out:
        csv_out=csv.writer(out)
        csv_out.writerow(['name','address', 'link', 'website', 'contact', 'review', 'review_count'])
        for row in extracts:
            csv_out.writerow(row)

    browser.close()
