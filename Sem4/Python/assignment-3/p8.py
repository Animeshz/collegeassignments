import re

input_gradesheet = '''
Name: Rohit Sharma
Roll Number: R17234512  SAPID: 50005673
Sem: 1                  Course: B.Tech. CSE AI&ML

Subject name: Marks
PDS:        70
Python:     80
Chemistry:  90
English:    60
Physics:    50

Percentage: 70%
CGPA:7.0
Grade: A
'''

PDS = int(re.search('PDS:\s+(\d+)', input_gradesheet).group(1))
Python = int(re.search('Python:\s+(\d+)', input_gradesheet).group(1))
Chemistry = int(re.search('Chemistry:\s+(\d+)', input_gradesheet).group(1))
English = int(re.search('English:\s+(\d+)', input_gradesheet).group(1))
Physics = int(re.search('Physics:\s+(\d+)', input_gradesheet).group(1))

percentage=((PDS+Python+Chemistry+English+Physics)/500)*100
cgpa=percentage/10

if cgpa>=9:
    grade="O"
elif cgpa>=8:
    grade="A+"
elif cgpa>=7:
    grade="A"
elif cgpa>=6:
    grade="B+"
elif cgpa>=5:
    grade="B"
elif cgpa>=3.5:
    grade="C+"
else:
    grade="F"

cgpa = round(cgpa, 1)
print(f"Percentage: {percentage}%")
print(f"CGPA: {cgpa}")
print(f"Grade: {grade}")
