import math
import cmath
import sys

print("For the quadratic equation: ax^2 + bx + c,")
a = int(input('Enter a: '))
b = int(input('Enter b: '))
c = int(input('Enter c: '))

if a == 0:
    print("Coefficient of x^2 can't be 0")
    sys.exit(1)

discriminant = b * b - 4 * a * c

if discriminant >= 0:
    print("Equation has real roots,")
    delta = math.sqrt(discriminant)
else:
    print("Equation has imaginary roots,")
    delta = cmath.sqrt(discriminant)

print((-b + delta) / (2 * a), "and",(-b - delta) / (2 * a))

