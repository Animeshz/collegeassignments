seconds = int(input("Input number of seconds: "))

hours = seconds // 3600
minutes = (seconds // 60) % 60
secs = seconds % 60

print(f"{hours}:{minutes}:{secs}")
