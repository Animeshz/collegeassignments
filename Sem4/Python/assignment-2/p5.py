import math

principal = int(input("Input principal value: "))
rate = int(input("Input rate per annum: "))
time = int(input("Input time in yrs: "))

simple_interest = principal * rate * time / 100

print("Simple Interest is calculated to be:", simple_interest)
