import math

a = int(input("Input length of side_1 of triangle: "))
b = int(input("Input length of side_2 of triangle: "))
c = int(input("Input length of side_3 of triangle: "))

semi_perimeter = (a+b+c)/2
area = math.sqrt(semi_perimeter * (semi_perimeter - a) * (semi_perimeter - b) * (semi_perimeter - c))

print("Area of triangle is calculated to be:", area)
