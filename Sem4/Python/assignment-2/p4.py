import math

x = int(input("Input base of right angled triangle: "))
y = int(input("Input perpendicular of right angled triangle: "))

z = math.sqrt(x*x + y*y)

print("Hypotenuse of right angled triangle:", z)
