operators = ['&', '|', '^']

for op in operators:
  print(f'Truth Table for "{op}":')
  print(f'a   b = a{op}b')
  print('-----------')
  for a in range(2):
    for b in range(2):
      exec(f'''
z = {a} {op} {b}
print('{a} {op} {b} =', z)
      ''')
  print()
