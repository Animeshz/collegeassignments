import math

radius = int(input("Input radius of circle: "))
area = math.pi * radius * radius

print("Area of circle:", area)
