#!/usr/bin/env python3
# j. How do I center widgets in tkinter?
import tkinter as tk

# create main window
root = tk.Tk()

# create a label and center it horizontally
label = tk.Label(root, text="Hello, World!", width=20)
label.pack(side=tk.TOP, pady=50)

# create a button and center it horizontally
button = tk.Button(root, text="Click me!")
button.pack(side=tk.TOP, pady=10)

# start the main event loop
root.mainloop()
