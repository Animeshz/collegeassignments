#!/usr/bin/env python3
# 2. Create a tkinter program that allows users to select a file from their computer and display the contents of the file in a text box.
# You can use the tkinter.filedialog module to create a file dialog box.

import tkinter as tk
from tkinter import filedialog

# create main window
root = tk.Tk()

# function to open a file and display its contents
def open_file():
    file_path = filedialog.askopenfilename()
    with open(file_path) as file:
        contents = file.read()
        text_box.delete("1.0", tk.END)
        text_box.insert(tk.END, contents)

# create a button to open a file
open_button = tk.Button(root, text="Open File", command=open_file)
open_button.pack()

# create a text box to display the file contents
text_box = tk.Text(root)
text_box.pack()

# start the main event loop
root.mainloop()
