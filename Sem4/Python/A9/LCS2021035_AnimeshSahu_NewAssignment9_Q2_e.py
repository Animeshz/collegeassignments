#!/usr/bin/env python3
# e. How do I create a scrollable list in tkinter?

import tkinter as tk

# create main window
root = tk.Tk()

# create a list of items
items = ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7", "Item 8", "Item 9", "Item 10"]

# create a scrollbar
scrollbar = tk.Scrollbar(root)

# create a listbox
listbox = tk.Listbox(root, yscrollcommand=scrollbar.set)

# add the items to the listbox
for item in items:
    listbox.insert(tk.END, item)

# pack the scrollbar and listbox to display them on the window
scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
listbox.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

# configure the scrollbar
scrollbar.config(command=listbox.yview)

# start the main event loop
root.mainloop()
