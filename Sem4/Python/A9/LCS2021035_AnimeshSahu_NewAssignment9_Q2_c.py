#!/usr/bin/env python3
# c. How do I create a text box in tkinter?

import tkinter as tk

# create main window
root = tk.Tk()

# create a text box
text_box = tk.Text(root)

# pack the text box to display it on the window
text_box.pack()

# start the main event loop
root.mainloop()
