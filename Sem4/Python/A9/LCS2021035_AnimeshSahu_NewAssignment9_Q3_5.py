#!/usr/bin/env python3
# 5. Create a tkinter program that allows users to select a color from a color picker and change the background color of the window. You
# can use the tkinter.colorchooser module to create a color picker dialog box.

import tkinter as tk
import tkinter.colorchooser as colorchooser

# create main window
root = tk.Tk()

# function to choose a color and change the background color of the window
def choose_color():
    color = colorchooser.askcolor()[1]
    root.config(bg=color)

# create a button to choose a color
color_button = tk.Button(root, text="Choose Color", command=choose_color)
color_button.pack()

# start the main event loop
root.mainloop()
