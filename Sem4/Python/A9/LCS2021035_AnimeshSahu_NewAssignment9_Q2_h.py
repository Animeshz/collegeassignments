#!/usr/bin/env python3
# h. How do I change the font size and style in tkinter?

import tkinter as tk

# create main window
root = tk.Tk()

# create a label with a custom font size and style
label = tk.Label(root, text="Hello, World!", font=("Arial", 24, "bold"))

# pack the label to display it on the window
label.pack()

# start the main event loop
root.mainloop()
