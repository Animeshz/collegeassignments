#!/usr/bin/env python3
# b. How do I create a label in tkinter?

import tkinter as tk

# create main window
root = tk.Tk()

# create a label
label = tk.Label(root, text="Hello, World!")

# pack the label to display it on the window
label.pack()

# start the main event loop
root.mainloop()
