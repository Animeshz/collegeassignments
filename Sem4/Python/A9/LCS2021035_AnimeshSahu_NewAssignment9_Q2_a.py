#!/usr/bin/env python3
# a. How do I create a button in tkinter?

import tkinter as tk

# create main window
root = tk.Tk()

# create a button
button = tk.Button(root, text="Click me!")

# pack the button to display it on the window
button.pack()

# start the main event loop
root.mainloop()
