#!/usr/bin/env python3
# g. How do I create a canvas in tkinter?

import tkinter as tk

# create main window
root = tk.Tk()

# create a canvas
canvas = tk.Canvas(root, width=200, height=200)

# create a rectangle on the canvas
rect = canvas.create_rectangle(50, 50, 150, 150, fill="blue")

# pack the canvas to display it on the window
canvas.pack()

# start the main event loop
root.mainloop()
