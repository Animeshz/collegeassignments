#!/usr/bin/env python3
# 3. Create a tkinter program that allows users to enter a URL in a text box, and then displays the HTML source code of the webpage in a
# text box. You can use the requests module to get the HTML source code of the webpage.

import tkinter as tk
import requests

# create main window
root = tk.Tk()

# function to get the HTML source code of a webpage
def get_html():
    url = url_entry.get()
    response = requests.get(url)
    html = response.text
    text_box.delete("1.0", tk.END)
    text_box.insert(tk.END, html)

# create a label and text box for entering the URL
url_label = tk.Label(root, text="Enter URL:")
url_label.pack()
url_entry = tk.Entry(root)
url_entry.pack()

# create a button to get the HTML source code
get_button = tk.Button(root, text="Get HTML", command=get_html)
get_button.pack()

# create a text box to display the HTML source code
text_box = tk.Text(root)
text_box.pack()

# start the main event loop
root.mainloop()
