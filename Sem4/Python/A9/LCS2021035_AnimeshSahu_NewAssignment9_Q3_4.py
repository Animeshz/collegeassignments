#!/usr/bin/env python3
# 4. Create a tkinter program that allows users to draw on a canvas using the mouse. You can use the tkinter.Canvas widget and the bind
# method to detect mouse events.

import tkinter as tk

# create main window
root = tk.Tk()

# create a canvas to draw on
canvas = tk.Canvas(root, width=500, height=500, bg="white")
canvas.pack()

# function to draw on the canvas
def draw(event):
    x, y = event.x, event.y
    canvas.create_oval(x-2, y-2, x+2, y+2, fill="black")

# bind mouse events to the canvas
canvas.bind("<B1-Motion>", draw)

# start the main event loop
root.mainloop()
