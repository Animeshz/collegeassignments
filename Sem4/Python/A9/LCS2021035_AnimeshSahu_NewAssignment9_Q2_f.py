#!/usr/bin/env python3
# f. How do I create a message box in tkinter?

import tkinter as tk
import tkinter.messagebox as messagebox

# create main window
root = tk.Tk()

# define a function to show the message box
def show_message_box():
    messagebox.showinfo("Title", "This is a message box.")

# create a button to show the message box
button = tk.Button(root, text="Show Message Box", command=show_message_box)

# pack the button to display it on the window
button.pack()

# start the main event loop
root.mainloop()
