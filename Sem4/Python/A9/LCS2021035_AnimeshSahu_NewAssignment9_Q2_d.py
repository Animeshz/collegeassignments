#!/usr/bin/env python3
# d. How do I create a drop-down menu in tkinter?

import tkinter as tk

# create main window
root = tk.Tk()

# create a list of options
options = ["Option 1", "Option 2", "Option 3"]

# create a variable to store the selected option
selected_option = tk.StringVar(root)

# set the default option
selected_option.set(options[0])

# create a drop-down menu
drop_down_menu = tk.OptionMenu(root, selected_option, *options)

# pack the drop-down menu to display it on the window
drop_down_menu.pack()

# start the main event loop
root.mainloop()
