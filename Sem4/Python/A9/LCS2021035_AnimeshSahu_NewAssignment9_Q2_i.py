#!/usr/bin/env python3
# i. How do I change the color of widgets in tkinter?

import tkinter as tk

# create main window
root = tk.Tk()

# create a label with a custom background color
label = tk.Label(root, text="Hello, World!", bg="blue")

# pack the label to display it on the window
label.pack()

# start the main event loop
root.mainloop()
