#!/usr/bin/env python3
# 1. Create a tkinter program that displays a random quote when a button is clicked. You can store the quotes in a list or a file.

import tkinter as tk
import random

# list of quotes
quotes = [
    "Intelligence is the ability to avoid doing work, yet getting the work done.  - Linus Torvalds",
    "Theory and practice sometimes clash. And when that happens, theory loses. Every single time.  - Linus Torvalds",
    "Linus Torvalds, the creator of Linux, is an expert of understatement in his leadership of Linux development community. When eager programmers would ask him, 'What part of Linux should I work on?' his answer would usually be, 'Let me know when you find out'.  - Dan Woods",
    "Everyone who lives within their means suffers from a lack of imagination.  - Oscar Wilde",
    "You don't make a great museum by putting all the art in the world into a single room. That's a warehouse. What makes a great museum great is the stuffs that's not on the walls. Someone says no. A curator is involved, making conscious decisions about what should stay and what should go. The best is a sub-sub-subset of all the possibilities.  - Rework (Book)",
    "An expert is a person who has made all the mistakes that can be made in a very narrow field.  - Neils Bohr",
    "Everything popular is wrong.  - Oscar Wilde",
    "Many a false step is made by standing still.  - Fortune Cookie"
    "I can't give you surefire formula for success, but I can give you a formula for failure: try to please everybody all the time.  - Herbert Bayard Swope"
    "The only way to do great work is to love what you do. -Steve Jobs",
    "Your time is limited, don't waste it living someone else's life. -Steve Jobs",
]

# create main window
root = tk.Tk()

# function to display a random quote
def display_quote():
    quote = random.choice(quotes)
    quote_label.config(text=quote)

# create a button to display a random quote
quote_button = tk.Button(root, text="Display Quote", command=display_quote)
quote_button.pack()

# create a label to display the quote
quote_label = tk.Label(root, text="")
quote_label.pack()

# start the main event loop
root.mainloop()
