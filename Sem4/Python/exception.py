num = input("Enter any number: ")
print(f"Multiplication table of {num}:")

try:
    for i in range(1,11):
        n = int(num)
        print(f"{n} x {i} = {n*i}")
except Exception as e:
    print(e)

print("End of code")
