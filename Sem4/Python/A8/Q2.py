import re

# \n\nQuestion Number 1

print("\nQuestion 1\n")
string = "The quick brown fox jumps over the lazy dog."
pattern = "brown"

match = re.search(pattern, string)

if match:
    print("Found match at index", match.start())
else:
    print("No match found.")

#\n\nQuestion Number 2
print("\nQuestion 2\n")

matches = re.findall(pattern, string)

print(matches)

#\nQuestion number 3
print("\nQuestion 3\n")

pattern = "\w+"

matches = re.findall(pattern, string)

print(matches)


#\nQuestion number 4
print("\nQuestion 4\n")
pattern = "\s"
replacement = "-"

new_string = re.sub(pattern, replacement, string)

print(new_string)

#\nQuestion number 5
print("\nQuestion 5\n")
pattern = "\s"

parts = re.split(pattern, string)

print(parts)

#\nQuestion number 6
print("\nQuestion 6\n")
pattern = "\w+"

match = re.match(pattern, string)

if match:
    print("Match found:", match.group())
else:
    print("No match found.")

#\nQuestion number 7
print("\nQuestion 7\n")
pattern = "\d"  # matches any digit

match = re.search(pattern, string)

if match:
    print("Match found:", match.group())
else:
    print("No match found.")

#\nQuestion number 8
print("\nQuestion 8\n")

string = "123"
pattern = r"^\d{3}$"
match = re.match(pattern, string)

if match:
    print("Match found!")
else:
    print("No match found.")

#\nQuestion number 9
print("\nQuestion 9\n")

string = "Hello, world!"
pattern = r"^Hello"
match = re.match(pattern, string)

if match:
    print("Match found!")
else:
    print("No match found.")


#\nQuestion number 10
print("\nQuestion 10\n")

import re

string = "foo\nfoo bar\nfoo baz"
pattern = r"^foo"
match = re.findall(pattern, string, flags=re.MULTILINE)

if match:
    print("Matches found!")
    print(match)
else:
    print("No matches found.")
