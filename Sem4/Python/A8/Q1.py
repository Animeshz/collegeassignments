import re

filename = input("Enter CSV filename: ")

name_regex = r'^[\sA-Za-z]+$'
phone_regex = r'^[6-9]\d{9}$'
email_regex = r'^[\w._-]+@[\w._-]+\.[\w]+$'
city_regex = r'^[\sA-Za-z]+$'

correct_names = []
wrong_names = []
correct_phones = []
wrong_phones = []
correct_emails = []
wrong_emails = []
correct_cities = []
wrong_cities = []

with open(filename, 'r') as file:

    for line in file:
        fields = line.strip().split(',')

        if re.match(name_regex, fields[0]):
            correct_names.append(fields[0])
        else:
            wrong_names.append(fields[0])

        if re.match(phone_regex, fields[1]):
            correct_phones.append(fields[1])
        else:
            wrong_phones.append(fields[1])

        if re.match(email_regex, fields[2]):
            correct_emails.append(fields[2])
        else:
            wrong_emails.append(fields[2])

        if re.match(city_regex, fields[3]):
            correct_cities.append(fields[3])
        else:
            wrong_cities.append(fields[3])

print("Correct names:", correct_names, len(correct_names))
print("Wrong names:", wrong_names, len(wrong_names))
print()
print("Correct phones:", correct_phones, len(correct_phones))
print("Wrong phones:", wrong_phones, len(wrong_phones))
print()
print("Correct emails:", correct_emails, len(correct_emails))
print("Wrong emails:", wrong_emails, len(wrong_emails))
print()
print("Correct cities:", correct_cities, len(correct_cities))
print("Wrong cities:", wrong_cities, len(wrong_cities))
