import os
import re

image_regex = re.compile(r'^(.+)\.(jpe?g|png|gif|tiff)$')

i = 1

for filename in os.listdir('.'):
    if len(image_regex.findall(filename)) != 0:

        current_filename = filename
        new_filename = re.sub(image_regex, rf'prefix-\1-{i:03}.\2', filename)

        os.rename(current_filename, new_filename)
        i += 1
