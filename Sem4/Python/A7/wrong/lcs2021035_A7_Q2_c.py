class Engine:
    def start(self):
        print("Engine started!")

class Transmission:
    def shift_gear(self):
        print("Gears shifted!")

class Car:
    def __init__(self):
        self._engine = Engine()
        self._transmission = Transmission()

    @property
    def engine(self):
        return self._engine

    @property
    def transmission(self):
        return self._transmission

    def start(self):
        self.engine.start()

    def shift_gear(self):
        self.transmission.shift_gear()

class SportsCar(Car):
    def __init__(self):
        super().__init__()
        self._spoiler = True

    @property
    def spoiler(self):
        return self._spoiler

    def activate_spoiler(self):
        print("Activating spoiler!")

car1 = Car()
car2 = SportsCar()

car1.start()
car2.start()

car1.shift_gear()
car2.shift_gear()

car2.activate_spoiler()
