class ValueTooSmallError(Exception):
    pass

def new_square(num):
    if num < 0:
        raise ValueTooSmallError("Number must be non-negative")
    return num ** 2

try:
    result = new_square(-2)
except ValueTooSmallError as e:
    print("Error:", e)
else:
    print("Square of the number is:", result)
