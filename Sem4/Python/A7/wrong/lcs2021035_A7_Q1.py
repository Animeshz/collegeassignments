class Student:
    def __init__(self, name, roll_no, address):
        self.name = name
        self.roll_no = roll_no
        self.address = address

class Classroom:
    def __init__(self):
        self.students = []

    def add_student(self, name, roll_no, address):
        student = Student(name, roll_no, address)
        self.students.append(student)

    def update_student(self, name, roll_no=None, address=None):
        for student in self.students:
            if student.name == name:
                if roll_no is not None:
                    student.roll_no = roll_no
                if address is not None:
                    student.address = address
                break
        else:
            print(f"{name} is not in the list of students")

    def remove_student(self, name):
        for student in self.students:
            if student.name == name:
                self.students.remove(student)
                break
        else:
            print(f"{name} is not in the list of students")

classroom = Classroom()
classroom.add_student("John", 1, "123 Main St.")
classroom.add_student("Jane", 2, "456 Maple Ave.")
for student in classroom.students:
    print(student.name)

classroom.update_student("John", address="789 Elm Rd.")
for student in classroom.students:
    print(student.address)

classroom.remove_student("Jane")
for student in classroom.students:
    print(student.name)
