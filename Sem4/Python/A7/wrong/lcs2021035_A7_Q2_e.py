class Shape:
    def draw(self):
        pass

class Circle(Shape):
    def draw(self):
        print("Circle drawn!")

class Triangle(Shape):
    def draw(self):
        print("Triangle drawn!")

class ShapeDrawer:
    def __init__(self, shapes):
        self.shapes = shapes

    def draw_all_shapes(self):
        for shape in self.shapes:
            shape.draw()

shapes = [Circle(), Triangle()]
drawer = ShapeDrawer(shapes)
drawer.draw_all_shapes()
