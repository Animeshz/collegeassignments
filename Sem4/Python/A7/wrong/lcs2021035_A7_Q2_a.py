class File:
    def __init__(self, file_name, file_type, file_size):
        self.file_name = file_name
        self.file_type = file_type
        self.file_size = file_size

    def __str__(self):
        return f"{self.file_name}.{self.file_type} - {self.file_size} bytes"

file_list = [
    File("lavesh", "pdf", 1024),
    File("resume", "png", 2048),
    File("love", "mp3", 4096),
]

for file in file_list:
    print(file)
