from abc import ABC, abstractmethod

class Shape(ABC):
    @abstractmethod
    def draw(self):
        pass

class Circle(Shape):
    def draw(self):
        print("Circle drawn!")

class Triangle(Shape):
    def draw(self):
        print("Triangle drawn!")

shape1 = Circle()
shape2 = Triangle()

shape1.draw()
shape2.draw()
