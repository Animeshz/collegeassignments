class Engine:
    def start(self):
        print("Starting engine")

class Transmission:
    def shift_gear(self):
        print("Shifting gears")

class Car:
    def __init__(self):
        self.engine = Engine()
        self.transmission = Transmission()

    def start(self):
        self.engine.start()

    def shift_gear(self):
        self.transmission.shift_gear()

class SportsCar(Car):
    def __init__(self):
        super().__init__()
        self.spoiler = True

    def activate_spoiler(self):
        print("Activating spoiler")

car1 = Car()
car2 = SportsCar()

car1.start()
car2.start()

car1.shift_gear()
car2.shift_gear()

car2.activate_spoiler()
