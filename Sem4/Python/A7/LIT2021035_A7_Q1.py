class Parent:
    students = []
    roll_nos = {}
    addresses = {}

    def new_data(self, name, roll_no, address):
        self.students.append(name)
        self.roll_nos[name] = roll_no
        self.addresses[name] = address

    @classmethod
    def update_data(cls, name, roll_no=None, address=None):
        if name in cls.students:
            if roll_no:
                cls.roll_nos[name] = roll_no
            if address:
                cls.addresses[name] = address
        else:
            print(f"{name} is not name of valid student")

    @staticmethod
    def delete_data(name):
        if name in Parent.students:
            Parent.students.remove(name)
            del Parent.roll_nos[name]
            del Parent.addresses[name]
        else:
            print(f"{name} is not name of valid student")

# Inherits all methods from Parent class
class Child(Parent):
    pass

# example usage
c = Child()
c.new_data("Animesh", 1, "Chattisgarh")
c.new_data("Meet", 2, "Maharashtra")
print(Child.students)  # ['Animesh', 'Meet']

Child.update_data("Animesh", address="Raipur, Chattisgarh")
print(Child.addresses)  # {'Animesh': 'Raipur, Chattisgarh', 'Meet': 'Maharashtra'}

Child.delete_data("Meet")
print(Child.students)  # ['Animesh']
