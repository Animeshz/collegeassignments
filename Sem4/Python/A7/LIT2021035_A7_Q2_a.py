class File:
    def __init__(self, name, type, size):
        self.name = name
        self.type = type
        self.size = size

    def get_file_info(self):
        return f"{self.name}.{self.type} - {self.size} bytes"

file1 = File("document", "pdf", 1024)
file2 = File("image", "png", 2048)
file3 = File("music", "mp3", 4096)

print(file1.get_file_info())
print(file2.get_file_info())
print(file3.get_file_info())
