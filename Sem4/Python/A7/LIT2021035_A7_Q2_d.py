class ValueTooSmallError(Exception):
    pass

def square_root(num):
    if num < 0:
        raise ValueTooSmallError(f"Number must be positive (given {num})")

    x = num/2
    for i in range(100):
        x = 0.5 * (x + num/x)

    return x

try:
    print("square_root(2): ", end='')
    print(square_root(2))
    print("square_root(-2): ", end='')
    print(square_root(-2))
except ValueTooSmallError as e:
    print(e)
else:
    print(result)
