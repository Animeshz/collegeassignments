is_prime = [True] * 101

for i in range(2, 101):
    if is_prime[i]:
        print(i)
        for j in range(2*i, 101, i):
            is_prime[j] = False
