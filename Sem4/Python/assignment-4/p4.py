import math

def is_prime(n):
    for i in range(2, int(math.sqrt(n)+1)):
        if n % i == 0:
            return False
    return True

n = int(input("Input a number: "))

if is_prime(n):
    print("prime")
else:
    print("NOT prime")
