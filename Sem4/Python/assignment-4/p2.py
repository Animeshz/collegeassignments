n = input("Input a number: ")
n_len = len(n)

arm_store = 0
for digit in n:
    digit_int = ord(digit)-ord('0')
    arm_store += digit_int**n_len

if arm_store == int(n):
    print(f"{n} is armstrong number")
else:
    print(f"{n} is NOT armstrong number")
