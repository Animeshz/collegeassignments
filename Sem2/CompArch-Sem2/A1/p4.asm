.data
   my_string:
      .asciiz "How many Fibonacci numbers you would like to calculate: "
   space:
      .asciiz  " "
   head:
      .asciiz  "The Fibonacci numbers are:\n"

   #Array of fibonacci numbers
   fibs: .word   3 : 30

   #Size
   size: .word  30

.text
.globl main

main:

   #Print string
   li $v0, 4
   la $a0, my_string
   syscall

   #Read integer into v0
   li $v0, 5
   syscall

   #Store v0 into size
   la $t5, size
   sw $v0, 0($t5)

   #Load address of fibonacci array into t0
   la   $t0, fibs        # load array

   #t0 = Current address into Fibonacci array

   #Load size into t5 (MIPS is RISC, no load-from-immediate-address instruction ava)
   la   $t5, size        #Load address of size into t5
   lw   $t5, 0($t5)      #Load word from t5

   #t5 = Number of numbers to compute

   #Load first (and second) Fibonacci number into t1 & t2
   li   $t1, 0
   li   $t2, 1

   add.d $f0, $f2, $f4

   #Store the first and second Fibonacci number into...
   sw   $t1, 0($t0)    #... the first
   sw   $t2, 4($t0)    #... and second array position (each position is 4 byte)

   #Set t1 = t5 - 2 = size = 2
   #t1 is the number of numbers left to compute, we skip the first two we already computed (thus the -2)
   addi $t1, $t5, -2

   #t1 = Number of numbers left to do

   loop:
      #t0 points to the second from last number in the array

      #Get the second from last number from the array
      lw   $t3, 0($t0)       # get values from array
      #Get the last number from the array
      lw   $t4, 4($t0)

      #Compute the next Fib number into t2
      add  $t2, $t3, $t4
      #Store it into the array (at fibs[t0+2])
      sw   $t2, 8($t0)

      #Advance t0 to point to the next number
      addi $t0, $t0, 4

      #Decrement the counter (number left to compute)
      addi $t1, $t1, -1

   #Jump if t1>0 back to the start of the loop
   bgtz $t1, loop

   #Call print with the array address and number of items
   la   $a0, fibs
   add  $a1, $zero, $t5
   jal  print

   #Exit
   li   $v0, 10
   syscall


print:
   #Mov arguments to t0 and t1
   add  $t0, $zero, $a0
   add  $t1, $zero, $a1

   #Print string
   la   $a0, head
   li   $v0, 4
   syscall

   out:
      #Load number from array and print it
      lw   $a0, 0($t0)
      li   $v0, 1
      syscall

      #Print space
      la   $a0, space
      li   $v0, 4
      syscall

      #Move t0 to the next number (each number occupies 4 bytes)
      addi $t0, $t0, 4
      #Decrement the number left to do
      addi $t1, $t1, -1

    #If t1 > 0 jump back to the start of the loop (out)
    bgtz $t1, out

    #Return
    jr $31
