.data
    Enter: .asciiz "Enter value of n: \n"

    .text
    .globl main

main:
    lui $s0, 0x1001

    li $v0, 4                   # print message for i/p
    la $a0, Enter
    syscall

    li $v0, 5                   # get n
    syscall

    add $s1, $v0, $zero         # init n = 5
    li $s2, 1                   # s2 = 1 for base case

    add $a0, $s1, $zero         # pass fib(a0)
    jal factorial

    li $v0, 1                   # print result
    add $a0, $v1, $zero
    syscall

    li $v0, 10                  # exit
    syscall

factorial:
    addi $sp, $sp, -8           # grow sp below
    sw $a0, 0($sp)              # store arguments and return addr
    sw $ra, 4($sp)

    bne $a0, $s2, recursion     # n != 1
    add $v1, $s2, $zero
    addi $sp, $sp, 8            # restore $sp and remove stack frame
    jr $ra

recursion:
    addi $a0, $a0, -1       # fib(a0 - 1)
    jal factorial

    lw $a0, 0($sp)          # load back a0 to use as a0 * fib(a0-1)
    lw $ra, 4($sp)
    addi $sp, $sp, 8
    mul $v1, $a0, $v1

    jr $ra
