.data
  prompt1: .asciiz "Enter string: "
  prompt2: .asciiz "Enter index to print: "
  buffer: .space 100

.text
  main:
    # Take string -> t0
    li $v0, 4
    la $a0, prompt1
    syscall

    li $v0, 8
    la $a0, buffer
    li $a1, 100
    syscall
    move $t0, $a0

    # Take int -> t1
    li $v0, 4
    la $a0, prompt2
    syscall

    li $v0, 5
    syscall
    move $t1, $v0

    # Read t1'th character from string (t0) -> t2
    add $t0, $t0, $t1
    lbu $t2, ($t0)

    # Print on console (char) <- t2
    li $v0, 11
    move $a0, $t2
    syscall

