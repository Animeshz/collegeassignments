.data
  prompt1: .asciiz "First integer: "
  prompt2: .asciiz "Second integer: "

.text
  main:
    # Take int 1 -> t0
    li $v0, 4
    la $a0, prompt1
    syscall

    li $v0, 5
    syscall
    move $t0, $v0

    # Take int 2 -> t1
    li $v0, 4
    la $a0, prompt2
    syscall

    li $v0, 5
    syscall
    move $t1, $v0

    # Perform division -> t2
    div $t0, $t1
    mflo $t2

    # Print on console <- t2
    li $v0, 1
    move $a0, $t2
    syscall
