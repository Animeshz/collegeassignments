import java.util.Scanner;

public class LCS2021035_LA2_Q3 {
    public static int fac(int i) {
        if (i == 1) return 1;
        return i * fac(i-1);
    }

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Input a number: ");
        while (!sc.hasNextInt()) {
            System.out.print("Please input an integer: ");
        }

        int i = sc.nextInt();

        System.out.println("Factorial of " + i + " is: " + fac(i));
    }
}
