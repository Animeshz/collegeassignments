import java.util.Scanner;

public class LCS2021035_LA2_Q1 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Input your name: ");
        String username = sc.nextLine();

        System.out.println("Dear " + username);
        System.out.println();
        System.out.println("  Welcome to the Java world.");
    }
}
