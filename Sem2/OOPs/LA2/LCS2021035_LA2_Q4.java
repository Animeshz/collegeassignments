import java.util.Scanner;

public class LCS2021035_LA2_Q4 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Input a number: ");
        while (!sc.hasNextInt()) {
            sc.next();
            System.out.print("Please input an integer: ");
        }

        int k = sc.nextInt();

        System.out.println("Fibonacci series upto n numbers are: ");
        int a = 0, b = 1;
        for (int i = 0; i < k; i++) {
            System.out.println(a);
            b += a;
            a = b - a;
        }
    }
}
