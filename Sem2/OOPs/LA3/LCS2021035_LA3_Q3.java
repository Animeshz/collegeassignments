import java.util.Scanner;

public class LCS2021035_LA3_Q3 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Which number we want table of? ");
        while (!sc.hasNextInt()) {
            System.out.print("Please input an integer value: ");
        }
        int n = sc.nextInt();

        int i = 0;
        while (++i <= 10) {
            System.out.println(n + " * " + i + " = " + n * i);
        }
    }
}
