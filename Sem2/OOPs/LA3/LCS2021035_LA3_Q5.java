import java.util.Scanner;

public class LCS2021035_LA3_Q5 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Input a number for which factorial is to be found: ");
        while (!sc.hasNextInt()) {
            System.out.print("Please input an integer: ");
        }

        int n = sc.nextInt();

        int answer = 1;
        for (int i = 1; i <= n; i++) {
            answer *= i;
        }

        System.out.println("Factorial of " + n + " is: " + answer);
    }
}
