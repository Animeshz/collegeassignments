import java.util.Scanner;

public class LCS2021035_LA3_Q2 {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);

        System.out.print("How many even numbers do we need the sum of? ");
        while (!sc.hasNextInt()) {
            System.out.print("Please input an integer value: ");
        }
        int n = sc.nextInt();

        int i = 0, answer = 0;
        while (++i <= n) {
            answer += i*2;
        }
        System.out.println("Sum of first " + n + " even numbers: " + answer);
    }
}
