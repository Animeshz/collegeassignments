import java.util.Scanner;

public class LCS2021035_LA3_Q6 {
    public static void main(String args[]) {
        int n = 8, answer = 0;
        for (int i = 1; i <= 10; i++) {
            answer += n * i;
        }
        System.out.println("Sum of numbers occurring in the multiplication table of 8 is " + answer);
    }
}
