import java.util.Scanner;

public class LCS2021035_LA3_Q4 {
    public static void main(String args[]) {
        int n = 10, i = 10;
        while (i > 0) {
            System.out.println(n + " * " + i + " = " + n * i);
            i--;
        }
    }
}
