import java.util.*;
import java.util.concurrent.*;

class SharedBuffer<T> {
    private BlockingQueue<T> q = new LinkedBlockingQueue<T>();

    void push(T v) throws InterruptedException {
        q.put(v);
    }

    T poll() throws InterruptedException {
        return q.take();
    }
}

class Producer extends Thread {
    private SharedBuffer<String> sb;
    Producer(SharedBuffer<String> sb) {
        this.sb = sb;
    }

    @Override
    public void run() {
        try {
            while (true) {  // Pushes a string to the sb every second
                sb.push("Hi");
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {}
    }
}

class Consumer extends Thread {
    private SharedBuffer<String> sb;
    Consumer(SharedBuffer<String> sb) {
        this.sb = sb;
    }

    @Override
    public void run() {
        try {
            while (true) {  // Prints the string to the stdout as received
                String value = sb.poll();
                System.out.println(value);
            }
        } catch (InterruptedException e) {}
    }
}

public class P1 {
    public static void main(String args[]) {
        SharedBuffer<String> sb = new SharedBuffer<>();
        Thread t1 = new Producer(sb);
        Thread t2 = new Consumer(sb);

        t1.start();
        t2.start();
    }
}
