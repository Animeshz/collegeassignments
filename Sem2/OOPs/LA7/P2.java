class Counter {
    private int v;
    synchronized void synchronizedIncrement() {
        v++;
    }

    void unsynchronizedIncrement() {
        v++;
    }

    int getValue() {
        return v;
    }

    void reset() {
        v = 0;
    }
}

public class P2 {
    public static void main(String args[]) throws InterruptedException {
        Counter c = new Counter();

        Thread t1 = new Thread(() -> {
            for(int i = 0; i < 500; i++) c.unsynchronizedIncrement();
        });

        Thread t2 = new Thread(() -> {
            for(int i = 0; i < 500; i++) c.unsynchronizedIncrement();
        });

        Thread t3 = new Thread(() -> {
            for(int i = 0; i < 500; i++) c.unsynchronizedIncrement();
        });

        t1.start();
        t2.start();
        t3.start();
        t1.join();
        t2.join();
        t3.join();
        System.out.println("Unsynchronized increment: " + c.getValue());

        c.reset();
        t1 = new Thread(() -> {
            for(int i = 0; i < 500; i++) c.synchronizedIncrement();
        });

        t2 = new Thread(() -> {
            for(int i = 0; i < 500; i++) c.synchronizedIncrement();
        });

        t3 = new Thread(() -> {
            for(int i = 0; i < 500; i++) c.synchronizedIncrement();
        });

        t1.start();
        t2.start();
        t3.start();
        t1.join();
        t2.join();
        t3.join();
        System.out.println("Synchronized increment: " + c.getValue());
    }
}
