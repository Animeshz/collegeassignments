import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

class Receiver {
    List<BlockingQueue<Integer>> qs;

    Receiver() {
        qs = new ArrayList();
    }

    BlockingQueue<Integer> consume() {
        BlockingQueue<Integer> q = new ArrayBlockingQueue(5);
        qs.add(q);
        return q;
    }

    void push(int value) throws InterruptedException {
        for (BlockingQueue<Integer> q : qs) {
            q.put(value);
        }
    }
}

public class LCS2021035_Q2 {
    public static void main(String args[]) throws InterruptedException {
        Receiver rcv = new Receiver();

        Thread t1 = new Thread(() -> {
            Random ran = new Random();
            while (true) {
                try {
                    Thread.sleep(1000);
                    rcv.push(ran.nextInt());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();

        Thread t2 = new Thread(() -> {
            BlockingQueue<Integer> q = rcv.consume();

            while (true) {
                try {
                    Integer val = q.take();

                    if (val % 2 == 0) {
                        System.out.println("Square of " + val + ": " + val * val);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t2.start();

        Thread t3 = new Thread(() -> {
            BlockingQueue<Integer> q = rcv.consume();

            while (true) {
                try {
                    Integer val = q.take();

                    if (val % 2 != 0) {
                        System.out.println("Cube of " + val + ": " + val * val * val);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t3.start();

        t1.join();
    }
}
