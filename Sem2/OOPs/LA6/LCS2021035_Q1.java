import java.util.InputMismatchException;
import java.util.Scanner;

class NumberNotIntegerException extends Exception {
    public NumberNotIntegerException(String error) {
        super(error);
    }
}

public class LCS2021035_Q1 {
    public static void main(String[] args) throws NumberNotIntegerException, ArithmeticException {
        Scanner sc = new Scanner(System.in);
        int num1, num2;

        try {
            System.out.print("Enter first number: ");
            num1 = sc.nextInt();
            System.out.print("Enter second number: ");
            num2 = sc.nextInt();
        } catch (InputMismatchException error) {
            throw new NumberNotIntegerException("Input is not a number");
        }

        if (num2 == 0) {
            throw new ArithmeticException("num2 should not be zero");
        }
        System.out.println("num1 / num2 = " + num1 / (double) num2);
        sc.close();
    }
}
