class UnitConvertor {
    public double kmToMiles(int km) {
        return 0.62137119 * km;
    }
}

public class LCS2021035_A1_P4 {
    public static void main(String args[]) {
        UnitConvertor uc = new UnitConvertor();

        System.out.println("1km in miles: " + uc.kmToMiles(1));
        System.out.println("52km in miles: " + uc.kmToMiles(52));
    }
}
