class Percentage {
    double mm1;
    double mm2;
    double mm3;
    double mm4;

    Percentage(double mm1, double mm2, double mm3, double mm4) {
        this.mm1 = mm1;
        this.mm2 = mm2;
        this.mm3 = mm3;
        this.mm4 = mm4;
    }

    double calculateFor(double m1, double m2, double m3, double m4) {
        return (m1 + m2 + m3 + m4) * 100 / (this.mm1 + this.mm2 + this.mm3 + this.mm4);
    }
}

public class LCS2021035_A1_P3 {
    public static void main(String args[]) {
        Percentage p = new Percentage(100, 100, 100, 100);
        double percent = p.calculateFor(89, 92, 88, 99);

        System.out.println(percent);
    }
}
