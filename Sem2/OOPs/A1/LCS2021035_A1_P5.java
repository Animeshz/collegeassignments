// Function Overloading (Same Name Multiple Definitions)
class Fish {
    int length = 50;
    String color = "Blue";

    double timeToPassLength(double speed) {
        return length / speed;
    }

    double timeToPassLength() {
        return timeToPassLength(10);  // default speed
    }
}

public class LCS2021035_A1_P5 {
    public static void main(String args[]) {
        Fish f = new Fish();
        System.out.println("Fish needs " + f.timeToPassLength(30) + "s to move past its length with speed of 30m/s");
        System.out.println("Fish needs " + f.timeToPassLength() + "s to move past its length with its default speed");
        System.out.println();

        // Operator Overloading
        System.out.println("Operator Overloading");
        System.out.println(1 + 2);                // + on int
        System.out.println("Hello " + "world!");  // + on String
        System.out.println("Hello " + 55);        // + on String & int
    }
}
