class Employee {
    int emp_id;
    private int salary;
    private String name;

    Employee(int emp_id, int salary, String name) {
        this.emp_id = emp_id;
        this.salary = salary;
        this.name = name;
    }

    public void printDetails() {
        System.out.println("Employee " + this.name + " with id " + this.emp_id + " has salary " + this.salary + ".");
    }
    public int getSalary() {
        return this.salary;
    }
}

public class LCS2021035_A1_P1 {
    public static void main(String args[]) {
        Employee emp = new Employee(554, 70000, "Rohan Kumar");

        emp.printDetails();
        System.out.println("Employee id " + emp.emp_id + " has " + emp.getSalary() + " monthly salary.");
    }
}
