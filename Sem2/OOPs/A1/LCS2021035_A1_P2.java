class Sum {
    int num1;
    int num2;

    public int sum() {
        return this.num1 + this.num2;
    }
}

public class LCS2021035_A1_P2 {
    public static void main(String args[]) {
        Sum s = new Sum();

        s.num1 = 5;
        s.num2 = 7;
        System.out.println("Sum of " + s.num1 + " & " + s.num2 + " is " + s.sum() + ".");

        s.num1 = 8;
        System.out.println("Sum of " + s.num1 + " & " + s.num2 + " is " + s.sum() + ".");
    }
}
