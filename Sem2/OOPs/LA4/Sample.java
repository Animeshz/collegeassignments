class Car {
    String brand;

    Car(String brand) {
        this.brand = brand;
    }
    Car() { this("Ford"); }

    String getBrand() {
        return this.brand;
    }
}

public class Sample {
    public static void main(String args[]) {
        Car car1 = new Car();
        String car1Brand = car1.getBrand();

        System.out.println(car1Brand);
    }
}
