class ThisKeywordUsages {
    public int n;
    public AnotherClass acInstance;

    ThisKeywordUsages(int n) {
        System.out.println(this);  // Refer to current object instance
        this.n = n;     // Refer to variable of current object (1)

        // Pass itself as an argument (function/constructor call)
        function(this);
        this.acInstance = new AnotherClass(this);

        this.chainable();  // Call function on this object
        this.chainable().chainable().chainable();
    }

    ThisKeywordUsages() {
        this(12);       // Refer to another constructor of current class
    }

    void function(ThisKeywordUsages passedObject) {
        System.out.println("passedObject: " + passedObject.n);
        System.out.println("this: " + this.n);  // Refer to variable of current object (2)
    }

    ThisKeywordUsages chainable() {
        System.out.println("chainable");
        return this;    // Return instance of itself
    }

    void printP() {
        System.out.println("this.acInstance.p: " + this.acInstance.p);
    }
}

class AnotherClass {
    public int p;
    AnotherClass(ThisKeywordUsages obj) {
        this.p = obj.n + 13;
    }
}

public class P3 {
    public static void main(String args[]) {
        ThisKeywordUsages tku = new ThisKeywordUsages();
        tku.printP();
    }
}
