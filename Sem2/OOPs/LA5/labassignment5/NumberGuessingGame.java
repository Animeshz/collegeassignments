package labassignment5;

import java.util.Random;
import java.util.Scanner;

public class NumberGuessingGame {
    public static void main(String args[]) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        int v = random.nextInt(100);
        int userInput;

        System.out.println("Guess the number: ");
        while (true) {
            while (!scanner.hasNextInt()) {
                System.out.print("Please input an integer: ");
                scanner.next();
            }

            userInput = scanner.nextInt();

            if (userInput < v) {
                System.out.println("Value is smaller, guess another value: ");
            } else if (userInput > v) {
                System.out.println("Value is larger, guess another value: ");
            } else {
                System.out.println("Yay! You guessed it right!");
            }
        }
    }
}
