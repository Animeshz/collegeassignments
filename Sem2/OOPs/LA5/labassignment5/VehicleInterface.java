package labassignment5;

interface Vehicle {
    double durationOfTravel(long distance);
    String owner();
    int hornDecibals();
    int tyres();
}

class Car implements Vehicle {
    public int speed;
    public String owner;

    Car(int speed, String owner) {
        this.speed = speed;
        this.owner = owner;
    }

    public double durationOfTravel(long distance) {
        return distance / (double)speed;
    }

    public String owner() {
        return this.owner;
    }

    public int hornDecibals() {
        return 30;
    }

    public int tyres() {
        return 2;
    }
}

class Bike implements Vehicle {
    public int speed;
    public String owner;

    Bike(int speed, String owner) {
        this.speed = speed;
        this.owner = owner;
    }

    public double durationOfTravel(long distance) {
        return distance / (double)speed;
    }

    public String owner() {
        return this.owner;
    }

    public int hornDecibals() {
        return 20;
    }

    public int tyres() {
        return 2;
    }
}

class Bicycle implements Vehicle {
    public String owner;

    Bicycle(String owner) {
        this.owner = owner;
    }

    public double durationOfTravel(long distance) {
        return distance / 10.0;
    }

    public String owner() {
        return this.owner;
    }

    public int hornDecibals() {
        return 8;
    }

    public int tyres() {
        return 2;
    }
}

public class VehicleInterface {
    public static void main(String args[]) {
        Vehicle v1 = new Car(80, "Mohan");
        Vehicle v2 = new Bike(40, "Rohan");
        Vehicle v3 = new Bicycle("Ritik");

        System.out.println("Car will take " + v1.durationOfTravel(80) + "hrs to travel 80km");
        System.out.println("Bike will take " + v2.durationOfTravel(80) + "hrs to travel 80km");
        System.out.println("Bicycle will take " + v3.durationOfTravel(80) + "hrs to travel 80km");
    }
}
