#include <iostream>
using namespace std;

struct BstNode {
    int data;
    struct BstNode *left;
    struct BstNode *right;

    BstNode(int data) : data(data) {
        left = NULL;
        right = NULL;
    }
};

void insert_bst(struct BstNode *root, int data) {
    struct BstNode *new_node = new BstNode(data);
    struct BstNode *current_node = root;

    while (true) {
        if (new_node->data <= current_node->data) {
            if (current_node->left != NULL) current_node = current_node->left;
            else { current_node->left = new_node; break; }
        } else {
            if (current_node->right != NULL) current_node = current_node->right;
            else { current_node->right = new_node; break; }
        }
    }
}

struct BstNode *create_bst(int n, int *inputs) {
    if (n == 0) return NULL;

    struct BstNode *root_node = new BstNode(inputs[0]);

    for (int i = 1; i < n; i++) {
        insert_bst(root_node, inputs[i]);
    }

    return root_node;
}

void print_bst(struct BstNode *root) {
    if (root == NULL) return;

    print_bst(root->left);
    cout << root->data << " ";
    print_bst(root->right);
}

int main() {
    int n;
    cout << "No of elements: ";
    cin >> n;

    int inputs[n];
    cout << "Elements (space seperated): ";
    for (int i = 0; i < n; i++) cin >> inputs[i];

    struct BstNode *bst = create_bst(n, inputs);
    print_bst(bst);
    cout << endl;

    int new_data;
    cout << "Element to be inserted now: ";
    cin >> new_data;
    insert_bst(bst, new_data);
    print_bst(bst);
    cout << endl;

    return 0;
}
