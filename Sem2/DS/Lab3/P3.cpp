#include <iostream>
using namespace std;

struct node {
    int data;
    struct node *next;
};

struct node *solution(struct node *head) {
    struct node *middle_ptr = head, *nv_ptr = head;

    int even_iter = 0;
    while (nv_ptr != NULL) {
        if (even_iter) middle_ptr = middle_ptr->next;

        nv_ptr = nv_ptr->next;
        even_iter = !even_iter;
    }

    return middle_ptr;
}

int main() {
    int n;
    cin >> n;

    // Inputs
    struct node *head = NULL, *local_node = NULL;
    for (int i = 0; i < n; i++) {
        struct node *new_node = (struct node*) malloc(sizeof(struct node));
        cin >> new_node->data;
        new_node->next = NULL;

        if (head == NULL) head = new_node, local_node = new_node;
        else local_node->next = new_node, local_node = new_node;
    }

    struct node *middle_node = solution(head);
    cout << middle_node->data << endl;

    // Cleanup
    while (head != NULL) {
        struct node *next = head->next;
        free(head);
        head = next;
    }

    return 0;
}
