#include <iostream>
using namespace std;

struct node {
    int data;
    struct node *next;
};

struct node *solution(struct node *head, int data) {
    struct node *nv_node = head;
    while (nv_node->next != NULL) nv_node = nv_node->next;

    struct node *new_node = (struct node*) malloc(sizeof(struct node));
    new_node->data = data;
    new_node->next = NULL;

    nv_node->next = new_node;
    return head;
}

int main() {
    int n;
    cin >> n;

    // Inputs
    struct node *head = NULL, *local_node = NULL;
    for (int i = 0; i < n; i++) {
        struct node *new_node = (struct node*) malloc(sizeof(struct node));
        cin >> new_node->data;
        new_node->next = NULL;

        if (head == NULL) head = new_node, local_node = new_node;
        else local_node->next = new_node, local_node = new_node;
    }

    int data;
    cin >> data;
    solution(head, data);

    // Output & Cleanup
    while (head != NULL) {
        cout << head->data << ' ';
        struct node *next = head->next;
        free(head);
        head = next;
    }
    cout << endl;

    return 0;
}
