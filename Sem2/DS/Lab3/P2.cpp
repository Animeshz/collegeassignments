#include <iostream>
using namespace std;

struct node {
    int data;
    struct node *next;
};

int solution(struct node *head, int E) {
    int index = 0;
    while (head != NULL) {
        if (head->data == E) return index;

        head = head->next, index++;
    }

    return -1;
}

int main() {
    int n;
    cin >> n;

    // Inputs
    struct node *head = NULL, *local_node = NULL;
    for (int i = 0; i < n; i++) {
        struct node *new_node = (struct node*) malloc(sizeof(struct node));
        cin >> new_node->data;
        new_node->next = NULL;

        if (head == NULL) head = new_node, local_node = new_node;
        else local_node->next = new_node, local_node = new_node;
    }

    int data;
    cin >> data;
    int index = solution(head, data);
    cout << index << endl;

    // Cleanup
    while (head != NULL) {
        struct node *next = head->next;
        free(head);
        head = next;
    }

    return 0;
}
