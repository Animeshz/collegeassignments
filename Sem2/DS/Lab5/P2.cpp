#include <bits/stdc++.h>
using namespace std;

struct qNode {
    int data;
};

struct Queue {
    int size;
    struct qNode *elements;
    int front;
    int rear;
};

struct Queue *new_queue(int n) {
    struct qNode *nq = (struct qNode *) malloc(sizeof(struct qNode));
    struct Queue *q = (struct Queue *) malloc(sizeof(struct Queue));
    q->size = n;
    q->elements = nq;
    q->front = -1;
    q->rear = -1;

    return q;
}

void enqueue(struct Queue *q, int data) {
    // never reached: Assumption section did not mentioned size, so size is taken upto no of operations
    if (q->front == 0 && q->rear == q->size-1) {
        cout << "queue is full" << endl;
    }

    if (q->front == -1 && q->rear == -1) q->front++;
    q->elements[++q->rear].data = data;
}

void dequeue(struct Queue *q) {
    if (q->front == -1 && q->rear == -1) {
        cout << "queue is empty" << endl;
    }

    q->front++;
    if (q->front > q->rear) {
        q->front = -1;
        q->rear = -1;
    }
}

int main() {
    int n;
    cin >> n;

    int data[n], ops[n];
    for (int i = 0; i < n; i++) cin >> data[i];
    for (int i = 0; i < n; i++) cin >> ops[i];

    struct Queue *q = new_queue(n);
    for (int i = 0; i < n; i++) {
        if (ops[i]) {
            enqueue(q, data[i]);
        } else {
            dequeue(q);
        }
    }

    if (q->front != -1) {
        cout << endl;
        for (int i = q->front; i <= q->rear; i++) {
            cout << q->elements[i].data << ' ';
        }
        cout << endl;
    }

    free(q->elements);
    free(q);

    return 0;
}
