#include <bits/stdc++.h>
using namespace std;

struct Queue {
    int elements[10];
    int front;
    int rear;
};

struct Queue *new_queue() {
    struct Queue *q = (struct Queue *) malloc(sizeof(struct Queue));
    q->front = -1;
    q->rear = -1;

    return q;
}

struct Queue *solution(int A[], int N) {
    struct Queue *q = new_queue();

    for (int i = 0; i < N; i++) {
        int o = A[i];

        if (o == 3) return q;

        if (o == 1) {
            int k = A[++i];
            if (q->front == 0 && q->rear == 9) {
                cout << "queue is full" << endl;
                continue;
            }

            if (q->front == -1 && q->rear == -1) q->front++;
            q->elements[++q->rear] = k;
        } else {
            if (q->front == -1 && q->rear == -1) {
                cout << "queue is empty" << endl;
                continue;
            }

            q->front++;
            if (q->front > q->rear) {
                q->front = -1;
                q->rear = -1;
            }
        }
    }

    return q;
}


int main() {
    int n;
    cin >> n;
    int a[n];
    for (int i = 0; i < n; i++) {
        cin >> a[i];
        if (a[i] == 3) break;  // point of return
    }

    struct Queue *q = solution(a, n);

    if (q->front != -1) {
        cout << endl;
        for (int i = q->front; i <= q->rear; i++) {
            cout << q->elements[i] << ' ';
        }
        cout << endl;
    }

    free(q);

    return 0;
}
