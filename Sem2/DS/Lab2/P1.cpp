#include <iostream>
#include <climits>
using namespace std;

// Q1
void Display(int n, int *arr) {
    cout << "[ ";
    for (int i = 0; i < n; i++) {
        cout << arr[i] << ' ';
    }
    cout << ']' << endl;
}

// Q2
void Append(int &n, int *&arr, int val) {
    int *new_arr = (int*) malloc((n+1) * sizeof(int));
    for (int i = 0; i < n; i++)
        new_arr[i] = arr[i];
    new_arr[n++] = val;

    free(arr);
    arr = new_arr;
}

// Q3
void Insert(int &n, int *&arr, int idx, int val) {
    if (idx > n) return;  // Index >= Size

    int i, *new_arr = (int*) malloc((n+1) * sizeof(int));
    for (i = 0; i < idx; i++) new_arr[i] = arr[i];
    new_arr[i++] = val;
    for (; i <= n; i++) new_arr[i] = arr[i-1];

    n = n+1;
    free(arr);
    arr = new_arr;
}

// Q4 (removes all the occurrence of val, if val exists, overwrites n for new size)
void Delete(int &n, int *&arr, int val) {
    int ctr = 0;
    for (int i = 0; i < n; i++) if (arr[i] == val) ctr++;
    if (ctr == 0) return;  // No occurrence found

    int *new_arr = (int*) malloc((n-ctr) * sizeof(int));
    for (int i = 0, v = 0; i < n; i++) if (arr[i] != val) new_arr[v++] = arr[i];

    n = n-ctr;
    free(arr);
    arr = new_arr;
}

// Q5 (returns index)
int LinearSearch(int n, int *arr, int val) {
    for (int i = 0; i < n; i++)
        if (arr[i] == val) return i;
    return -1;
}

// Q6
int Get(int n, int *arr, int idx) {
    if (idx >= n) return -1;  // Index >= Size
    return arr[idx];
}

// Q7
void Set(int n, int *arr, int idx, int val) {
    if (idx >= n) return;  // Index >= Size
    arr[idx] = val;
}

// Q8
int Max(int n, int *arr) {
    int max = INT_MIN;
    for (int i = 0; i < n; i++)
        if (arr[i] > max) max = arr[i];
    return max;
}

// Q9
int Min(int n, int *arr) {
    int min = INT_MAX;
    for (int i = 0; i < n; i++)
        if (arr[i] < min) min = arr[i];
    return min;
}

// Q10
void Reverse(int n, int *arr) {
    for (int i = 0; i < n/2; i++) {
        int tmp = arr[i];
        arr[i] = arr[n-1-i];
        arr[n-1-i] = tmp;
    }
}

// Q11
void Shift(int n, int *arr) {
    int tmp = arr[n-1];
    for (int i = n-1; i > 0; i--) arr[i] = arr[i-1];
    arr[0] = tmp;
}

// Q12
void Rotate(int n, int *arr, int amount) {
    amount %= n;
    amount += (amount < 0) * n;

    // for (int i = 0; i < amount; i++) Shift(n, arr);  // Ez, though inefficient

    int *tmps = (int*) malloc(amount * sizeof(int));
    for (int i = 0; i < amount; i++) tmps[i] = arr[n-amount+i];

    for (int i = n-1; i >= amount; i--) arr[i] = arr[i-amount];
    for (int i = 0; i < amount; i++) arr[i] = tmps[i];
    free(tmps);
}

int main() {
    int size = 4;
    int *arr = (int*) malloc(size*sizeof(int));
    arr[0] = 1;
    arr[1] = 3;
    arr[2] = 5;
    arr[3] = 6;

    cout << "Display: ";
    Display(size, arr);

    cout << "Append(2): ";
    Append(size, arr, 2);
    Display(size, arr);

    cout << "Insert(2,9): ";
    Insert(size, arr, 2, 9);
    Display(size, arr);

    cout << "Delete(3): ";
    Delete(size, arr, 3);
    Display(size, arr);

    cout << "LinearSearch(5): index " << LinearSearch(size, arr, 5) << endl;
    cout << "Get(4): " << Get(size, arr, 4) << endl;

    cout << "Set(4, 7): ";
    Set(size, arr, 4, 7);
    Display(size, arr);

    cout << "Min: " << Min(size, arr) << endl;
    cout << "Max: " << Max(size, arr) << endl;

    cout << "Reverse: ";
    Reverse(size, arr);
    Display(size, arr);

    cout << "Shift: ";
    Shift(size, arr);
    Display(size, arr);

    cout << "Rotate(2): ";
    Rotate(size, arr, 2);
    Display(size, arr);

    cout << "Rotate(-1): ";
    Rotate(size, arr, -1);
    Display(size, arr);

    free(arr);
    return 0;
}
