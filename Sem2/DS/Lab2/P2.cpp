#include <iostream>
#include <climits>
using namespace std;

void Display(int n, int *arr) {
    cout << "[ ";
    for (int i = 0; i < n; i++) {
        cout << arr[i] << ' ';
    }
    cout << ']' << endl;
}

// Q1
bool IsSorted(int n, int *arr) {
    for (int i = 0; i < n-1; i++) {
        if (arr[i] > arr[i+1]) return false;
    }
    return true;
}

// Q2
int FirstOccurrence(int n, int *arr, int val) {
    for (int i = 0; i < n; i++) {
        if (arr[i] == val) return i;
    }
    return -1;
}

// Q3
int *AllOccurrences(int n, int *arr, int val, int &occurences) {
    occurences = 0;
    for (int i = 0; i < n; i++) {
        if (arr[i] == val) occurences++;
    }

    int *vrr = (int*) malloc(occurences * sizeof(int));
    for (int i = 0, c = 0; i < n; i++) {
        if (arr[i] == val) vrr[c++] = i;
    }
    return vrr;
}

// Q4
void FirstPairOfRequiredSum(int n, int *arr, int k, int &a, int &b) {
    a = INT_MIN, b = INT_MIN;
    for (int i = 0; i < n; i++) {
        for (int j = i+1; j < n; j++) {
            if (arr[i] + arr[j] == k) {
                a = arr[i], b = arr[j];
                return;
            }
        }
    }
}

// Q5
void MinMax(int n, int *arr, int &min, int &max) {
    min = INT_MAX, max = INT_MIN;
    for (int i = 0; i < n; i++) {
        if (arr[i] < min) min = arr[i];
        if (arr[i] > max) max = arr[i];
    }
}


int main() {
    int size = 4;
    int *arr1 = (int*) malloc(size*sizeof(int));
    int *arr2 = (int*) malloc(size*sizeof(int));

    arr1[0] = 1;
    arr1[1] = 3;
    arr1[2] = 3;
    arr1[3] = 6;

    arr2[0] = 1;
    arr2[1] = 3;
    arr2[2] = 6;
    arr2[3] = 3;

    cout << "Display(arr1): "; Display(size, arr1);
    cout << "Display(arr2): "; Display(size, arr2);
    cout << endl;

    cout << "IsSorted(arr1): " << IsSorted(size, arr1) << endl;
    cout << "IsSorted(arr2): " << IsSorted(size, arr2) << endl;

    cout << "FirstOccurrence(arr1, 3): index " << FirstOccurrence(size, arr1, 3) << endl;

    int total_occurrences, *occ;

    cout << "MultipleOccurrence(arr1, 3): indices ";
    occ = AllOccurrences(size, arr1, 3, total_occurrences);
    Display(total_occurrences, occ);
    free(occ);

    cout << "MultipleOccurrence(arr2, 3): indices ";
    occ = AllOccurrences(size, arr2, 3, total_occurrences);
    Display(total_occurrences, occ);
    free(occ);

    int min, max;
    cout << "MinMax(arr1): ";
    MinMax(size, arr1, min, max);
    cout << min << ' ' << max << endl;

    free(arr1);
    free(arr2);

    return 0;
}
