#include <iostream>
using namespace std;

unsigned int max_size = 1e5;
string opening = "({[<";
string closing = ")}]>";

bool match(string p) {
    int stack[max_size], i = -1;

    for (auto c : p) {
        int op_idx = opening.find(c);
        int cl_idx = closing.find(c);

        if (op_idx != string::npos) {
            stack[++i] = c;
        } else if (cl_idx != string::npos) {
            if (i < 0 || stack[i--] != opening[cl_idx]) return false;  // stack empty or unmatched
        }
    }

    if (i != -1) return false;  // stack not empty by end of traversal
    return true;
}

int main() {
    string parens;
    getline(cin, parens);

    if (match(parens)) cout << "Parenthesis matches" << endl;
    else cout << "Parenthesis doesn't match" << endl;

    return 0;
}
