#include <iostream>
using namespace std;

bool qis_full(int *q, int n, int f, int r) {
    return r == n-1 && f == 0 || r == f-1;
}

bool qis_empty(int *q, int n, int f, int r) {
    return f == -1;
}

void insert(int *q, int n, int &f, int &r, int x) {
    if (qis_full(q, n, f, r)) return;

    if (qis_empty(q, n, f, r)) {
        f++, r++;
        q[0] = x;
    } else {
        r = (r+1) % n;
        q[r] = x;
    }
}

int remove(int *q, int n, int &f, int &r) {
    if (qis_empty(q, n, f, r)) return -1;  // no element exists, returning -1

    int ret = q[f];
    if (f == r) { f = -1; r = -1; }
    else f = (f+1) % n;
    return ret;
}

void print(int *q, int n, int f, int r) {
    cout << "circular_queue: ";
    for (int i = 0; i < n; i++) {
        if ((f <= r && i >= f && i <= r) || (f > r && (i >= f || i <= r)))  // f & r ke beech mei h, even circular
            cout << q[i] << ' ';
        else
            cout << "_ ";
    }
    cout << endl;
}

int main() {
    int n, f = -1, r = -1;

    cout << "Input the size of the circular queue: ";
    cin >> n;
    int *circular_queue = new int[n];

    int select = 0;

    while (true) {
        cout << "What do you want to do?" << endl;
        cout << "[0] Exit" << endl;
        cout << "[1] Print" << endl;
        cout << "[2] IsEmpty" << endl;
        cout << "[3] IsFull" << endl;
        cout << "[4] Insert" << endl;
        cout << "[5] Remove" << endl;
        cout << ": ";

        cin >> select;

        switch (select) {
            case 0:
                return 0;
            case 1:
                print(circular_queue, n, f, r);
                break;
            case 2:
                cout << "is_empty: " << qis_empty(circular_queue, n, f, r) << endl;
                break;
            case 3:
                cout << "is_full: " << qis_full(circular_queue, n, f, r) << endl;
                break;
            case 4:
                if (qis_full(circular_queue, n, f, r)) { cout << "Queue is full" << endl; break; }

                int x;
                cout << "Enter number you want to insert: ";
                cin >> x;
                insert(circular_queue, n, f, r, x);
                break;
            case 5:
                if (qis_empty(circular_queue, n, f, r)) { cout << "Queue is empty" << endl; break; }

                cout << "removed element: " << remove(circular_queue, n, f, r) << endl;
                break;
        }
        cout << endl;
    }

    delete [] circular_queue;

    return 0;
}
