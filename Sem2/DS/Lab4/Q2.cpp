#include <iostream>
using namespace std;

unsigned int max_size = 1e5;
string digits = "0123456789";
string ops = "+-/*^";

long pow(long base, long exp) {
    long ans = 1;
    while (exp--) ans *= base;
    return ans;
}

long evaluate(string p) {
    long stack[max_size], i = -1, num = 0, num_flag = 0;

    for (auto c : p) {
        int op_idx = ops.find(c);
        int dg_idx = digits.find(c);

        // space or operator came while parsing number character by character
        if (num_flag && (c == ' ' || op_idx != string::npos)) {
            stack[++i] = num;
            num = 0, num_flag = false;
        }

        if (op_idx != string::npos) {  // operator came
            long operand2 = stack[i--];
            long operand1 = stack[i--];

            if (c == '^') stack[++i] = pow(operand1, operand2);
            else if (c == '/') stack[++i] = operand1 / operand2;
            else if (c == '*') stack[++i] = operand1 * operand2;
            else if (c == '-') stack[++i] = operand1 - operand2;
            else stack[++i] = operand1 + operand2;

        } else if (dg_idx != string::npos) {  // number digit (character by character)
            if (!num_flag) num_flag = true;
            num *= 10;
            num += c - '0';
        }
    }

    return stack[0];
}

int main() {
    string parens;
    getline(cin, parens);

    cout << evaluate(parens) << endl;

    return 0;
}
