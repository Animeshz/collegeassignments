#include <stdio.h>

int main() {
    int num;
    printf("Input a two-digit number: ");
    scanf("%d", &num);

    if (num < 10 || num > 99) {
        printf("Please give two-digit number\n");
        return 1;
    }

    int reverse = (num % 10) * 10 + (num / 10);
    printf("Reverse of the given number is: %d\n", reverse);
    return 0;
}
