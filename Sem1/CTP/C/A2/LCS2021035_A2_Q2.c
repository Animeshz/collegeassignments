#include <stdio.h>

int main() {
    int num;
    printf("Input a two-digit number: ");
    scanf("%d", &num);

    if (num < 10 || num > 99) {
        printf("Please give two-digit number\n");
        return 1;
    }

    int sum = (num % 10) + (num / 10);
    printf("Sum of its digits: %d\n", sum);
    return 0;
}
