#include <stdio.h>

void swap_third_var(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

void swap_arithmatic(int *a, int *b) {
    *a = *a + *b;
    *b = *a - *b;
    *a = *a - *b;
}

void swap_bitwise(int *a, int *b) {
    *a = *a ^ *b;
    *b = *a ^ *b;
    *a = *a ^ *b;
}

int main() {
    int a, b;
    printf("Please provide 2 numbers in given format\n");
    printf("a b: ");  // Example: 2 4
    scanf("%d %d", &a, &b);

    int a_copy1 = a, a_copy2 = a, a_copy3 = a;
    int b_copy1 = b, b_copy2 = b, b_copy3 = b;

    swap_third_var(&a_copy1, &b_copy1);
    swap_arithmatic(&a_copy2, &b_copy2);
    swap_bitwise(&a_copy3, &b_copy3);

    printf("Value of a and b respectively after swap (using third variable) is: %d and %d\n", a_copy1, b_copy1);
    printf("Value of a and b respectively after next swap (using arithmatic operators) is: %d and %d\n", a_copy2, b_copy2);
    printf("Value of a and b respectively after next swap (using bitwise operators) is: %d and %d\n", a_copy3, b_copy3);

    return 0;
}
