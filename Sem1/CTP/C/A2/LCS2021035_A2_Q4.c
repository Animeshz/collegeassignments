#include <stdio.h>

int main() {
    int num;
    printf("Input a number: ");
    scanf("%d", &num);

    if (num < 0) num *= -1;
    printf("The absolute value of the given number is %d\n", num);

    return 0;
}
