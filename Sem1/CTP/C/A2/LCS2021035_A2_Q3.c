#include <stdio.h>

int middle_if_else(int a, int b, int c) {
    int middle;
    if ((a > b && b > c) || (a < b && b < c)) middle = b;
    else if ((b > a && a > c) || (b < a && a < c)) middle = a;
    else middle = c;

    return middle;
}

int middle_ternary(int a, int b, int c) {
    int max_ab = a > b ? a : b;
    int max_bc = b > c ? b : c;
    int max_ac = a > c ? a : c;

    int middle = max_ab ^ max_bc ^ max_ac;
    return middle;
}

int main() {
    int a, b, c;
    printf("Please provide 3 numbers in given format\n");
    printf("a b c: ");  // Example: 2 3 4
    scanf("%d %d %d", &a, &b, &c);

    printf("The second highest number of the given set (using if-else) is %d\n", middle_if_else(a, b, c));
    printf("The second highest number of the given set (using ternary-operator) is %d\n", middle_ternary(a, b, c));

    return 0;
}
