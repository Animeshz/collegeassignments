#include <stdio.h>

int main() {
    int num;
    printf("Input a number: ");
    scanf("%d", &num);

    if (num < 0) {
        printf("Given number is a negative number\n");
    } else if (num % 2 == 0) {
        printf("Given number is an even number\n");
    } else {
        printf("Given number is an odd number\n");
    }
    return 0;
}
