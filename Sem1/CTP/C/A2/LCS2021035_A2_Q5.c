#include <stdio.h>

int main() {
    int num;
    printf("Input a year in YYYY format: ");
    scanf("%d", &num);

    if (num % 400 == 0 || (num % 4 == 0 && num % 100 != 0))
        printf("The year is a leap year\n");
    else
        printf("The year is not a leap year\n");

    return 0;
}
