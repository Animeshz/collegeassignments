#include <stdio.h>

int main() {
    int input;
    printf("Input a binary number: ");
    scanf("%d", &input);

    int temp = input;
    while (temp) {
        int digit = temp % 10;
        if (digit != 0 && digit != 1) {
            printf("%d is not a valid binary number. A binary number must consist of 0s and 1s.\n", input);
            return 0;
        }

        temp /= 10;
    }
    printf("%d is a valid binary number, ", input);

    if ((input & 1) == 0) {
        printf("and is an even number in base 10.\n");
    } else {
        printf("and is an odd number in base 10.\n");
    }

    return 0;
}
