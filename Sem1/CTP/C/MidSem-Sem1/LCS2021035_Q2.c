#include <stdio.h>

int main() {
    int n;
    printf("Please input the boundary number (1<=n<=50): ");
    scanf("%d", &n);

    if (n < 1 || n > 50) {
        printf("Please give the boundary number in between 1 and 50.");
        return 1;
    }

    for (int i = 1; i <= 2*n-1; i++) {
        int reflection_i = i;
        if (reflection_i > n) reflection_i = 2*n-i;

        int value = n;
        for (int j = 1; j <= 2*n-1; j++) {
            if (n >= 10 && value < 10) printf(" ");
            printf("%d ", value);

            if (j < reflection_i) value--;
            else if (j > 2*n-1-reflection_i) value++;
        }
        printf("\n");
    }

    return 0;
}
