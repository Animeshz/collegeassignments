#include <stdio.h>

int first_duplicate(int *arr, int n, int *first_occ, int *second_occ) {
    *first_occ = *second_occ = -1;
    for (int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
        for (int j = 0; j < i; j++) {
            if (arr[i] == arr[j]) {
                *first_occ = j, *second_occ = i;
                return arr[i];
            }
        }
    }
    return 0;
}

int main() {
    int n;
    printf("Input size of the array: ");
    scanf("%d", &n);
    if (n > 1000) {
        printf("Number of elements in the array is limited to 1000 only.\n");
        return 1;
    }

    int arr[n];
    printf("Input the numbers, space separated: ");
   
    int first_occ, second_occ;
    int dup = first_duplicate(arr, n, &first_occ, &second_occ);
    if (first_occ == -1)
        printf("There's no duplicate number in the given array\n");
    else
        printf("The element %d is the first duplicate number, and its first & second occurrence are at %d & %d respectively\n", dup, first_occ, second_occ);
    return 0;
}
