#include <stdio.h>

void add(int *a, int *b, int *res) {
    *res = *a + *b;
}

int main() {
    int a, b;
    printf("Input two numbers, space separated: ");
    scanf("%d %d", &a, &b);

    int result;
    add(&a, &b, &result);

    printf("Result of add(): %d\n", result);

    return 0;
}
