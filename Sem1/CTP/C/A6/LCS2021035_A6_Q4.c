#include <stdio.h>

void max_occurrence(int *arr, int n, int *element, int *max_freq) {
    *max_freq = 0, *element = -1;
    for (int i = 0; i < n; i++) {
        int cnt = 0;
        for (int j = 0; j < n; j++) {
            if (arr[i] == arr[j]) cnt++;
        }

        if (cnt > *max_freq) {
            *element = arr[i];
            *max_freq = cnt;
        }
    }
}

int main() {
    int n;
    printf("Input size of the array: ");
    scanf("%d", &n);
    if (n > 1000) {
        printf("Number of elements in the array is limited to 1000 only.\n");
        return 1;
    } else if (n <= 0) {
        printf("Please give positive number of elements in array\n");
        return 1;
    }

    int arr[n];
    printf("Input the numbers, space separated: ");
    for (int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }

    int element, max_freq;
    max_occurrence(arr, n, &element, &max_freq);
    
    printf("Element %d is first element having max (%d) frequency in the array\n", element, max_freq);

    return 0;
}
