#include <stdio.h>

void unique_elements(int *arr, int n, int *dest, int *dest_cnt) {
    *dest_cnt = 0;
    for (int i = 0; i < n; i++) {
        int flag = 1;
        for (int j = 0; j < i; j++) {
            if (arr[i] == arr[j]) {
                flag = 0;
                break;
            }
        } 
        if (flag) dest[(*dest_cnt)++] = arr[i];
    }
}

int main() {
    int n;
    printf("Input size of the array: ");
    scanf("%d", &n);
    if (n > 1000) {
        printf("Number of elements in the array is limited to 1000 only.\n");
        return 1;
    }

    int arr[n];
    printf("Input the numbers, space separated: ");
    for (int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }

    int cnt, dest[n];
    unique_elements(arr, n, dest, &cnt);

    for (int i = 0; i < cnt; i++) printf("%d ", dest[i]);
    printf("\n");

    return 0;
}
