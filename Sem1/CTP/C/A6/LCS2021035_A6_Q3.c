#include <stdio.h>

void union_(int n, int *set1, int m, int *set2, int *result_size, int *result) {
    *result_size = 0;
    int i, j;
    for (i = 0, j = 0; i < n && j < m;) {
        int a = set1[i], b = set2[j];
        if (a == b) { result[(*result_size)++] = a; i++; j++; }
        else if (a < b) { result[(*result_size)++] = a; i++; }
        else { result[(*result_size)++] = b; j++; }
    }

    if (i != n) {
        for (; i < n; i++) result[(*result_size)++] = set1[i];
    } else if (j != m) {
        for (; j < m; j++) result[(*result_size)++] = set2[j];
    }
}

void intersection(int n, int *set1, int m, int *set2, int *result_size, int *result) {
    *result_size = 0;
    for (int i = 0, j = 0; i < n && j < m;) {
        int a = set1[i], b = set2[j];
        if (a == b) { result[(*result_size)++] = a; i++; j++; }
        else if (a < b) i++;
        else j++;
    }
}

void difference(int n, int *set1, int m, int *set2, int *result_size, int *result) {
    *result_size = 0;
    int i, j;
    for (i = 0, j = 0; i < n && j < m;) {
        int a = set1[i], b = set2[j];
        if (a == b) { i++; j++; }
        else if (a < b) { result[(*result_size)++] = a; i++; }
        else j++;
    }
    if (i != n) {
        for (; i < n; i++) result[(*result_size)++] = set1[i];
    }
}

void symmetric_difference(int n, int *set1, int m, int *set2, int *result_size, int *result) {
    *result_size = 0;
    int i, j;
    for (i = 0, j = 0; i < n && j < m;) {
        int a = set1[i], b = set2[j];
        if (a == b) { i++; j++; }
        else if (a < b) { result[(*result_size)++] = a; i++; }
        else { result[(*result_size)++] = b; j++; }
    }

    if (i != n) {
        for (; i < n; i++) result[(*result_size)++] = set1[i];
    } else if (j != m) {
        for (; j < m; j++) result[(*result_size)++] = set2[j];
    }
}

void sort(int *arr, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = i+1; j < n; j++) {
            if (arr[i] > arr[j]) {
                int tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
        }
    }
}

void print_elements(int size, int *arr) {
    for (int i = 0; i < size; i++) printf("%d ", arr[i]);
    printf("\n");
}

int main() {
    int n, m;

    printf("Input size of first set: ");
    scanf("%d", &n);
    int arr_1[n];
    printf("Input the numbers, space separated: ");
    for (int i = 0; i < n; i++) {
        scanf("%d", &arr_1[i]);
    }
    sort(arr_1, n);

    printf("Input size of second set: ");
    scanf("%d", &m);
    int arr_2[m];
    printf("Input the numbers, space separated: ");
    for (int i = 0; i < m; i++) {
        scanf("%d", &arr_2[i]);
    }
    sort(arr_2, m);

    int fin_size, final[n+m];

    union_(n, arr_1, m, arr_2, &fin_size, final);
    printf("Union: ");
    print_elements(fin_size, final);

    intersection(n, arr_1, m, arr_2, &fin_size, final);
    printf("Intersection: ");
    print_elements(fin_size, final);

    difference(n, arr_1, m, arr_2, &fin_size, final);
    printf("Difference: ");
    print_elements(fin_size, final);

    symmetric_difference(n, arr_1, m, arr_2, &fin_size, final);
    printf("Symmetric Difference: ");
    print_elements(fin_size, final);

    return 0;
}
