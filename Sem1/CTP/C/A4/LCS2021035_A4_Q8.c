#include <stdio.h>

int main() {
    int binary;
    printf("Input 5 digit binary number: ");
    scanf("%d", &binary);

    int answer = 0;
    for (int i = 0; binary; binary /= 10, i++) {
        int digit = binary % 10;
        if (digit != 0 && digit != 1) {
            printf("Please give number in binary only, using 0 or 1\n");
            return 1;
        }

        answer |= digit << i;
    }

    printf("The corresponding integer of the binary equivalent is %d\n", answer);

    return 0;
}
