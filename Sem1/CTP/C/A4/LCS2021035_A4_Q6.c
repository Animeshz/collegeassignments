#include <stdio.h>

int main() {
    double n;
    printf("Input a number whose sqrt is to be found: ");
    scanf("%lf", &n);

    /*
     * Used technique is also known for Newton's mathod
     * Basically we define an error function f(x) = x^2 - n
     * whose root is sqrt(n).
     *
     * We start with a random number, I chose (ans=) n/2. The close
     * it is to sqrt(n), better the precision of the final value.
     * Then we draw a tangent at the f(x) with chosen number
     * and find the point where it meets y=0 (x-axis) and update
     * the answer. It leads us to a point where funciton is more
     * close to the 0 than current ans.
     *
     * It formulates as [slope at xold of f(x) = slope of tangent]
     * f'(xold) = (0 - f(xold)) / (xnew - xold)
     * xnew - xold = -(xold^2 - n) / 2xold
     * xnew = (xold + n/xold) / 2
     * we repeat this over and over again to find more precise answer.
     */
    double ans = n/2;
    for (int i = 0; i < 100; i++) {
        ans = (ans + n/ans) / 2;
    }

    printf("The sqrt of the given number (%lf) is %.15lf\n", n, ans);

    return 0;
}
