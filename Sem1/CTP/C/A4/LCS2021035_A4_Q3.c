#include <stdio.h>

int main() {
    int is_not_prime[1001] = {0};

    is_not_prime[0] = 1;
    is_not_prime[1] = 1;
    for (int i = 2; i <= 1000; i++) {
        if (!is_not_prime[i]) {
            for (int j = 2; i*j <= 1000; j++) {
                is_not_prime[i*j]++;
            }
        }
    }

    printf("Primes from 1 to 1000 are:\n");
    for (int i = 0; i <= 1000; i++) {
        if (!is_not_prime[i]) {
            printf("%d ", i);
        }
    }
    printf("\n");

    return 0;
}
