#include <stdio.h>

/*
 * Recursively walk into bits of number by shifting it to right
 * Start printing digit from the inner-most recursive call then
 * outer in succession. Recursive call breaks when number becomes
 * 0 after getting right shifted multiple times.
 */
void print_binary(int input) {
    if (input == 0) return;
    print_binary(input >> 1);
    printf("%d", input & 1);
}

int main() {
    int n;
    printf("Input a 2-digit number: ");
    scanf("%d", &n);

    if (n < 10 || n > 99) {
        printf("Please input a 2 digit number only\n");
        return 1;
    }

    printf("The binary representation is: ");
    print_binary(n);
    printf("\n");

    return 0;
}
