#include<stdio.h>

int main() {
    int n;
    printf("How many terms of the series you want to print? ");
    scanf("%d", &n);
    if (n <= 0) {
        printf("Please give a number greater than 0");
        return 1;
    }

    printf("f(x) = ");

    if (n > 0) printf("1 ");
    if (n > 1) printf("- (x / 2!) ");

    for (int i = 2; i < n; i++) {
        if (i % 2 == 0) printf("+ "); else printf("- ");
        printf("(%dx^%d / %d!) ", i, i, i+1);
    }
    printf("\n");

    return 0;
}
