#include <stdio.h>
#include <math.h>

#define PI 3.14159265358979

int main() {
    float m1, c1, m2, c2;
    printf("Input the coefficients of y=mx+c of two lines in given format: \n(m1 c1) (m2 c2): ");
    scanf("(%f %f) (%f %f)", &m1, &c1, &m2, &c2);

    if (m1*m2 == -1) {
        printf("Lines are perpendicular\n");
    } else if (m1 == m2) {
        if (c1 == c2) {
            printf("Lines are overlapping\n");
        } else {
            printf("Lines are parallel\n");
        }
    } else {
        float degrees = (180/PI) * atan((m1-m2)/(1+m1*m2));
        printf("Lines are intersecting with %.2f degrees\n", fabs(degrees));
    }

    return 0;
}
