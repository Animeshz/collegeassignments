#include <stdio.h>
#include <math.h>

int main() {
    int n;
    float x;

    printf("For how many terms of you want to calculate the sum? ");
    scanf("%d", &n);
    if (n <= 100) {
        printf("Please give more than 100 terms\n");
        return 1;
    }

    printf("For what input you want to calculate the sum? ");
    scanf("%f", &x);
    if (!(-1 < x && x <= 1)) {
        printf("Please give value between -1 (exclusive) and 1 (inclusive)\n");
        return 1;
    }

    double sum = 0;

    // sum = x - (x^2 / 2) + (x^3 / 3) - ...
    for (int i = 1; i <= n; i++) {
        sum += (i % 2 == 0 ? -1 : 1) * pow(x, i) / i;
    }

    printf("The sum of the series at given %f is %lf\n", x, sum);

    return 0;
}
