#include <stdio.h>

int main() {
    char input;
    printf("Input a character: ");
    scanf("%c", &input);

    switch (input) {
        case 'a': case 'A':
        case 'e': case 'E':
        case 'i': case 'I':
        case 'o': case 'O':
        case 'u': case 'U':
            printf("It's a Vowel\n");
            break;
        default:
            printf("It's a Consonant\n");
            break;
    }

    return 0;
}
