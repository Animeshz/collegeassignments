#include <stdio.h>

int main() {
    int n, base;
    printf("Input the number (in base 10) to be checked if its Harshad/Niven: ");
    scanf("%d", &n);
    printf("Which base to check with: ");
    scanf("%d", &base);

    int digit_sum = 0;
    for (int n_cp = n; n_cp != 0; n_cp /= base)
        digit_sum += n_cp % base;

    if (n % digit_sum == 0) {
        printf("Harshad number.\n");
    } else {
        printf("Not a Harshad number.\n");
    }

    return 0;
}
