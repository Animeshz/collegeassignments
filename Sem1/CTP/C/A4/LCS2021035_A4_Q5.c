#include <stdio.h>
#include <math.h>

int main() {
    double x1, x2, x3;
    double y1, y2, y3;

    printf("Input the three coordinate in given format\n(x1 y1) (x2 y2) (x3 y3): ");
    scanf("(%lf %lf) (%lf %lf) (%lf %lf)", &x1, &y1, &x2, &y2, &x3, &y3);

    if ((x1==x2 && y1==y2) && (x2==x3 && y2==y3) && (x3==x1 && y3==y1)) {
        printf("All the three points are same");
    }

    // Edge case: when any two points are same, it won't form triangle
    // and NaN == NaN is false
    double slope1 = (y1-y2)/(x1-x2);
    double slope2 = (y2-y3)/(x2-x3);
    if (slope1 == slope2 || (x1==x2 && y1==y2) || (x2==x3 && y2==y3) || (x3==x1 && y3==y1)) {
        printf("The points are collinear and donot form any triangle\n");
        return 0;
    }

    double dist1 = sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
    double dist2 = sqrt((x2-x3)*(x2-x3) + (y2-y3)*(y2-y3));
    double dist3 = sqrt((x3-x1)*(x3-x1) + (y3-y1)*(y3-y1));

    if (dist1 == dist2 || dist2 == dist3 || dist3 == dist1) {
        if (dist1 == dist2 && dist2 == dist3) {
            printf("Equilateral Triangle\n");
        } else {
            printf("Isosceles Triangle\n");
        }
    } else {
        printf("Scalene Triangle\n");
    }

    return 0;
}
