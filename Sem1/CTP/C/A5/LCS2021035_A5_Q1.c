#include <stdio.h>
#include <math.h>

double factorial(int n) {
    double out = 1;
    while (n) out *= n--;
    return out;
}

int main() {
    int n; double x;

    printf("How many terms do you want to calculate the sum of the series (n>100): ");
    scanf("%d", &n);
    if (n <= 100) {
        printf("Please input n > 100\n");
        return 1;
    }

    printf("For what value do you want to calculate the sum (-1 < x <= 1): ");
    scanf("%lf", &x);
    printf("\n");
    if (x <= -1 || x > 1) {
        printf("Please input -1 < x <= 1\n");
        return 1;
    }

    double sum = 0;
    if (n > 0) sum = 1;

    for (int i = 1; i < n; i++) {
        sum += (i%2 ? -1 : 1) * i * pow(x, i) / factorial(i+1);
    }
    printf("The sum of series 1-(x/2!)+2(x^2/3!)-3(x^3/4!)+... upto %d terms for x=%lf is %.15lf\n", n, x, sum);

    return 0;
}
