#include <stdio.h>

void swap(float *a, float *b) {
    float tmp = *a;
    *a = *b;
    *b = tmp;
}

int main() {
    int n;
    printf("Please input the size of array: ");
    scanf("%d", &n);

    float arr[n];
    printf("Input the elements of array, space seperated: ");
    for (int i = 0; i < n; i++) scanf("%f", &arr[i]);

    for (int i = 0; i < n; i++) {
        for (int j = i+1; j < n; j++) {
            if (arr[i] > arr[j]) swap(&arr[i], &arr[j]);
        }
    }

    for (int i = 0; i < n; i++) printf("%f ", arr[i]);
    printf("\n");

    return 0;
}
