#include <stdio.h>

int main() {
    int n;
    printf("Number of elements: ");
    scanf("%d", &n);

    float arr[n];
    printf("Array elements (space seperated): ");
    for (int i = 0; i < n; i++) scanf("%f", &arr[i]);

    float find_element;
    printf("Enter the element to be found: ");
    scanf("%f", &find_element);

    int found = 0;
    for (int i = 0; i < n; i++) {
        if (find_element == arr[i]) {
            printf("key %f is found at index %d\n", find_element, i);
            found = 1;
            // not breaking so all index can be found
        }
    }

    if (!found) {
        printf("key %f is not found in the given array\n", find_element);
    }

    return 0;
}
