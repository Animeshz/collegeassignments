#include <stdio.h>

int main() {
    int n;
    printf("Input a number: ");
    scanf("%d", &n);

    if (n < 0) {
        printf("Given number is not a palindrome\n");
        return 0;
    }

    int digits[10] = {0};
    int i = 0;
    for (int t = n; t != 0 && i < 10; i++) {
        digits[i] = t % 10;
        t /= 10;
    }

    for (int j = 0; j < (i+1)/2; j++) {
        if (digits[j] != digits[i-j-1]) {
            printf("Given number is not a palindrome\n");
            return 0;
        }
    }
    printf("Given number is a palindrome\n");

    return 0;
}
