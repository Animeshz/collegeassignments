#include <stdio.h>

int main() {
    int n;
    printf("Input a number: ");
    scanf("%d", &n);

    printf("All the numbers divisible by 5 or 7 between 1 and n is: ");
    for (int i = 0; i < n; i++)
        if (i % 5 == 0 || i % 7 == 0)
            printf("%d ", i);
    printf("\n");

    return 0;
}
