#include <stdio.h>

void A(int n) {
    printf("A\n");
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= i; j++) {
            printf("%d ", j);
        }
        printf("\n");
    }
}

void B(int n) {
    printf("B\n");
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= i; j++) {
            printf("%d ", i);
        }
        printf("\n");
    }
}

int main() {
    int rows;
    printf("Enter number of rows to be printed: ");
    scanf("%d", &rows);

    A(rows);
    printf("\n");
    B(rows);

    return 0;
}
