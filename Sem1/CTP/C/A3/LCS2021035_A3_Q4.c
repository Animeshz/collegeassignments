#include <stdio.h>

int main() {
    int n;
    printf("Input a number: ");
    scanf("%d", &n);

    int sum = 0;
    if (n >= 0) {
        for (int i = 1; i < n; i++) if (n % i == 0) sum += i;
    }

    if (sum == n)
        printf("It is a special number\n");
    else
        printf("It is not a special number\n");

    return 0;
}
