#include <stdio.h>

int main() {
    int a, b;
    printf("Input two numbers in the given format:\na b: ");
    scanf("%d %d", &a, &b);

    int sumdiv1 = 0;
    int sumdiv2 = 0;
    for (int i = 1; i < a; i++) if (a % i == 0) sumdiv1 += i;
    for (int i = 1; i < b; i++) if (b % i == 0) sumdiv2 += i;

    if (sumdiv1 == b && sumdiv2 == a)
        printf("It is an amicable number");
    else
        printf("It is not an amicable number");

    return 0;
}
