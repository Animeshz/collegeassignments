#include <stdio.h>

int min(int a, int b) {
    return a < b ? a : b;
}

int main() {
    int a, b;
    printf("Input two integers in given format\na b: ");
    scanf("%d %d", &a, &b);

    a = a >= 0 ? a : -a;
    b = b >= 0 ? b : -b;

    int HCF = 1;
    for (int i = min(a, b); i > 1; i--) {
        if (a % i == 0 && b % i == 0) {
            HCF = i;
            break;
        }
    }

    unsigned long LCM = a * b / HCF;
    printf("The LCM of %d and %d is: %lu\n", a, b, LCM);

    return 0;
}
