#include <stdio.h>

long nPr(long n, long r) {
    int out = 1;
    while (r--) out *= n--;

    return out;
}

// in each iteration, product is calculated first to ensure no integer trucation happens
// e.g. 10C2 = (10/1 * 9)/2
long nCr(long n, long r) {
    int out = 1;
    for (int i = 1; i <= r;) {
        out = (out * n--) / i++;
    }

    return out;
}

int main() {
    long int n, r;
    printf("Input n and r respectively for nCr and nPr\nn r: ");
    scanf("%li %li", &n, &r);

    printf("nPr: %li\n", nPr(n, r));
    printf("nCr: %li\n", nCr(n, r));

    return 0;
}
