#include <stdio.h>
#include <stdbool.h>

int min(int a, int b) {
    return a < b ? a : b;
}

int main() {
    unsigned int a, b;
    printf("Input two positive integers in given format\na b: ");
    scanf("%u %u", &a, &b);

    bool coprime = true;
    for (int i = 2; i <= min(a, b); i++) {
        if (a % i == 0 && b % i == 0) {
            coprime = false;
            break;
        }
    }

    if (coprime)
        printf("Given numbers are co-prime\n");
    else
        printf("Given numbers are not co-prime\n");

    return 0;
}
