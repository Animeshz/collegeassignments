#include <stdio.h>

int main() {
    int a;
    printf("Input a number: ");
    scanf("%d", &a);

    int armsum = 0;
    if (a >= 0) {
        int dig, t = a;
        while (t != 0) {
            dig = t%10;
            armsum += dig*dig*dig;
            t /= 10;
        }
    }

    if (armsum == a)
        printf("%d is an armstrong number\n", a);
    else
        printf("%d is not an armstrong number\n", a);

    return 0;
}
