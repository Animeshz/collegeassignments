#include <stdio.h>

void add_matrix(int n, int m, int m1[][m], int m2[][m], int final[][m]) {
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
            final[i][j] = m1[i][j] + m2[i][j];
}

void multiply_matrix(int r1, int r2_c1, int c2, int m1[][r2_c1], int m2[][c2], int final[][c2]) {
   for (int i = 0; i < r1; i++)
        for (int j = 0; j < c2; j++) {
            final[i][j] = 0;
            for (int k = 0; k < r2_c1; k++)
                final[i][j] += m1[i][k] * m2[k][j];
        }
}

void print_matrix(int n, int m, int matrix[][m]) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++)
            printf("%d ", matrix[i][j]);
        printf("\n");
    }
}

int determinant(int n, int matrix[][n]) {
    if (n == 1) return matrix[0][0];
    if (n == 2) return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix [1][0];

    int det = 0;
    for (int i = 0; i < n; i++) {
        int matrix_view[n-1][n-1];
        for (int k = 0; k < n-1; k++) {
            for (int j = 0; j < n-1; j++) {
                matrix_view[k][j] = matrix[k+1][j+(j>=i?1:0)];
            }
        }
        det += (i % 2 == 0 ? 1 : -1) * determinant(n-1, matrix_view);
    }
    return det;
}

int is_symmetric(int n, int matrix[][n]) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j <= i; j++) {
            if (matrix[i][i-j] != matrix[i-j][i]) return 0;
        }
    }
    return 1;
}

int main() {
    int r1,c1,r2,c2;
    printf("Enter number of rows and columns for matrix 1 (NxM): ");
    scanf("%dx%d",&r1,&c1);
    int m1[r1][c1];
    printf("Enter elements of the matrix 1 (space separated): \n");
    for (int i = 0; i < r1; i++)
        for (int j = 0; j < c1; j++)
            scanf("%d", &m1[i][j]);
    printf("Enter number of rows and columns for matrix 2 (NxM): ");
    scanf("%dx%d",&r2,&c2);
    int m2[r2][c2];
    printf("Enter elements of the matrix 1 (space separated): \n");
    for (int i = 0; i < r2; i++)
        for (int j = 0; j < c2; j++)
            scanf("%d", &m2[i][j]);

    if (r1==r2 && c1==c2) {
        int added_matrix[r1][c1];
        add_matrix(r1, c1, m1, m2, added_matrix);
        printf("\nAddition of matrix:\n");
        print_matrix(r1, c1, added_matrix);
    }
    else printf("Addition not possible with different sizes\n");

    if (c1 == r2) {
        int multiplied_matrix[r1][c1];
        multiply_matrix(r1, r2, c2, m1, m2, multiplied_matrix);
        printf("\nMultiplication of matrix:\n");
        print_matrix(r1, c1, multiplied_matrix);
    }
    else printf("Multiplication not possible with different col and row sizes of matrix 1 and 2 respectively\n");

    if (r1==c1) {
        printf("\nDeterminant of matrix 1: %d\n", determinant(r1, m1));
        printf("Matrix 1 is %s\n", is_symmetric(r1, m1) ? "symmetric" : "not symmetric");
    }
    else printf("Determinant/Symmetricity of matrix 1 not possible, different row and col\n");

    if (r2==c2) {
        printf("\nDeterminant of matrix 2: %d\n", determinant(r2, m2));
        printf("Matrix 2 is %s\n", is_symmetric(r2, m2) ? "symmetric" : "not symmetric");
    }
    else printf("Determinant/Symmetricity of matrix 2 not possible, different row and col\n");

    return 0;
}
