#include <stdio.h>

void input_str(char *str) {
    if ((str[0] = getc(stdin)) != '\n') str++;
    scanf("%[^\n]s", str);
}

char to_lower(char c) {
    if ('A' <= c && c <= 'Z') return c - 'A' + 'a';
    return c;
}
int check_equal(char *str1, char *str2, int n) {
    for (int i = 0; i < n; i++) {
        if (to_lower(str1[i]) != to_lower(str2[i])) return 0;  // false
    }
    return 1;  // true
}

int main() {
    char s1[1000], s2[1000];
    printf("Input String 1: ");
    input_str(s1);
    printf("Input String 1: ");
    input_str(s2);

    int n1=0, n2=0;
    while (s1[n1] != '\0') n1++;
    while (s2[n2] != '\0') n2++;
    if (n1 != n2) {
        printf("Not same!\n");
        return 0;
    }

    if (check_equal(s1, s2, n1)) printf("Same!\n");
    else printf("Not same!\n");;

    return 0;
}
