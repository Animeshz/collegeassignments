#include <stdio.h>

int gcd(int a, int b) {
    if (a == 0) return b;
    return gcd(b%a, a);
}

int binary_search(int *arr, int min, int max, int key) {
    if (min <= max) {
        int mid = (min+max)/2;
        if (arr[mid] == key) return mid;
        if (arr[mid] > key) return binary_search(arr, min, mid-1, key);
        return binary_search(arr, mid+1, max, key);
    }
    return -1;
}

int fib_ith(int n) {
    if (n==0) return 0;
    if (n==1) return 1;
    return fib_ith(n-1) + fib_ith(n-2);
}
void print_fibonacci(int n) {
    for (int i = 0; i < n; i++) printf("%d ", fib_ith(i));
    printf("\n");
}

int is_palindrome(char *start, char *end) {
    if (*start != *end) return 0;
    if (end - start <= 0) return 1;

    return is_palindrome(start+1, end-1);
}

int main() {
    int a, b;
    printf("Input two numbers whose gcd has to be found (a b): ");
    scanf("%d %d", &a, &b);
    printf("The gcd of the given numbers is %d\n\n", gcd(a, b));

    int c, k;
    printf("Input number of elements in sorted array where binary search has to be performed: ");
    scanf("%d", &c);
    printf("Input elements (space separated): ");
    int arr[c];
    for (int i = 0; i < c; i++) scanf("%d", &arr[i]);
    printf("Input element to be found: ");
    scanf("%d", &k);
    printf("The index found by performing binary search on given sorted array is: %d\n\n", binary_search(arr, 0, c-1, k));

    int d;
    printf("Input number of terms of fibonacci sequence is to be printed: ");
    scanf("%d", &d);
    print_fibonacci(d);
    printf("\n");

    char string[10000];
    printf("Input the string: ");
    char *input_ptr = &string[0];
    if ((string[0] = getc(stdin)) != '\n') input_ptr++;
    scanf("%[^\n]s", input_ptr);
    int s=0;
    while (string[s] != '\0') s++;
    if (is_palindrome(string, string+s-1)) printf("Given string is palindrome\n");
    else printf("Given string is not palindrome\n");

    return 0;
}
