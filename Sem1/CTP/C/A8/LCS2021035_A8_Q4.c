#include <stdio.h>
#include <stdlib.h>

void sort_desc(int *arr, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = i+1; j < n; j++) {
            if (arr[i] < arr[j]) {
                int tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
        }
    }
}

int main() {
    int n;
    printf("Input number of integers: ");
    scanf("%d", &n);
    int *arr = malloc(n * sizeof(int));
    printf("Input the distinct integers, space separated: ");
    for (int i = 0; i < n; i++) scanf("%d", &arr[i]);

    sort_desc(arr, n);
    for (int i = 0; i < n; i++) printf("%d ", arr[i]);
    printf("\n");

    free(arr);
    return 0;
}
