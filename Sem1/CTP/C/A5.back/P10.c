#include <stdio.h>

int main() {
    int n;
    printf("Please input the size of array: ");
    scanf("%d", &n);

    double arr[n];
    printf("Input the elements of array, space seperated: ");
    for (int i = 0; i < n; i++) scanf("%lf", &arr[i]);

    if (n <= 0) {
        printf("Give 1 or more elements\n");
        return 1;
    }

    if (n == 1) {
        printf("For single element array, the largest value is the element itself: %f\n", arr[0]);
        return 0;
    }

    double max = arr[0], second_max = arr[0];
    for (int i = 1; i < n; i++) {
        if (arr[i] > max) {
            second_max = max;
            max = arr[i];
        } else if (arr[i] > second_max) {
            // edge case if max is already greater and things aren't checked against second_max
            second_max = arr[i];
        }
    }

    printf("Largest and Second Largest numbers respectively are: %lf and %lf\n", max, second_max);

    return 0;
}
