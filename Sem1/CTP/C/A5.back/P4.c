#include <stdio.h>

int main() {
    int n;
    printf("Number of elements: ");
    scanf("%d", &n);

    float arr[n];
    printf("Array elements (space seperated): ");
    for (int i = 0; i < n; i++) scanf("%f", &arr[i]);

    float find_element;
    printf("Enter the element to be found: ");
    scanf("%f", &find_element);

    int idx = -1;
    int start = 0, end = n-1, mid = (end+start)/2;
    while (start <= end) {
        if (find_element < arr[mid]) end = mid - 1;
        else if (find_element > arr[mid]) start = mid + 1;
        else {
            idx = mid;
            break;
        }
        mid = (end+start)/2;
    }

    if (idx != -1) {
        while (idx >= 0 && arr[idx] == find_element) idx--;
        while (++idx < n && arr[idx] == find_element) printf("key %f is found at index %d\n", find_element, idx);
    } else {
        printf("key %f is not found in the given array\n", find_element);
    }

    return 0;
}
