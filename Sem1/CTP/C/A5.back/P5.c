#include <stdio.h>

int main() {
    int size;
    printf("Size of string: ");
    scanf("%d", &size);

    char string[size+1];
    printf("Input the string: ");

    // if old \n is read ignore
    char *input_ptr = &string[0];
    if ((string[0] = getc(stdin)) != '\n') input_ptr++;
    scanf("%[^\n]s", input_ptr);

    // if size == odd, then size/2 will be truncated, and anyways don't need to check middle character
    for (int i = 0; i < size/2; i++) {
        if (string[i] != string[size-1-i]) {
            printf("The given string is not a palindrome\n");
            return 0;
        }
    }
    printf("The given string is a palindrome\n");

    return 0;
}
