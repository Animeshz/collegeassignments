#include <stdio.h>

int main() {
    int size;
    printf("Size of string: ");
    scanf("%d", &size);

    char string[size+1];
    printf("Input the string: ");

    // if old \n is read ignore
    char *input_ptr = &string[0];
    if ((string[0] = getc(stdin)) != '\n') input_ptr++;
    scanf("%[^\n]s", input_ptr);

    for (int i = 0; i <= size-4; i++) {
        int cnt = 0;
        while (string[i] == 'a' || string[i] == 'A') {
            cnt++, i++;
            if (cnt == 4) {
                printf("Yes\n");
                return 0;
            }
        }
    }

    printf("No\n");
    return 0;
}
