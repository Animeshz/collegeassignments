#include <stdio.h>

int main() {
    int size;
    printf("Size of string: ");
    scanf("%d", &size);

    char string[size+1];
    printf("Input the string: ");

    // if old \n is read ignore
    char *input_ptr = &string[0];
    if ((string[0] = getc(stdin)) != '\n') input_ptr++;
    scanf("%[^\n]s", input_ptr);

    for (int i = 0; i < size; i++) {
        if (string[i] == 'a') {
            string[i] = 'b';
        } else if (string[i] == 'A') {
            string[i] = 'B';
        }
    }
    printf("%s\n", string);
    return 0;
}
