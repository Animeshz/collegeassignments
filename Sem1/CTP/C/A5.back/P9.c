#include <stdio.h>
#include <math.h>

double perimeter(double a, double b, double c);
double area(double a, double b, double c);

int main() {
    double a, b, c;
    printf("Input side lengths of the triangle, space separated: ");
    scanf("%lf %lf %lf", &a, &b, &c);

    printf("Perimeter of the triangle = %lf\n", perimeter(a, b, c));
    printf("Area of the triangle = %lf\n", area(a, b, c));

    return 0;
}

double perimeter(double a, double b, double c) {
    return a + b + c;
}

/*
 * Area of triangle = 1/2 * any side * perpendicular drawn from oppoiste vertex into it
 * A = 1/2 * a * p
 *
 * perpendicular = any other side * sin(angle b/w chosen side and other side)
 * p = b * sin(beta)
 *
 * And
 * cos(beta) by cosine law = (a^2+b^2-c^2) / 2ab
 */
double area(double a, double b, double c) {
    if (a == 0 || b == 0 || c == 0) return 0;

    double cos_beta = (a*a + b*b - c*c) / (2*a*b);
    double sin_beta = sqrt(1-cos_beta*cos_beta);

    double area = 0.5 * a * b * sin_beta;
    return area;
}
