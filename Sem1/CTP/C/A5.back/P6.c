#include <stdio.h>

void swap(float *a, float *b) {
    float tmp = *a;
    *a = *b;
    *b = tmp;
}

int main() {
    int n;
    printf("Please input the size of array: ");
    scanf("%d", &n);

    float arr[n];
    printf("Input the elements of array, space seperated: ");
    for (int i = 0; i < n; i++) scanf("%f", &arr[i]);

    // We don't need to swap the middle element if n==odd, and n/2 will ignore it
    for (int i = 0; i < n/2; i++) {
        swap(&arr[i], &arr[n-1-i]);
    }

    for (int i = 0; i < n; i++) printf("%f ", arr[i]);
    printf("\n");

    return 0;
}
