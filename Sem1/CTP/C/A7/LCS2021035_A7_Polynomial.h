#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Question 1 */

// Stores the polynomial P(x) = a + bx + cx^2 + ...
typedef struct Px {
    int n;                       // terms
    float *terms_coeff;          // a, b, c, ..
    unsigned int *terms_power;   // 0, 1, 2, ..
} Px;

void sort_desc_by_first(unsigned int *arr, float *arr2, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = i+1; j < n; j++) {
            if (arr[i] < arr[j]) {
                int t1 = arr[i], t2 = arr2[i];
                arr[i] = arr[j], arr2[i] = arr2[j];
                arr[j] = t1, arr2[j] = t2;
            }
        }
    }
}

void parse(char *str, Px *poly) {
    int num_terms = 0, size = 8, i = 0, neg = 0, char_read;
    float *coeff = malloc(8*sizeof(int));
    unsigned int *power = malloc(8*sizeof(int));

    while (1) {
        while (str[i] == ' ') i++;
        if (str[i] == '\0') break;
        if (++num_terms > size) {
            size = (int) (1.5*size);
            coeff = realloc(coeff, size);
            power = realloc(power, size);
        }

        if (str[i] == '+') i++; else if (str[i] == '-') { i++; neg=1; }
        sscanf(str+i, "%f%n", &coeff[num_terms-1], &char_read);
        i += char_read; while (str[i] == ' ') i++;
        if (neg) { coeff[num_terms-1] *= -1; neg=0; }

        if (str[i] != 'x') { power[num_terms-1] = 0; continue; } else i++;
        if (str[i] != '^') { power[num_terms-1] = 1; continue; } else i++;
        sscanf(str+i, "%u%n", &power[num_terms-1], &char_read);
        i += char_read;
    }

    poly->n = num_terms;
    sort_desc_by_first(power, coeff, num_terms);
    poly->terms_coeff = coeff;
    poly->terms_power = power;
}

void print(Px poly) {
    for (int i = 0; i < poly.n; i++) {
        if (poly.terms_coeff[i] == 0) continue;
        printf("+ %gx^%u ", poly.terms_coeff[i], poly.terms_power[i]);
    }
    printf("\n");
}

void cleanup(Px poly) {
    free(poly.terms_coeff);
    free(poly.terms_power);
}

/* Question 2 */

Px sum(Px p1, Px p2) {
    Px pf; pf.n = 0;
    int size = p1.n + p2.n, i, j;
    pf.terms_coeff = malloc(size*sizeof(float));
    pf.terms_power = malloc(size*sizeof(unsigned int));

    for (i = 0, j = 0; i < p1.n && j < p2.n;) {
        if (p1.terms_power[i] < p2.terms_power[j]) {
            pf.terms_power[pf.n] = p2.terms_power[j];
            pf.terms_coeff[pf.n] = p2.terms_coeff[j];
            pf.n++, j++;
        } else if (p1.terms_power[i] > p2.terms_power[j]) {
            pf.terms_power[pf.n] = p1.terms_power[i];
            pf.terms_coeff[pf.n] = p1.terms_coeff[i];
            pf.n++, i++;
        } else {
            pf.terms_power[pf.n] = p1.terms_power[i];
            pf.terms_coeff[pf.n] = p1.terms_coeff[i] + p2.terms_coeff[j];
            pf.n++, i++, j++;
        }
    }
    while (i < p1.n) {
        pf.terms_power[pf.n] = p1.terms_power[i];
        pf.terms_coeff[pf.n] = p1.terms_coeff[i];
        pf.n++, i++;
    }
    while (j < p2.n) {
        pf.terms_power[pf.n] = p2.terms_power[j];
        pf.terms_coeff[pf.n] = p2.terms_coeff[j];
        pf.n++, j++;
    }

    return pf;
}

/* Question 3 */

Px multiply(Px p1, Px p2) {
    Px pf; pf.n = 0;
    int size = p1.n * p2.n;
    pf.terms_coeff = malloc(size*sizeof(float));
    pf.terms_power = malloc(size*sizeof(unsigned int));

    for (int i = 0; i < p1.n; i++) {
        for (int j = 0; j < p2.n; j++) {
            unsigned int power = p1.terms_power[i] + p2.terms_power[j];
            float coeff = p1.terms_coeff[i] * p2.terms_coeff[j];

            int flag = 0;
            for (int k = 0; k < pf.n; k++) {
                if (pf.terms_power[k] == power) {
                    pf.terms_coeff[k] += coeff;
                    flag = 1;
                }
            }
            if (!flag) {
                pf.terms_power[pf.n] = power;
                pf.terms_coeff[pf.n] = coeff;
                pf.n++;
            }
        }
    }

    return pf;
}

