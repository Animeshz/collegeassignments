#include <stdio.h>

int main() {
    int n;
    printf("Total number of complex number (n): ");
    scanf("%d", &n);

    if (n <= 3) {
        printf("Please input n > 3\n");
        return 1;
    }

    // prod_real, prod_imag
    float a = 1, b = 0;
    for (int i = 0; i < n; i++) {
        float c, d;
        printf("Input the complex number %d (in a+bi format, including coefficients): ", i+1);
        scanf("%f%fi", &c, &d);
        float local_real = a*c - b*d, local_imag = b*c+a*d;
        a = local_real, b = local_imag;
    }

    printf("The net product of complex numbers is: %.2f + %.2fi\n", a, b);
    return 0;
}
