#include "LCS2021035_A7_Polynomial.h"

int main() {
    char string[10000];
    printf("Input the polynomial: ");
    char *input_ptr = &string[0];
    if ((string[0] = getc(stdin)) != '\n') input_ptr++; // if old \n is read ignore
    scanf("%[^\n]s", input_ptr);  // read till new line (spaces skipping)

    Px polynomial = {};
    parse(string, &polynomial);
    print(polynomial);
    cleanup(polynomial);
    return 0;
}
