#include "LCS2021035_A7_Polynomial.h"

int main() {
    char string[10000], *input_ptr;
    Px p1, p2, pf;

    printf("Input the first polynomial: ");
    input_ptr = &string[0];
    if ((string[0] = getc(stdin)) != '\n') input_ptr++; // if old \n is read ignore
    scanf("%[^\n]s", input_ptr);  // read till new line (spaces skipping)
    parse(string, &p1);

    printf("Input the second polynomial: ");
    input_ptr = &string[0];
    if ((string[0] = getc(stdin)) != '\n') input_ptr++; // if old \n is read ignore
    scanf("%[^\n]s", input_ptr);  // read till new line (spaces skipping)
    parse(string, &p2);

    pf = sum(p1, p2);

    print(pf);
    cleanup(p1);
    cleanup(p2);
    cleanup(pf);
    return 0;
}
