#include <stdio.h>

int main() {
    int n;
    printf("Total number of complex number (n): ");
    scanf("%d", &n);

    if (n <= 3) {
        printf("Please input n > 3\n");
        return 1;
    }

    float sum_real = 0, sum_imag = 0;
    for (int i = 0; i < n; i++) {
        float real, imag;
        printf("Input the complex number %d (in a+bi format, including coefficients): ", i+1);
        scanf("%f%fi", &real, &imag);
        sum_real += real, sum_imag += imag;
    }

    printf("The sum of complex numbers is: %.2f + %.2fi\n", sum_real, sum_imag);
    return 0;
}
