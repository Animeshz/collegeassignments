#include <stdio.h>
#include <stdlib.h>

float read_spaced_f(char *in, int *total_read) {
    int i = 0, read_chars, neg = 0; float read; char next;
    while (in[i] == ' ') i++;
    sscanf(in+i, "%c%n", &next, &read_chars); i += read_chars;

    if (next == '-') neg = 1;
    else if (next == '+');
    else if ((0 <= next - '0' && next - '0' <= 9) || next == 'i') i--;
    else { printf("Not a complex number\n"); exit(1); }

    if (in[i] == 'i') return 1;  // +i or -i
    sscanf(in+i, "%f%n", &read, &read_chars); i += read_chars;
    while (in[i] == ' ') i++;
    *total_read += i;
    return read * (neg ? -1 : 1);
}

void parse(char *in, float *real, float *imag) {
    int i = 0;
    *real = read_spaced_f(in+i, &i);
    if (in[i] == '\0') return;
    if (in[i] == 'i') { *imag = *real; *real = 0; return; }
    *imag = read_spaced_f(in+i, &i);
}

int main() {
    int n;
    printf("Total number of complex number (n): ");
    scanf("%d", &n);

    //if (n <= 3) {
    //    printf("Please input n > 3\n");
    //    return 1;
    //}

    char string[50], *input_ptr;
    float sum_real = 0, sum_imag = 0;
    for (int i = 0; i < n; i++) {
        printf("Input the complex number %d (in a+bi format): ", i+1);
        input_ptr = &string[0];
        if ((string[0] = getc(stdin)) != '\n') input_ptr++; // if old \n is read ignore
        scanf("%[^\n]s", input_ptr);  // read till new line (spaces skipping)

        float real, imag;
        parse(input_ptr, &real, &imag);
        sum_real += real, sum_imag += imag;
    }
    printf("The sum of complex numbers is: %.2f + %.2fi\n", sum_real, sum_imag);

    return 0;
}
