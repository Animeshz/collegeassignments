#include <stdio.h>

int main() {
    int a, b, c;
    printf("Input 3 numbers in given format\n");
    printf("a b c: ");
    scanf("%d %d %d", &a, &b, &c);

    int max_ab = a > b ? a : b;
    int max_bc = b > c ? b : c;
    int max = max_ab > max_bc ? max_ab : max_bc;

    printf("The maximum of a, b and c is %d\n", max);
}
