#include <stdio.h>

/**
 * Final Amount = Principal Amount * [ rate * time ] / (100 * 365)
 * where rate is in %, and time in days
 */
int main() {
    int age, principal, days;
    printf("Please provide the age, principal amount and time duration in given format\n");
    printf("<age> <principal> <days>: ");  // Example: 18 2000 90
    scanf("%d %d %d", &age, &principal, &days);

    if (age < 0 || principal < 0 || days < 0) {
        printf("Please provide proper non-negative value\n");
        return 1;
    } else if (days > 365) {
        printf("Rate is unknown for days higher than 365\n");
        return 1;
    }

    double rate = 0, interest_factor, final_amount;
    if (age >= 60) {
        if (days >= 7 && days <= 45) rate = 6.25;
        else if (days >= 46 && days <= 179) rate = 6.75;
        else if (days >= 180 && days <= 210) rate = 6.85;
        else if (days >= 211 && days <= 365) rate = 6.9;
    } else {
        if (days >= 7 && days <= 45) rate = 5.75;
        else if (days >= 46 && days <= 179) rate = 6.25;
        else if (days >= 180 && days <= 210) rate = 6.35;
        else if (days >= 211 && days <= 365) rate = 6.4;
    }

    interest_factor = rate * days / (100 * 365);
    final_amount = principal * (1 + interest_factor);

    printf("Final amount you'll receive by %d days will be %.2f rupees\n", days, final_amount);
    return 0;
}
