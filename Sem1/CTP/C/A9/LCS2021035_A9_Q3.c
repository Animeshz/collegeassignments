#include <stdio.h>
#include <string.h>

typedef struct Student {
    char name[50];
    int roll_no;
    int marks[5];
} Student;

int startsWith(const char *str, const char *pre) {
    size_t lenpre = strlen(pre), lenstr = strlen(str);
    return lenstr < lenpre ? 0 : memcmp(pre, str, lenpre) == 0;
}

int main() {
    Student students[10];
    for (int i = 0; i < 10; i++) {
        students[i].roll_no = i+1;
        students[i].marks[0] = i;
        students[i].marks[1] = i+1;
        students[i].marks[2] = 2*i;
        students[i].marks[3] = i*i;
        students[i].marks[4] = i*(i-1);
    }
    strcpy(students[0].name, "Arjun");
    strcpy(students[1].name, "Mohan");
    strcpy(students[2].name, "Rohan");
    strcpy(students[3].name, "Aditya");
    strcpy(students[4].name, "Rinki");
    strcpy(students[5].name, "Pinki");
    strcpy(students[6].name, "Raushan");
    strcpy(students[7].name, "Tikki");
    strcpy(students[8].name, "Ashmeet");
    strcpy(students[9].name, "Jasmine");

    int q;
    printf("How many queries? ");
    scanf("%d", &q);

    int roll_no, subject_number;
    char input[50];
    for (int i = 0; i < q; i++) {
        Student searched;
        printf("Search by name or rollno? ");
repeat:
        scanf("%s", input);
        if (startsWith(input, "name")) {
            printf("Student's name? ");
            char *input_ptr = &input[0];
            if ((input[0] = getc(stdin)) != '\n') input_ptr++;
            scanf("%[^\n]s", input_ptr);
            for (int j = 0; j < 10; j++) {
                if (strcmp(input, students[j].name) == 0) {
                    searched = students[j];
                    break;
                }
            }
        } else if (startsWith(input, "roll")) {
            printf("Student's roll no? ");
            scanf("%d", &roll_no);
            for (int j = 0; j < 10; j++) {
                if (roll_no == students[j].roll_no) {
                    searched = students[j];
                    break;
                }
            }
        } else {
            printf("Please enter name or roll only: ");
            goto repeat;
        }
        printf("Subject number? ");
        scanf("%d", &subject_number);

        printf("Student '%s' with roll no '%d' has got '%d' in subject %d\n", searched.name, searched.roll_no, searched.marks[subject_number-1], subject_number);
    }

    return 0;
}
