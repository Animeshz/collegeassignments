#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct Holder {
    char id;
    int max;
    int holding;
    int *current;  // current[max];
} Holder;

void move_hanoi(int n, Holder *src, Holder *dest, Holder *aux) {
    if (n==0) return;
    move_hanoi(n-1, src, aux, dest);
    printf("Moving %d at %c to %c\n", n, src->id, aux->id);
    dest->current[dest->holding++] = src->current[--src->holding];
    move_hanoi(n-1, aux, dest, src);
}
void print_state(Holder a, Holder b, Holder c) {
    printf("A: ");
    for (int i = 0; i < a.holding; i++) printf("%d ", a.current[i]);
    printf("\nB: ");
    for (int i = 0; i < b.holding; i++) printf("%d ", b.current[i]);
    printf("\nC: ");
    for (int i = 0; i < c.holding; i++) printf("%d ", c.current[i]);
    printf("\n");
}

int main() {
    int n;
    printf("Total rings to shift: ");
    scanf("%d", &n);

    Holder a = { id: 'A', max: n, holding: n, current: malloc(sizeof(int)*n) };
    Holder b = { id: 'B', max: n, holding: 0, current: malloc(sizeof(int)*n) };
    Holder c = { id: 'C', max: n, holding: 0, current: malloc(sizeof(int)*n) };
    for (int i = 0; i < n; i++) a.current[i] = n-i;
    memset(b.current, 0, sizeof(int)*b.max);
    memset(c.current, 0, sizeof(int)*c.max);

    print_state(a, b, c);
    move_hanoi(n, &a, &b, &c);
    print_state(a, b, c);
    return 0;
}
