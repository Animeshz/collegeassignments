#include <stdio.h>
#include <math.h>

typedef struct Point {
    float x;
    float y;
} Point;

float distance(Point p1, Point p2) {
    return sqrt((p1.x-p2.x) * (p1.x-p2.x) + (p1.y-p2.y) * (p1.y-p2.y));
}

Point mid_pt(Point p1, Point p2) {
    return (Point) { .x = (p1.x+p2.x)/2, .y = (p1.y+p2.y)/2 };
}

float area(Point p1, Point p2, Point p3) {
    return 0.5 * ( p1.x * (p2.y-p3.y) + p2.x * (p3.y-p1.y) + p3.x * (p1.y-p2.y) );
}

int main() {
    Point p1, p2, p3;

    printf("Input point 1 (x y): ");
    scanf("%f %f", &p1.x, &p1.y);
    printf("Input point 2 (x y): ");
    scanf("%f %f", &p2.x, &p2.y);

    printf("Distance between the points: %.4g\n", distance(p1, p2));
    Point mid = mid_pt(p1, p2);
    printf("Mid point of line joining the given points: (%.4g, %.4g)\n", mid.x, mid.y);

    printf("Input point 3 (x y): ");
    scanf("%f %f", &p3.x, &p3.y);
    printf("Area of triangle formed by given 3 points: %.4g\n", area(p1, p2, p3));

    return 0;
}
