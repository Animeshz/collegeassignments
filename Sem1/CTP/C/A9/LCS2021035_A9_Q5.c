#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char to_lower(char c) {
    if ('A' <= c && c <= 'Z') return c - 'A' + 'a';
    else return c;
}

int frequency(char *filename, char *substr) {
    FILE *f = fopen(filename, "r");
    if (0 == f) {
        printf("file can't be opened \n");
        return -1;
    }

    int freq = 0, char_match = 0;
    char ch;
    do {
        ch = fgetc(f);
        if (to_lower(substr[char_match]) != to_lower(ch)) char_match = 0; else char_match++;
        if (substr[char_match] == '\0') {
            freq++;
            char_match = 0;
        }
    } while (ch != EOF);
    fclose(f);
    return freq;
}

int main() {
    char filename[1000], substring[1000], *input_ptr;
    printf("Input filename to be read: ");
    input_ptr = &filename[0];
    if ((filename[0] = getc(stdin)) != '\n') input_ptr++;
    scanf("%[^\n]s", input_ptr);
    printf("Input substring: ");
    input_ptr = &substring[0];
    if ((substring[0] = getc(stdin)) != '\n') input_ptr++;
    scanf("%[^\n]s", input_ptr);

    int freq = frequency(filename, substring);
    printf("'%s' has appeared %d times in %s\n", substring, freq, filename);

    return 0;
}
