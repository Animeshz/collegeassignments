#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void input_str(char *str) {
    if ((str[0] = getc(stdin)) != '\n') str++;
    scanf("%[^\n]s", str);
}

char to_lower(char c) {
    if ('A' <= c && c <= 'Z') return c - 'A' + 'a';
    return c;
}

int main() {
    char s1[1000], s2[1000];
    printf("Input String 1: ");
    input_str(s1);
    printf("Input String 1: ");
    input_str(s2);

    int n1 = 0, n2 = 0;
    while (s1[n1] != '\0') { s1[n1] = to_lower(s1[n1]); n1++; }
    while (s2[n2] != '\0') { s2[n2] = to_lower(s2[n2]); n2++; }

    if (strcmp(s1, s2) == 0) printf("Same!\n");
    else printf("Not same!\n");;

    return 0;
}
