#include <stdio.h>

typedef struct Complex {
    float real;
    float imag;
} Complex;

Complex conjugate(Complex c) {
    c.imag *= -1;
    return c;
}

Complex add(Complex c1, Complex c2) {
    return (Complex) { .real = c1.real + c2.real, .imag = c1.imag + c2.imag };
}
Complex subtract(Complex c1, Complex c2) {
    return (Complex) { .real = c1.real - c2.real, .imag = c1.imag - c2.imag };
}
Complex multiply(Complex c1, Complex c2) {
    return (Complex) { .real = c1.real * c2.real - c1.imag * c2.imag, .imag = c1.real * c2.imag + c1.imag * c2.real };
}
Complex divide(Complex c1, Complex c2) {
    Complex rationalized_multiplication = multiply(c1, conjugate(c2));
    float div = c2.real*c2.real+c2.imag*c2.imag;
    return (Complex) { .real = rationalized_multiplication.real/div, .imag = rationalized_multiplication.imag/div };

}

int main() {
    float r, i;
    printf("Input a complex number (a b): ");
    scanf("%f %f", &r, &i);
    Complex number1 = { real: r, imag: i };
    printf("Input another complex number (a b): ");
    scanf("%f %f", &r, &i);
    Complex number2 = { real: r, imag: i };

    Complex add_res = add(number1, number2);
    Complex sub_res = subtract(number1, number2);
    Complex mul_res = multiply(number1, number2);
    Complex div_res = divide(number1, number2);
    printf("add result: %.2g%+.2gi\n", add_res.real, add_res.imag);
    printf("subtract result: %.2g%+.2gi\n", sub_res.real, sub_res.imag);
    printf("multiply result: %.2g%+.2gi\n", mul_res.real, mul_res.imag);
    printf("divide result: %.2g%+.2gi\n", div_res.real, div_res.imag);
    return 0;
}
