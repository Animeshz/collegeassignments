#include <stdio.h>

int main() {
    // Pattern 1:
    // ****
    // ***
    // **
    // *
    // My Code:
    // int n = 5;
    // for (int i = 0; i < 5; i++) {
    //     for (int j = 0; j < n-i; j++) printf("* ");
    //     printf("\n");
    // }
    // Their Code:
    int n = 5;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i>=j) printf("*");
            //if (i<=j) printf("*");
            //if (i+j < n) printf("*");
            //if (i+j >= n-1) printf("*");
            else printf(" ");
        }
        printf("\n");
    }

    // Pattern 2:
    // 4
    // 2 1 4 3
    //   *
    //   **
    // * **
    // ****
    // My Code:
    // int n;
    // scanf("%d", &n);
    // int arr[n];
    // for (int i = 0; i < n; i++) scanf("%d", &arr[i]);
    // for (int i = 0; i < n; i++) {
    //     for (int j = 0; j < n; j++) {
    //         if (arr[j] - (n-i-1) > 0) printf("*");
    //         else printf(" ");
    //     }
    //     printf("\n");
    // }
    // Their Code:
    //int n;
    //scanf("%d", &n);
    //int arr[n];
    //for (int i = 0; i < n; i++) scanf("%d", &arr[i]);
    //int max = arr[0];
    //for (int i = 0; i < n; i++) if (arr[i] > max) max = arr[i];
    //for (int i = 0; i < max; i++) {
    //    for (int j = 0; j < n; j++) {
    //        if (arr[j] >= max - i) printf("*");
    //        else printf(" ");
    //    }
    //    printf("\n");
    //}

    // Probelm 3:
    // The output
    //int arr[] = {1, 2, 3};
    //printf("%u, %u", arr, &arr);

    // Problem 4:
    // Value of i, j, m     (ans: 3 2 15)
    //int a[5] = {5, 1, 15, 20, 25};
    //int i, j, m;
    //i = ++a[1];
    //j = a[1]++;
    //m = a[i++];
    //printf("%d, %d, %d", i, j, m);

    // Problem 5:
    // The output: 0    (reason: partial element initialization makes other 0)
    //int arr[10] = {1, 2, 3, 4, 5};
    //printf("%d", arr[5]);

    // Problem 6:
    // The output: Garbage value, not segfault because stack address might be accessible.
    //int a[10];
    //printf("%d, %d", a[-1], a[12]);

    // Problem 7:
    // The output: 4
    //float arr[] = {1.0, 2.4, 3.6, 4.2};
    //printf("%d", sizeof(arr)/sizeof(a[0]));

    // Problem 8:
    // The output: static is assigned only once
    //static int var = 5;
    //printf("%d", var--);
    //if (var) main();

    // Problem 9:
    // Determine output: 9
    //int abc(int i) {return (i++);}
    //void main_dummy() {int i = abc(10); printf("%d", --i);}
    //main_dummy();

    // Problem 10:
    // The output: 4
    //int f(int x) { if (x <= 4) return x; return f(--x); }
    //void main_dummy2() { printf("%d ", f(7)); }
    //main_dummy2();


}

// Problem 11:
// Whcih error? (NET 2020): Redeclaration of int a;  (c++ gives both redeclaration of int a, as well as missing return type)
//f(int a, int b) { int a; a = 20; return a; }

