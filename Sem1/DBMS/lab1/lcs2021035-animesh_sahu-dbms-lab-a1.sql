DROP DATABASE IF EXISTS hospital_management;
CREATE DATABASE hospital_management;
USE hospital_management;

CREATE TABLE Hospital (hospital_id int PRIMARY KEY AUTO_INCREMENT, name varchar(255));
CREATE TABLE Doctor (doctor_id int PRIMARY KEY AUTO_INCREMENT, dr_name varchar(255), hospital_id int REFERENCES Hospital(hospital_id));

CREATE TABLE Patient (patient_id int PRIMARY KEY AUTO_INCREMENT, name varchar(255));
CREATE TABLE IndoorPatient (patient_id int PRIMARY KEY REFERENCES Patient(patient_id), room_no int, ipd_id int);
CREATE TABLE OutdoorPatient (patient_id int PRIMARY KEY REFERENCES Patient(patient_id), charge varchar(255), opd_id int);

CREATE TABLE MedicalRecord (mr_id int PRIMARY KEY AUTO_INCREMENT, report_name varchar(255), patient_id int REFERENCES Patient(patient_id));

-- A separate table implementing many-to-many relationship between Patient and Doctor
CREATE TABLE TreatmentRecord (patient_id int REFERENCES Patient(patient_id), doctor_id int REFERENCES Doctor(doctor_id));


-- Sample data

SET @last_hospital_id = 0;
SET @last_doctor_id = 0;
SET @last_patient_id = 0;

INSERT INTO Hospital VALUES (DEFAULT, "VY Hospital"); SELECT LAST_INSERT_ID() INTO @last_hospital_id;
INSERT INTO Doctor VALUES (DEFAULT, "Dr. Rohan Kumar", @last_hospital_id); SELECT LAST_INSERT_ID() INTO @last_doctor_id;

INSERT INTO Patient VALUES (DEFAULT, "Mohan Gupta"); SELECT LAST_INSERT_ID() INTO @last_patient_id;
INSERT INTO IndoorPatient VALUES (@last_patient_id, 544, 203);
INSERT INTO MedicalRecord VALUES (DEFAULT, "Eye Test Report", @last_patient_id);
INSERT INTO TreatmentRecord VALUES (@last_patient_id, @last_doctor_id);

INSERT INTO Patient VALUES (DEFAULT, "Sushma Singh"); SELECT LAST_INSERT_ID() INTO @last_patient_id;
INSERT INTO OutdoorPatient VALUES (@last_patient_id, "Rohan Yadav", 104);
INSERT INTO MedicalRecord VALUES (DEFAULT, "Blood Corpus Defect / Sickle Cell Report", @last_patient_id);
INSERT INTO TreatmentRecord VALUES (@last_patient_id, @last_doctor_id);

INSERT INTO Hospital VALUES (DEFAULT, "CIMS"); SELECT LAST_INSERT_ID() INTO @last_hospital_id;
INSERT INTO Doctor VALUES (DEFAULT, "Dr. Ramu Prasad", @last_hospital_id); SELECT LAST_INSERT_ID() INTO @last_doctor_id;

INSERT INTO Patient VALUES (DEFAULT, "Rajiv Mehra"); SELECT LAST_INSERT_ID() INTO @last_patient_id;
INSERT INTO IndoorPatient VALUES (@last_patient_id, 543, 204);
INSERT INTO MedicalRecord VALUES (DEFAULT, "Eye Test Report", @last_patient_id);
INSERT INTO TreatmentRecord VALUES (@last_patient_id, @last_doctor_id);


SELECT * FROM Hospital;
SELECT * FROM Doctor;
SELECT * FROM MedicalRecord;
SELECT * FROM Patient;
SELECT * FROM IndoorPatient;
SELECT * FROM OutdoorPatient;
SELECT * FROM TreatmentRecord;
