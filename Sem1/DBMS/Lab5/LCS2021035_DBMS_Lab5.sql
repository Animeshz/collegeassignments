DROP DATABASE IF EXISTS Lab5;
CREATE DATABASE Lab5;
USE Lab5;

CREATE TABLE ACTOR_Animesh_LCS035(
    Act_id int PRIMARY KEY,
    Act_Name varchar(30) NOT NULL,
    Act_Gender ENUM("M",  "F",  "T") NOT NULL
);

CREATE TABLE DIRECTOR_Animesh_LCS035(
    Dir_id int PRIMARY KEY,
    Dir_Name varchar(30) NOT NULL,
    Dir_Phone bigint
);

CREATE TABLE MOVIES_Animesh_LCS035(
    Mov_id int PRIMARY KEY,
    Mov_Title varchar(30) NOT NULL,
    Mov_Year int NOT NULL,
    Mov_Lang varchar(20) NOT NULL,
    Dir_id int REFERENCES DIRECTOR_Animesh_LCS035(Dir_id)
);

CREATE TABLE MOVIE_CAST_Animesh_LCS035(
    Act_id int REFERENCES ACTOR_Animesh_LCS035(Act_id),
    Mov_id int REFERENCES MOVIES_Animesh_LCS035(Mov_id),
    Role varchar(30) NOT NULL,
    PRIMARY KEY (Act_id,  Mov_id)
);

CREATE TABLE RATING_Animesh_LCS035(
    Mov_id int PRIMARY KEY REFERENCES MOVIES_Animesh_LCS035(Mov_id),
    Rev_Stars int
);

INSERT INTO ACTOR_Animesh_LCS035 VALUES
    (111, "DEEPA SANNIDHI", "F"),
    (222, "SUDEEP", "M"),
    (333, "PUNEETH", "M"),
    (444, "DHIGANTH", "M"),
    (555, "ANGELA", "F");

INSERT INTO DIRECTOR_Animesh_LCS035 VALUES
    (101, "HITCHCOCK", 112267809),
    (102, "RAJ MOULI", 152358709),
    (103, "YOGARAJ", 272337808),
    (104, "STEVEN SPIELBERG", 363445678),
    (105, "PAVAN KUMAR", 385456809);

INSERT INTO MOVIES_Animesh_LCS035 VALUES
    (1111, "LASTWORLD", 2009, "ENGLISH", 104),
    (2222, "EEGA", 2010, "TELUGU", 102),
    (4444, "PARAMATHMA", 2012, "KANNADA", 103),
    (3333, "MALE", 2006, "KANNADA", 103),
    (5555, "MANASARE", 2010, "KANNADA", 103),
    (6666, "REAR WINDOW", 1954, "ENGLISH", 101),
    (7777, "NOTORIOUS", 1946, "ENGLISH", 101);

INSERT INTO MOVIE_CAST_Animesh_LCS035 VALUES
    (222, 2222, "VILAN"),
    (333, 4444, "HERO"),
    (111, 4444, "HEROIN"),
    (444, 3333, "GUEST"),
    (444, 5555, "HERO"),
    (555, 7777, "MOTHER");

INSERT INTO RATING_Animesh_LCS035 VALUES
    (1111, 3),
    (2222, 4),
    (3333, 3),
    (5555, 4),
    (4444, 5);

\! echo "1. List the titles of all movies directed by ‘HITCHCOCK’."
SELECT Mov_Title FROM DIRECTOR_Animesh_LCS035 JOIN MOVIES_Animesh_LCS035 USING (Dir_id) WHERE Dir_Name = "HITCHCOCK";

\! echo "2. Find the movie names where one or more actors acted in two or more movies."
SELECT Act_id, Act_Name, GROUP_CONCAT(Mov_Title) FROM MOVIES_Animesh_LCS035 JOIN MOVIE_CAST_Animesh_LCS035 USING (Mov_id) JOIN ACTOR_Animesh_LCS035 USING (Act_id) GROUP BY Act_id HAVING COUNT(Mov_id) >= 2;

\! echo "3. List all actors who acted in a movie before 2000 and also in a movie after 2015."
SELECT * FROM ACTOR_Animesh_LCS035 WHERE Act_id IN (
    SELECT Act_id FROM MOVIES_Animesh_LCS035 JOIN MOVIE_CAST_Animesh_LCS035 USING (Mov_id) GROUP BY Act_id HAVING MIN(Mov_Year) < 2000 AND MAX(Mov_Year) > 2015
);

\! echo "4. Find the title of movies and number of stars for each movie that has at least one rating and find the highest number of stars that movie received. Sort the result by movie title."
\! echo "(i) Max stars"
SELECT Mov_Title, Rev_Stars FROM MOVIES_Animesh_LCS035 JOIN RATING_Animesh_LCS035 USING (Mov_id) WHERE Rev_Stars = (
    SELECT Rev_Stars FROM RATING_Animesh_LCS035 ORDER BY Rev_Stars DESC LIMIT 1
);
\! echo "(ii) Sorted movies having 1+ stars"
SELECT Mov_Title, Rev_Stars FROM MOVIES_Animesh_LCS035 JOIN RATING_Animesh_LCS035 USING (Mov_id) WHERE Rev_Stars >= 1 ORDER BY Mov_Title;

\! echo "5. Update rating of all movies directed by ‘STEVEN SPIELBERG’ to 5."
UPDATE RATING_Animesh_LCS035 SET Rev_Stars = 5 WHERE Mov_id IN (
    SELECT Mov_id FROM DIRECTOR_Animesh_LCS035 JOIN MOVIES_Animesh_LCS035 USING (Dir_id) WHERE Dir_Name = "STEVEN SPIELBERG"
);
SELECT * FROM RATING_Animesh_LCS035;
