DROP DATABASE IF EXISTS LAB3;
CREATE DATABASE LAB3;
USE LAB3;

-- As of 8.0.19, \! also works on Windows now.
\! echo "1. Create table Student (Rno, Name, DOB, Gender, Class, College, City, Marks)";
CREATE TABLE STUDENT_Animesh_LCS2021035(
    RNo int NOT NULL PRIMARY KEY,
    Name varchar(255) NOT NULL,
    DOB date NOT NULL,
    Gender ENUM("male", "female", "trans", "not specified") NOT NULL,
    Class varchar(15) NOT NULL,
    College varchar(255) NOT NULL,
    City varchar(30) NOT NULL,
    Marks int NOT NULL
);

\! echo "2. Insert 5 records in student table";
INSERT INTO STUDENT_Animesh_LCS2021035 VALUES
    (1, "Hrittik", "2002-09-21", "male", "CS", "IIITL", "Jaipur", 25),
    (5, "Patiala", "2001-06-27", "female", "CSB", "IIITL", "Amritsar", 50),
    (9, "Rohan", "2004-02-17", "male", "CSAI", "IIITL", "Surat", 22),
    (35, "Animesh", "2003-10-22", "male", "CS", "IIITL", "Bilaspur", 72),
    (151, "Ritika", "2003-04-22", "female", "IT", "IIITL", "Lucknow", 84)
;

\! echo "3. Display the information of all the students";
SELECT * FROM STUDENT_Animesh_LCS2021035;

\! echo "4. Display the detail structure of student table";
DESC STUDENT_Animesh_LCS2021035;

\! echo "5. Display Rno, Name and Class information of ‘Patiala’ students.";
SELECT RNo, Name, Class FROM STUDENT_Animesh_LCS2021035 WHERE Name = "Patiala";

\! echo "6. Display information on ascending order of marks";
SELECT * FROM STUDENT_Animesh_LCS2021035 ORDER BY Marks ASC;

\! echo "7. Change the marks of Rno 5 to 89.";
UPDATE STUDENT_Animesh_LCS2021035 SET Marks = "89" WHERE RNo = 5;
SELECT * FROM STUDENT_Animesh_LCS2021035;

\! echo "8. Change the name and city of Rno 9.";
UPDATE STUDENT_Animesh_LCS2021035 SET Name = "Rohan Kumar", city = "Ahmedabad" WHERE RNo = 9;
SELECT * FROM STUDENT_Animesh_LCS2021035;

\! echo "9. Delete the information of ‘Amritsar’ city records";
DELETE FROM STUDENT_Animesh_LCS2021035 WHERE city = "Amritsar";
SELECT * FROM STUDENT_Animesh_LCS2021035;

\! echo "10. Delete the records of student where marks<30.";
DELETE FROM STUDENT_Animesh_LCS2021035 WHERE Marks<30;
SELECT * FROM STUDENT_Animesh_LCS2021035;
