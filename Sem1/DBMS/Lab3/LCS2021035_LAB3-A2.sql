DROP DATABASE IF EXISTS Lab3;
CREATE DATABASE Lab3;
USE Lab3;

-- As of 8.0.19, \! also works on Windows now.
\! echo "1. Create table emp which has the following attributes (employee table)";
\! echo "(empno, ename, job, sal, deptno, commission)";
CREATE TABLE Employee_Animesh_LCS2021035(
    empno int NOT NULL PRIMARY KEY,
    ename varchar(50) NOT NULL,
    job varchar(50) NOT NULL,
    sal int NOT NULL,
    deptno int NOT NULL,
    commission int
);

\! echo "2. Insert appropriate records in above tables.";
INSERT INTO Employee_Animesh_LCS2021035 VALUES
    (107, "ABHINAV", "HR", 5000, 45 , NULL),
    (225, "ASHUTOSH", "Director", 8000, 40, NULL),
    (708, "ROHAN", "CA", 4500, 10, NULL),
    (799, "JAIKI", "Salesperson", 3700, 30, 220),
    (821, "ROHIT", "Support Specialist", 1400, 20, 310),
    (904, "KRITIKA", "Clerk", 1500, 20, 310),
    (937, "COOPER", "Product Manager", 3600, 32, NULL),
    (940, "ERIC", "Frontend Developer", 2800, 22, 320)
;

\! echo "3. Get employee no and employee name who works in dept no 10";
SELECT empno, ename FROM Employee_Animesh_LCS2021035 WHERE deptno = 10;

\! echo "4. Display the employee names of those clerks whose salary > 2000";
SELECT ename FROM Employee_Animesh_LCS2021035 WHERE sal > 2000;

\! echo "5. Display name and sal of Salesperson & Clerks";
SELECT ename, sal FROM Employee_Animesh_LCS2021035 WHERE job = "Salesperson" OR job = "Clerk";

\! echo "6. Display all details of employees whose salary between 2000 and 3000";
SELECT * FROM Employee_Animesh_LCS2021035 WHERE sal < 3000 AND sal > 2000;

\! echo "7. Display all details of employees whose dept no is 10, 20, or 30";
SELECT * FROM Employee_Animesh_LCS2021035 WHERE deptno = 10 OR deptno = 20 OR deptno = 30;

\! echo "8. Display name of those employees whose commission is NULL";
SELECT ename FROM Employee_Animesh_LCS2021035 WHERE commission IS NULL;

\! echo "9. Display dept number & salary in ascending order of dept number and within";
\! echo "each dept number, salary should be in descending order.";
SELECT deptno, sal FROM Employee_Animesh_LCS2021035 ORDER BY deptno ASC, sal DESC;

\! echo "10. Display name of employees that starts with ‘C’";
SELECT ename FROM Employee_Animesh_LCS2021035 WHERE ename REGEXP "^C";

\! echo "11. Display name of employees that ends with ‘C’";
SELECT ename FROM Employee_Animesh_LCS2021035 WHERE ename REGEXP "C$";

\! echo "12. Display name of employees having two ‘a’ or ‘A’ chars in the name";
SELECT ename FROM Employee_Animesh_LCS2021035 WHERE ename REGEXP "[aA].*?[aA]";

\! echo "13. Display the name of the employees whose second char is ‘b’ or ‘B’";
SELECT ename FROM Employee_Animesh_LCS2021035 WHERE ename REGEXP "^.[bB]";

\! echo "14. Display the name of the employees whose first or last char is ‘a’ or ‘A’";
SELECT ename FROM Employee_Animesh_LCS2021035 WHERE ename REGEXP "^[aA]|[aA]$";
