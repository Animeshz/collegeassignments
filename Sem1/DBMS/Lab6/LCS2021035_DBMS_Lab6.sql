DROP DATABASE IF EXISTS Lab6;
CREATE DATABASE Lab6;
USE Lab6;

CREATE TABLE BOOK_Animesh_LCS035(
    Book_id int PRIMARY KEY,
    Title varchar(50) NOT NULL,
    Publisher_Name varchar(50) NOT NULL,
    Pub_Year int
);

CREATE TABLE BOOK_AUTHORS_Animesh_LCS035(
    Book_id int PRIMARY KEY,
    Author_Name varchar(50) NOT NULL
);

CREATE TABLE PUBLISHER_Animesh_LCS035(
    Name varchar(50) PRIMARY KEY,
    Address varchar(50) NOT NULL,
    Phone varchar(15) NOT NULL
);

CREATE TABLE BOOK_COPIES_Animesh_LCS035(
    Book_id int NOT NULL,
    Branch_id int NOT NULL,
    No_of_Copies int,
    PRIMARY KEY (Book_id,  Branch_id)
);

CREATE TABLE BOOK_LENDING_Animesh_LCS035(
    Book_id int NOT NULL,
    Branch_id int NOT NULL,
    Card_No int NOT NULL,
    Date_Out date NOT NULL,
    Due_Date date NOT NULL,
    PRIMARY KEY (Book_id,  Branch_id,  Card_No)
);

CREATE TABLE LIBRARY_BRANCH_Animesh_LCS035(
    Branch_id int(4) PRIMARY KEY,
    Branch_Name varchar(30) NOT NULL,
    Address varchar(30) NOT NULL
);


INSERT INTO BOOK_Animesh_LCS035 VALUES
    (1111, "SE", "PEARSON", 2005),
    (2222, "DBMS", "MCGRAW", 2004),
    (3333, "ANOTOMY", "PEARSON", 2010),
    (4444, "ENCYCLOPEDIA", "SAPNA", 2010);

INSERT INTO BOOK_AUTHORS_Animesh_LCS035 VALUES
    (1111, "SOMMERVILLE"),
    (2222, "NAVATHE"),
    (3333, "HENRY GRAY"),
    (4444, "THOMAS");

INSERT INTO PUBLISHER_Animesh_LCS035 VALUES
    ("PEARSON", "BANGALORE", 9875462530),
    ("MCGRAW", "NEWDELHI", 7845691234),
    ("SAPNA", "BANGALORE", 7845963210);

INSERT INTO BOOK_COPIES_Animesh_LCS035 VALUES
    (1111, 11, 5),
    (3333, 22, 6),
    (4444, 33, 10),
    (2222, 11, 12),
    (4444, 55, 3);

INSERT INTO BOOK_LENDING_Animesh_LCS035 VALUES
    (2222, 11, 1, "2017-01-10", "2017-08-20"),
    (3333, 22, 2, "2017-07-09", "2017-08-12"),
    (4444, 55, 1, "2017-04-11", "2017-08-09"),
    (2222, 11, 5, "2017-08-09", "2017-08-19"),
    (4444, 33, 1, "2017-07-10", "2017-08-15"),
    (1111, 11, 1, "2017-05-12", "2017-06-10"),
    (3333, 22, 1, "2017-07-10", "2017-07-15");

INSERT INTO LIBRARY_BRANCH_Animesh_LCS035 VALUES
    (11, "CENTRAL TECHNICAL", "MG ROAD"),
    (22, "MEDICAL", "BH ROAD"),
    (33, "CHILDREN", "SS PURAM"),
    (44, "SECRETARIAT", "SIRAGATE"),
    (55, "GENERAL", "JAYANAGAR");


\! echo "1. Retrieve details of all books in the library – Book_id, title, name of publisher, authors, number of copies in each branch, etc."
SELECT Branch_Name, Book_id, Title, Publisher_Name, Author_Name, No_of_Copies FROM BOOK_Animesh_LCS035 JOIN BOOK_AUTHORS_Animesh_LCS035 USING (Book_id) JOIN BOOK_COPIES_Animesh_LCS035 USING (Book_id) JOIN LIBRARY_BRANCH_Animesh_LCS035 USING (Branch_id);

\! echo "2. Get the particulars of borrowers who have borrowed more than 3 books, but from Jan 2017 to Jun 2017."
SELECT * FROM BOOK_LENDING_Animesh_LCS035 JOIN BOOK_COPIES_Animesh_LCS035 USING (Book_id, Branch_id) WHERE Date_Out >= "2017-01-01" AND Due_Date < "2017-07-01" AND No_of_Copies > 3;

\! echo "3. Delete a book in BOOK table. Update the contents of other tables to reflect this data manipulation operation."
DELETE BOOK_Animesh_LCS035, BOOK_AUTHORS_Animesh_LCS035, BOOK_COPIES_Animesh_LCS035, BOOK_LENDING_Animesh_LCS035 FROM BOOK_Animesh_LCS035 JOIN BOOK_AUTHORS_Animesh_LCS035 USING (Book_id) JOIN BOOK_COPIES_Animesh_LCS035 USING (Book_id) JOIN BOOK_LENDING_Animesh_LCS035 USING (Book_id, Branch_id) WHERE Book_id = 1111;
SELECT * FROM BOOK_Animesh_LCS035;
SELECT * FROM BOOK_AUTHORS_Animesh_LCS035;
SELECT * FROM BOOK_COPIES_Animesh_LCS035;
SELECT * FROM BOOK_LENDING_Animesh_LCS035;

\! echo "4. Partition the BOOK table based on year of publication. Demonstrate its working with a simple query."
CREATE VIEW BOOK_Partitioned_Year AS SELECT Pub_Year, GROUP_CONCAT(Title) FROM BOOK_Animesh_LCS035 GROUP BY Pub_Year;
SELECT * FROM BOOK_Partitioned_Year;

\! echo "5. Create a view of all books and its number of copies that are currently available in the Library."
CREATE VIEW BOOK_Available_Copies_Animesh_LCS035 AS SELECT Title, No_of_Copies FROM BOOK_COPIES_Animesh_LCS035 JOIN BOOK_Animesh_LCS035 USING (Book_id);
SELECT * FROM BOOK_Available_Copies_Animesh_LCS035;

