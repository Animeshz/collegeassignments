#include <bits/stdc++.h>
using namespace std;

class Solution {
public:
    int lengthOfLIS(vector<int>& nums, int k) {
      int last_idx = nums.size() - 1;
      return pick(nums, k, last_idx, last_idx);
    }

    int pick(vector<int>& nums, int k, int idx, int last_idx) {
      if (idx < 0) return 0;

      if (nums[idx] - nums[last_idx] < k) {
        cout << idx << endl;
        return max(1+pick(nums, k, idx-1, idx), pick(nums, k, idx-1, last_idx));
      } else {
        return pick(nums, k, idx-1, last_idx);
      }
    }
};

int main() {
    Solution s;

    vector<int> nums = {4,2,1,4,3,4,5,8,15};
    int k = 3;
    cout << s.lengthOfLIS(nums, k);
}
