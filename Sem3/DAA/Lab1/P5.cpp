#include <bits/stdc++.h>
using namespace std;

double fractional_ksp(vector<pair<int, int>> values_weights, double capacity) {
    sort(values_weights.begin(), values_weights.end(), [](auto a, auto b) {
        return double(a.first)/a.second < double(b.first)/b.second;
    });

    double value = 0;
    for (int i = values_weights.size() - 1; i >= 0; i--) {
        auto val_wt = values_weights[i];
        double val_per_wt = double(val_wt.first) / val_wt.second;
        double chosen_weight = min((double) val_wt.second, capacity);

        capacity -= chosen_weight;
        value += chosen_weight * val_per_wt;

        if (chosen_weight > 0)
            cout << "choice: " << val_wt.first << " weight: " << chosen_weight << endl;
    }

    return value;
}

int main() {
    vector<pair<int, int>> values_weights = {
        {7, 3},
        {15, 7},
        {5, 2},
        {20, 12},
        {45, 20}
    };

    double max_value_attained = fractional_ksp(values_weights, 24.5);

    cout << max_value_attained << endl;
}
