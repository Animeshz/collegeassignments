#include <iostream>
using namespace std;

int binary_search(int array[], int start, int end, int value) {
    int mid = (start+end)/2;

    if (array[mid] == value) return mid;
    else if (array[mid] > value) return binary_search(array, start, mid-1, value);
    else return binary_search(array, mid+1, end, value);
}

int main() {
    int size = 7;
    int arr[] = {1,2,5,7,8,9,10};
    cout << "index of 2: " << binary_search(arr, 0, size-1, 2) << endl;
    cout << "index of 8: " << binary_search(arr, 0, size-1, 8) << endl;
}
