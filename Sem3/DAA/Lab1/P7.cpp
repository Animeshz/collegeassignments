#include <bits/stdc++.h>
using namespace std;

int count(string first, string second, int l1, int l2,
  vector<vector<int>> &reuse) {
    if (l1 == 0 || l2 == 0) return 0;

    if (first[l1-1] == second[l2-1]) {
        return reuse[l1][l2] = 1 + count(first, second, l1-1, l2-1, reuse);
    }

    if (reuse[l1][l2] == -1) {
        reuse[l1][l2] = max(
          count(first, second, l1, l2-1, reuse),
          count(first, second, l1-1, l2, reuse)
        );
    }

    return reuse[l1][l2];
}

int longest_common_subsequence(string first, string second) {
    int l1 = first.length(), l2 = second.length();
    vector<vector<int>> reuse(l1+1, vector<int>(l2+1, -1));

    return count(first, second, l1, l2, reuse);
}

int main() {
    string s1 = "applesNcookies";
    string s2 = "cookiesNapples";

    cout << longest_common_subsequence(s1, s2) << endl;
}
