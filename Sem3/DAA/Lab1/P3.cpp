#include <iostream>
using namespace std;

void swap(int *a, int *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

int partition(int array[], int start, int end) {
    int pivot = array[end];
    int i = start-1;
    for (int j = start; j <= end - 1; j++) {
        if (array[j] < pivot) {
            i++; swap(&array[i], &array[j]);
        }
    }
    swap(&array[i+1], &array[end]);
    return i+1;
}

void quick_sort(int array[], int start, int end) {
    if (start >= end) return;

    int part_idx = partition(array, start, end);
    quick_sort(array, start, part_idx - 1);
    quick_sort(array, part_idx + 1, end);
}

int main() {
    int size = 7;
    int arr[] = {5,7,8,1,9,2,10};
    quick_sort(arr, 0, size-1);

    for (int i = 0; i < size; i++)
        cout << arr[i] << ' ';
    cout << endl;
}
