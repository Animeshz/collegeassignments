#include <bits/stdc++.h>
using namespace std;

int zero_one_ksp(vector<pair<int, int>> values_weights, int capacity) {
    int n = values_weights.size();
    int dp[capacity + 1] = {};

    for (int i = 1; i < n + 1; i++) {
        for (int w = capacity; w >= 0; w--) {
            if (values_weights[i - 1].second <= w)
                dp[w] = max(dp[w], dp[w - values_weights[i - 1].second] + values_weights[i - 1].first);
        }
    }
    return dp[capacity];
}

int main() {
    vector<pair<int, int>> values_weights = {
        {7, 3},
        {15, 7},
        {5, 2},
        {20, 12},
        {45, 20}
    };

    int max_value_attained = zero_one_ksp(values_weights, 24);

    cout << max_value_attained << endl;
}

