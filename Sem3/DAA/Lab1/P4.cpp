#include <bits/stdc++.h>
using namespace std;

void select_min_coins(int n, int coins[], int target) {
    sort(coins, coins + n);
    for (int i = n - 1; i >= 0 && target > 0; i--) {
        int n = target/coins[i];
        if (n > 0) {
            target -= n * coins[i];
            cout << n << "*Rs." << coins[i] << ' ';
        }
    }
    cout << endl;
}

int main() {
    int total_coins = 7;
    int coins[] = {1, 2, 5, 10, 20, 50, 100};

    int target = 49;
    select_min_coins(total_coins, coins, target);
}
