#include <bits/stdc++.h>

using namespace std;
const int MAX = 1e3 + 10,
    Hi = 1e9 + 10;
int dismtance[MAX][MAX];
int main() {
    for (int i = 0; i < MAX; ++i) {
        for (int j = 0; j < MAX; ++j) {
            if (i == j) dismtance[i][j] = 0;
            else dismtance[i][j] = Hi;
        }
    }
    int n, m;
    cin >> n >> m;
    for (int i = 0; i < m; ++i) {
        int x, y, tmp;
        cin >> x >> y >> tmp;
        dismtance[x][y] = tmp;
        cout << endl;
    }
    for (int k = 1; k <= n; ++k)
        for (int i = 1; i <= n; ++i)
            for (int j = 1; j <= n; ++j)
                dismtance[i][j] = min(dismtance[i][j], dismtance[i][k] + dismtance[k][j]);
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j) {
            if (dismtance[i][j] == Hi) cout << " I ";
            else cout << " " << dismtance[i][j] << " ";
        }
        cout << endl;
    }
}

