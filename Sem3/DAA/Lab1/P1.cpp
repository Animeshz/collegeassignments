#include <iostream>
using namespace std;

int linear_search(int n, int array[], int value) {
    for (int i = 0; i < n; i++) {
        if (array[i] == value) return i;
    }
    return -1;
}

int main() {
    int size = 7;
    int arr[] = {5,7,8,1,9,2,10};
    cout << "index of 2: " << linear_search(size, arr, 2) << endl;
    cout << "index of 8: " << linear_search(size, arr, 8) << endl;
}
