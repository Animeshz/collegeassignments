---
theme: seriph
hideInToc: true
aspectRatio: '0.75'
defaults:
  layout: 'default'
themeConfig:
  code-font-size: '10px'
# background: https://source.unsplash.com/collection/94734566/1920x1080
class: 'text-left'  # windi-css
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# persist drawings in exports and build
drawings:
  persist: false
# use UnoCSS
css: unocss
---

<img src="assets/logo.png" class="h-10 mb-3" />

# DAA Lab Assignment

Indian Institute of Information Technology, Lucknow

* Name: Animesh Sahu
* EnrollmentNo: LCS2021035
* Program: BTech (CS) 3rd Sem
* Subject: Design & Analysis of Algorithms
* Instructor: Dr. Deepshikha Agarwal

---

# Table of Contents

| Topic                                                | Page |
|------------------------------------------------------|------|
| 1) Implementation of Linear search method            | 3    |
| 2) Implementation of Recursive Binary search method  | 5    |
| 3) Implementation of Recursive Quicksort method      | 8    |
| 4) Implementation of Coin selection problem using Greedy approach      | 12   |
| 5) Implementation of Fractional Knapsack problem using Greedy approach      | 14   |
| 6) Implementation of 0/1 Knapsack problem using Dynamic approach      | 16   |
| 7) Implementation of LCS using Dynamic approach      | 18   |
| 8) Implementation of n-Queen’s problem using Backtracking approach      | 20   |
| 9) Develop a program to implement the solution of Travelling Salesman Problem by considering the Hamiltonian cycle approach.      | 23   |
| 10) Develop a program to find the shortest path from each node to solve the road network problem.| 27   |

For each of them there's:

Aim | Algorithm | Program | Output | Time Complexity Analysis

---

# 1) Implementation of Linear search method

## Aim

To search for a value in an array by looking at the elements one-by-one.

## Algorithm

<br>

```
Iterate over the array (size n)
  If value in array == expected_value
    Break and return index

Element not found then return some indicator (-1)
```

<br>

## Program

<br>

```cpp
#include <iostream>
using namespace std;

int linear_search(int n, int array[], int value) {
    for (int i = 0; i < n; i++) {
        if (array[i] == value) return i;
    }
    return -1;
}

int main() {
    int size = 7;
    int arr[] = {5,7,8,1,9,2,10};
    cout << "index of 2: " << linear_search(size, arr, 2) << endl;
    cout << "index of 8: " << linear_search(size, arr, 8) << endl;
}
```

<br>

## Output

![output1](assets/output1.jpg)

---

## Time Complexity Analysis

* Best Case: $\Omega(1)$
  ```
  Case: Element found at the first index

  >> for loop will terminate immediately after first comparision
  ```
  <br>
* Worst Case: $O(N)$
  ```
  Case 1: Element found at the last index
  Case 2: Element does not exist

  >> for loop will run till the end
  ```
  <br>
* Average Case: $\Theta(N)$
  ```
  Case 1: Element is present anywhere between index 0 to N-1
          (1 to N comparisions)
  Case 2: Element does not exist (N+1'th comparision)

  >> for loop will run once, twice, ... and till the end of it
     if the element didn't exited; TotalCases = N+1
  ```

  Average Comparisions in all cases
  $$
  = \frac{\displaystyle\sum_{i=1}^{N+1} i}{N+1}
  = \frac{(N+1)(N+2)/2}{N+1}
  = \frac{N+2}{2}
  = O(N)
  $$

---

# 2) Implementation of Recursive Binary search method

## Aim

To search for a value in a sorted array by divide and conquer method (split the search area in half each iteration).

## Algorithm

<br>

```
Take array and start, end (the indices indicating the start and end of search)

if start > end:
  Element not found; return some indicator (-1)

if value in array at [(start+end)/2] == expected_value:
  Exit and return index
Else if value > expected_value
  Call itself with new end = mid-1; Return it
Else if value < expected_value
  Call itself with new start = mid+1; Return it
```

<br>

## Program

<br>

```cpp
#include <iostream>
using namespace std;

int binary_search(int array[], int start, int end, int value) {
    if (start > end) return -1;

    int mid = (start+end)/2;

    if (array[mid] == value) return mid;
    else if (array[mid] > value)
        return binary_search(array, start, mid-1, value);
    else
        return binary_search(array, mid+1, end, value);
}

int main() {
    int size = 7;
    int arr[] = {1,2,5,7,8,9,10};
    cout << "index of 2: " << binary_search(arr, 0, size-1, 2) << endl;
    cout << "index of 8: " << binary_search(arr, 0, size-1, 8) << endl;
}
```

---

## Output

![output2](assets/output2.jpg)

## Time Complexity Analysis

* Best Case: $\Omega(1)$
  ```
  Case: Element found at the middle of the array

  >> The function will return immediately
  ```
* Worst Case: $O(log_2N)$
  ```
  Case 1: Element is present at first or last index
  Case 2: Element does not exist

  >> Function will keep calling itself with half the search size each time
  ```
  The time function for the algorithm:
  $$
  T(n) = T(n/2) + c\\
  T(n/2) = T(n/4) + 2c\\
  ...\\
  \therefore T(n) = T(n/2^k) + kc\\
  $$
  For worst case we want to exhaust the T term in RHS all the way: $\\n/2^k = 1\\=> 2^k = n\\=> k = log_2(n)$

  $$
  \therefore T(n) = T(1) + log_2(n)*c\\
  => T(n) \propto log_2(n)\\
  => T(n) = O(log_2(N))
  $$

---

* Average Case: $\Theta(log_2N)$
  ```
  Case 1: Element is present anywhere between index 0 to N-1
          (1 to N comparisions)
  Case 2: Element does not exist
  ```

  Average Comparisions in all cases
  $$
  = \frac{\displaystyle\sum_{i=1}^{N+1} T(i)}{N+1}
  \approx \frac{\displaystyle\sum_{i=1}^{N+1} log_2(i)}{N+1}
  \leq log_2(N)\\
  $$

  $$
  \therefore T(N) = \Theta(log_2(N))
  $$

---

# 3) Implementation of Recursive Quicksort method

## Aim

To sort an array in efficient manner, by considering a pivot and recursively placing it in its correct position.

## Algorithm

<br>

```
Take a pivot, e.g. last element.

Loop over the rest of array and partition it by swapping in such a way
  that all elements smaller than chosen pivot are one side
  and larger are on other side.

Move the pivot from last to the point where partition is (again by swapping).

Keep repeating the process:
  for left part and right part of array about the pivot's new position.
```

<br>
<div class="text-right">PTO</div>

---

## Program

<br>

```cpp
#include <iostream>
using namespace std;

void swap(int *a, int *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

int partition(int array[], int start, int end) {
    int pivot = array[end];
    int i = start-1;
    for (int j = start; j <= end - 1; j++) {
        if (array[j] < pivot) {
            i++; swap(&array[i], &array[j]);
        }
    }
    swap(&array[i+1], &array[end]);
    return i+1;
}

void quick_sort(int array[], int start, int end) {
    if (start >= end) return;

    int part_idx = partition(array, start, end);
    quick_sort(array, start, part_idx - 1);
    quick_sort(array, part_idx + 1, end);
}

int main() {
    int size = 7;
    int arr[] = {5,7,8,1,9,2,10};
    quick_sort(arr, 0, size-1);

    for (int i = 0; i < size; i++)
        cout << arr[i] << ' ';
    cout << endl;
}
```

## Output

![output3](assets/output3.jpg)

---

## Time Complexity Analysis

If we partition the array into size of `K` and `N-K`, then

$$
T(n) = T(K) + T(n-K) + (locate-pivot)\\
T(n) = T(K) + T(n-K) + O(n)
$$

<br><br>
* Best Case: $\Omega(Nlog_2N)$
  ```
  Case: When we selected mean element as pivot, array is already sorted.
  ```
  $$
  T(n) = 2*T(n/2) + c*n\\
  => T(n/2) = 2*T(n/4) + c*(n/2)\\
  ...\\
  \therefore T(n) = 2^k * T(n/(2^k)) + k*c*n\\
  k=log_2n \space for \space which \space n/2^k = 1\\
  => T(n) = n * T(1) + n*log_2n\\
  \therefore T(n) = \Omega(n*log_2(n))
  $$

<br><br>
* Worst Case: $O(N^2)$
  ```
  Case: When size of partition is just 1 and N-1
  ```
  $$
  T(n) = T(n-1) + c*n\\
  T(n-1) = T(n-1) + c*(n-1)\\
  ...\\
  \therefore T(n) = T(n-k) + k*c*n - c*[(k-1) ....  + 3 + 2 + 1]\\
  k=n \space for \space which \space n-k=0\\

  => T(n) = T(0) + n*n*c - c*[n*(n-1)/2]\\
  => T(n) = n*n - n*(n-1)/2
  \therefore T(n) = O(n^2)
  $$

---

* Average Case: $\Theta(Nlog_2N)$
  ```
  Case: Random element for pivot, choosing all possibilities from N places
  ```
  $$
  T(n) = T(i) + T(n-i)\\
  => T(n) =  1/n *[\sum_{i=1}^{n-1} T(i)] + 1/n*[\sum_{i=1}^{n-1} T(n-i)]\\
  => T(n) = 2*c*log(n) * (n+1)\\
  \therefore T(n) = \Theta(n*log(n))
  $$

---

# 4) Implementation of Coin selection problem using Greedy approach

## Aim

To gather minimum number of coins to reach the target value.

## Algorithm

<br>

```
Sort the given set of coin's value.

And then move from higher to lower coin and greedily select what's possible.
```
<br>

## Program

<br>

```cpp
#include <bits/stdc++.h>
using namespace std;

void select_min_coins(int n, int coins[], int target) {
    sort(coins, coins + n);
    for (int i = n - 1; i >= 0 && target > 0; i--) {
        int n = target/coins[i];
        if (n > 0) {
            target -= n * coins[i];
            cout << n << "*Rs." << coins[i] << ' ';
        }
    }
    cout << endl;
}

int main() {
    int total_coins = 7;
    int coins[] = {1, 2, 5, 10, 20, 50, 100};

    int target = 49;
    select_min_coins(total_coins, coins, target);
}
```

## Output

![output4](assets/output4.jpg)

---

## Time Complexity Analysis

Sorting takes O(NlogN) complexity whereas selection at max will go upto O(N)

=> Hence sorting's complexity (merge-sort) will dominate.

* Best Case: $\Omega(Nlog_2N)$
* Worst Case: $O(Nlog_2N)$
* Average Case: $\Theta(Nlog_2N)$

---

# 5) Implementation of Fractional Knapsack problem using Greedy approach

## Aim

To find out the maximum value we can gather in a knapsack from available materials of given value.

## Algorithm

<br>

```
Make a value per unit weight list and sort the primary array by using it.

Now take as much value as we can from the array from highest to lowest.
```

## Program

<br>

```cpp
#include <bits/stdc++.h>
using namespace std;

double fractional_ksp(vector<pair<int, int>> values_weights, double capacity) {
    sort(values_weights.begin(), values_weights.end(), [](auto a, auto b) {
        return double(a.first)/a.second < double(b.first)/b.second;
    });

    double value = 0;
    for (int i = values_weights.size() - 1; i >= 0; i--) {
        auto val_wt = values_weights[i];
        double val_per_wt = double(val_wt.first) / val_wt.second;
        double chosen_weight = min((double) val_wt.second, capacity);

        capacity -= chosen_weight;
        value += chosen_weight * val_per_wt;

        if (chosen_weight > 0)
            cout << "choice: " << val_wt.first << " weight: " << chosen_weight << endl;
    }

    return value;
}

int main() {
    vector<pair<int, int>> values_weights = {
        {7, 3},
        {15, 7},
        {5, 2},
        {20, 12},
        {45, 20}
    };

    double max_value_attained = fractional_ksp(values_weights, 24.5);

    cout << max_value_attained << endl;
}
```

---

## Output

![output5](assets/output5.jpg)

## Time Complexity Analysis

Again, sorting takes O(NlogN) complexity whereas selection of materials for filling the knapsack will at max will go upto O(N)

=> Hence sorting's complexity (merge-sort) will dominate.

* Best Case: $\Omega(Nlog_2N)$
* Worst Case: $O(Nlog_2N)$
* Average Case: $\Theta(Nlog_2N)$

---

# 6) Implementation of 0/1 Knapsack problem using Dynamic approach

## Aim

To find out the maximum value we can gather in a knapsack from available materials of given value, condition that we cannot choose part of the material.

## Algorithm

<br>

```
We initially create an array (DP) in which we'll store the maximum value
  we could store for the capacity (index).

We keep filling the array
  by looping through the values and weights one-by-one.

At the end, we'll get the maximum value we could ever attain
  by filling discrete materials of at max capacity given.
```

## Program

<br>

```cpp
#include <bits/stdc++.h>
using namespace std;

int zero_one_ksp(vector<pair<int, int>> values_weights, int capacity) {
    int n = values_weights.size();
    int reuse[capacity + 1] = {};

    for (int i = 1; i < n + 1; i++) {
        for (int w = capacity; w >= 0; w--) {
            if (values_weights[i - 1].second <= w) {
                reuse[w] = max(reuse[w - values_weights[i - 1].second] + values_weights[i - 1].first, reuse[w]);
            }
        }
    }
    return reuse[capacity];
}

int main() {
    vector<pair<int, int>> values_weights = {
        {7, 3},
        {15, 7},
        {5, 2},
        {20, 12},
        {45, 20}
    };

    int max_value_attained = zero_one_ksp(values_weights, 24);

    cout << max_value_attained << endl;
}

```

---

## Output

![output6](assets/output6.jpg)

## Time Complexity Analysis

Since the loop will run exactly N*capacity times, there's no best/worst/average cases here.

=> Time Complexity will **remain constant**, that is
$$
\Theta(N * KnapsackCapacity)
$$

---

# 7) Implementation of LCS using Dynamic approach

## Aim

To find the longest common subsequence of characters in the given two strings.

## Algorithm

<br>

```
Recursively count the longest common subsequence by
  once decrementing the size of `first` string and
  once by decrementing size of `second` string.

While doing so, keep a record of what we counted previously
  by storing them into a 2D vector/array (DP).
```
<br>

## Program

<br>

```cpp
#include <bits/stdc++.h>
using namespace std;

int count(string first, string second, int l1, int l2,
  vector<vector<int>> &reuse) {
    if (l1 == 0 || l2 == 0) return 0;

    if (first[l1-1] == second[l2-1]) {
        return reuse[l1][l2] = 1 + count(first, second, l1-1, l2-1, reuse);
    }

    if (reuse[l1][l2] == -1) {
        reuse[l1][l2] = max(
          count(first, second, l1, l2-1, reuse),
          count(first, second, l1-1, l2, reuse)
        );
    }

    return reuse[l1][l2];
}

int longest_common_subsequence(string first, string second) {
    int l1 = first.length(), l2 = second.length();
    vector<vector<int>> reuse(l1+1, vector<int>(l2+1, -1));

    return count(first, second, l1, l2, reuse);
}

int main() {
    string s1 = "applesNcookies";
    string s2 = "cookiesNapples";

    cout << longest_common_subsequence(s1, s2) << endl;
}
```

---

## Output

![output7](assets/output7.jpg)

## Time Complexity Analysis

Without DP, we'd be either choosing a character or not choosing a character from either of the string, that would yield an exponential complexity, i.e. $2^{(len(FirstString) + len(SecondString))}$

But here, the time complexity is dominated by filling the DP table (2D matrix).

=> Hence in all the cases, the time complexity will **remain constant**, that is

$$
\Theta(len(FirstString)*len(SecondString))
$$

---

# 8) Implementation of n-Queen’s problem using Backtracking approach

## Aim

To find a possible combination where the the N Queens donot attack each other in NxN Chess matrix.

## Algorithm

<br>

```
We can recursively check for
  the next possible queen's position for a certain current queen's position
  until we placed N queens.

We can denote empty by 0, and queen placed by 1.

For this we've to consider the left_diagonals and right_diagonals
  of all the currently placed queens, as well as the columns & rows being reserved.

For col we're already recursively checking, for row, we can create another array
  that marks the rows for all the currently placed queens.

At the end if all queens got placed we'll recursively exit the function with true.
Or else we'll return false.
```

---

## Program

<br>

```cpp
#include <bits/stdc++.h>
using namespace std;

bool solve_nqueen_back(int col, vector<vector<int>> &board,
    int left_diag[], int right_diag[], int rowp[]) {
    int N = board.size();
    if (col >= N) return true;

    for (int i = 0; i < N; i++) {
        int ld = i-col+N-1, rd = i+col;
        if (left_diag[ld] != 1 && right_diag[rd] != 1 && rowp[i] != 1) {

            board[i][col] = left_diag[ld] = right_diag[rd] = rowp[i] = 1;

            if (solve_nqueen_back(col+1, board, left_diag, right_diag, rowp))
                return true;

            board[i][col] = left_diag[ld] = right_diag[rd] = rowp[i] = 0;
        }
    }

    return false;
}

vector<vector<int>> solve_nqueen(int size, bool &solved) {
    vector<vector<int>> board(size, vector<int>(size, 0));
    int left_diag[2*size] = {};
    int right_diag[2*size] = {};
    int row_possib[2*size] = {};

    solved = solve_nqueen_back(0, board, left_diag, right_diag, row_possib);
    return board;
}

int main() {
    int size = 8;
    bool solved = false;
    auto board = solve_nqueen(size, solved);

    if (solved) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                cout << board[i][j] << " ";
            }
            cout << endl;
        }
    } else {
        cout << "There's no possible solution to matrix of " << size << "x" << size << " chess board";
    }
}
```

---

## Output

![output8](assets/output8.jpg)

## Time Complexity Analysis

The time function comes out to be:

$$
T(n) = T(n-1) + c*n
$$

At all conditions we've to place exactly N queens, so single case will exist where time complexity:

$$
T(n-1) = T(n-2) + c*(n-1)\\
...\\

T(n) = T(n-k) + c * (n + (n-1) + ...)\\

till n = k\\

\therefore T(n) = T(0) + c*[n*(n+1)/2]\\
=> T(n) \propto n^2\\
\therefore T(n) = \Theta(n^2)
$$

---

# 9) Develop a program to implement the solution of Travelling Salesman Problem by considering the Hamiltonian cycle approach.

## Aim

To visit all the nodes in the graph and also returning to the starting position while travelling the minimum distance.

## Algorithm

```
We can iterate over the graph from vertex 0,
  and mark the vertex we've previously visited.

If we're able to comeback to our original starting position (0),
  visiting all the nodes once we return true, else false.
```
<br>

---

## Program

<br>

```cpp
#include <bits/stdc++.h>
using namespace std;

bool is_safe_path(int v, vector<vector<bool>> &graph, vector<int> &path, int pos) {
    int n = graph.size();
    if (graph[path[pos - 1]][v] == 0) return false;
    for (int i = 0; i < pos; i++) {
        if (path[i] == v) return false;
    }
    return true;
}

bool hamiltonian_cycle_back(vector<vector<bool>> &graph, vector<int> &path, int pos) {
    int n = graph.size();
    if (pos == n) {
        if (graph[path[pos - 1]][path[0]] == 1) {
            return true;
        } else {
            return false;
        }
    }

    for (int v = 1; v < n; v++) {
        if (is_safe_path(v, graph, path, pos)) {
            path[pos] = v;
            if (hamiltonian_cycle_back(graph, path, pos + 1) == true) return true;
            path[pos] = -1;
        }
    }
    return false ;
}

vector<int> hamiltonian_cycle(vector<vector<bool>> graph, bool &solved) {
    int n = graph.size();
    vector<int> p(n, 0);
    for (int i = 0; i < n; i++)
        p[i] = -1;
    p[0] = 0;
    if (hamiltonian_cycle_back(graph, p, 1) == false) {
        solved = false;
    } else {
        solved = true;
    }
    return p;
}
```

---

```cpp
int main() {
    int size = 5;
    vector<vector<bool>> graph = {
        {0, 1, 0, 1, 0},
        {1, 0, 1, 1, 1},
        {0, 1, 0, 0, 1},
        {1, 1, 0, 0, 1},
        {0, 1, 1, 1, 0}
    };

    bool solved = false;
    vector<int> path = hamiltonian_cycle(graph, solved);
    if (solved) {
        for (int i = 0; i < size; i++)
            cout << path[i] << " ";
        cout << path[0] << " ";
        cout << endl;
    } else {
        cout << "No solution is possible for given graph" << endl;
    }
}

```

---

## Output

![output9](assets/output9.jpg)

## Time Complexity Analysis

We're either moving to one position or not moving to one position that yeilds a complexity of $2^n$, and that too n times for each column in the row, for each case we're looping n times to check if its safe path or not.

In total complexity comes out to be

$$
O(n^2 * 2^n)
$$

---

# 10) Develop a program to find the shortest path from each node to solve the road network problem.

## Aim

Given a graph and a source vertex in the graph, we're trying to find the shortest paths from the source to all vertices in the given graph.

## Program

<br>

```cpp
#include <bits/stdc++.h>

using namespace std;
const int max = 1e3 + 10,
    Hi = 1e9 + 10;
int distance[max][max];
int main() {
    for (int i = 0; i < max; ++i) {
        for (int j = 0; j < max; ++j) {
            if (i == j) distance[i][j] = 0;
            else distance[i][j] = Hi;
        }
    }
    int n, m;
    cin >> n >> m;
    for (int i = 0; i < m; ++i) {
        int x, y, tmp;
        cin >> x >> y >> tmp;
        distance[x][y] = tmp;
        cout << endl;
    }
    for (int k = 1; k <= n; ++k)
        for (int i = 1; i <= n; ++i)
            for (int j = 1; j <= n; ++j)
                distance[i][j] = min(distance[i][j], distance[i][k] + distance[k][j]);
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++j) {
            if (distance[i][j] == Hi) cout << " I ";
            else cout << " " << distance[i][j] << " ";
        }
        cout << endl;
    }
}
```

---

## Output

![output10](assets/output10.jpg)

## Time Complexity Analysis

To find the minimum distance we're using three loops, and hence it yields a time complexity of:

$$
O(n^3)
$$
