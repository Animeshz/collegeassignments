#include <bits/stdc++.h>
using namespace std;

bool solve_nqueen_back(int col, vector<vector<int>> &board,
    int left_diag[], int right_diag[], int rowp[]) {
    int N = board.size();
    if (col >= N) return true;

    for (int i = 0; i < N; i++) {
        int ld = i-col+N-1, rd = i+col;
        if (left_diag[ld] != 1 && right_diag[rd] != 1 && rowp[i] != 1) {

            board[i][col] = left_diag[ld] = right_diag[rd] = rowp[i] = 1;

            if (solve_nqueen_back(col+1, board, left_diag, right_diag, rowp))
                return true;

            board[i][col] = left_diag[ld] = right_diag[rd] = rowp[i] = 0;
        }
    }

    return false;
}

vector<vector<int>> solve_nqueen(int size, bool &solved) {
    vector<vector<int>> board(size, vector<int>(size, 0));
    int left_diag[2*size] = {};
    int right_diag[2*size] = {};
    int row_possib[2*size] = {};

    solved = solve_nqueen_back(0, board, left_diag, right_diag, row_possib);
    return board;
}

int main() {
    int size = 8;
    bool solved = false;
    auto board = solve_nqueen(size, solved);

    if (solved) {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                cout << board[i][j] << " ";
            }
            cout << endl;
        }
    } else {
        cout << "There's no possible solution to matrix of " << size << "x" << size << " chess board";
    }
}

