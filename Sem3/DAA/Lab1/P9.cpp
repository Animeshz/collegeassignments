#include <bits/stdc++.h>
using namespace std;

bool is_safe_path(int v, vector<vector<bool>> &graph, vector<int> &path, int pos) {
    int n = graph.size();
    if (graph[path[pos - 1]][v] == 0) return false;
    for (int i = 0; i < pos; i++) {
        if (path[i] == v) return false;
    }
    return true;
}

bool hamiltonian_cycle_back(vector<vector<bool>> &graph, vector<int> &path, int pos) {
    int n = graph.size();
    if (pos == n) {
        if (graph[path[pos - 1]][path[0]] == 1) {
            return true;
        } else {
            return false;
        }
    }

    for (int v = 1; v < n; v++) {
        if (is_safe_path(v, graph, path, pos)) {
            path[pos] = v;
            if (hamiltonian_cycle_back(graph, path, pos + 1) == true) return true;
            path[pos] = -1;
        }
    }
    return false ;
}

vector<int> hamiltonian_cycle(vector<vector<bool>> graph, bool &solved) {
    int n = graph.size();
    vector<int> p(n, 0);
    for (int i = 0; i < n; i++)
        p[i] = -1;
    p[0] = 0;
    if (hamiltonian_cycle_back(graph, p, 1) == false) {
        solved = false;
    } else {
        solved = true;
    }
    return p;
}

int main() {
    int size = 5;
    vector<vector<bool>> graph = {
        {0, 1, 0, 1, 0},
        {1, 0, 1, 1, 1},
        {0, 1, 0, 0, 1},
        {1, 1, 0, 0, 1},
        {0, 1, 1, 1, 0}
    };

    bool solved = false;
    vector<int> path = hamiltonian_cycle(graph, solved);
    if (solved) {
        for (int i = 0; i < size; i++)
            cout << path[i] << " ";
        cout << path[0] << " ";
        cout << endl;
    } else {
        cout << "No solution is possible for given graph" << endl;
    }
}

